-- --------------------------------------------------------------------------------
-- Routine DDL
-- Note: comments before and after the routine body will not be stored by the server
-- --------------------------------------------------------------------------------
USE `forge`;
DROP procedure IF EXISTS `update_node_status`;


DELIMITER $$

CREATE PROCEDURE `update_node_status` (IN nodeStatus INTEGER,IN statusDetail LONGTEXT,IN nodeName varchar(20))
BEGIN

DECLARE EXIT HANDLER FOR NOT FOUND 

BEGIN 

	CALL log_forge_error (CONCAT('procedure name: update_node_status() NO RECORD FOUND FOR ',nodeName));

END;

DECLARE EXIT HANDLER FOR SQLEXCEPTION 

BEGIN 

	CALL log_forge_error (CONCAT('procedure name: update_node_status() SQL EXCEPTION'));

END;

DECLARE CONTINUE HANDLER FOR SQLWARNING 

BEGIN 

	CALL log_forge_error (CONCAT('procedure name: update_node_status() SQL WARNING'));

END;

	UPDATE node SET status = nodeStatus, status_detail = statusDetail WHERE name = nodeName LIMIT 1;

	COMMIT;
END$$
DELIMITER ;

GRANT EXECUTE ON PROCEDURE forge.update_node_status TO 'restb2c'@'localhost' IDENTIFIED BY 'restb2c';

FLUSH PRIVILEGES;
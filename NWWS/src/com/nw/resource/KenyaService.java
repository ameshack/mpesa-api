
package com.nw.resource;


import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ke.co.ars.entity.MpesaC2BCheckoutResult;
import ke.co.ars.entity.TrxResponse;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.ars.c2b.checkout.CheckoutController;
import com.nw.controller.Card2Mobile;
import com.nw.controller.MpesaBuyGoods;
import com.nw.controller.MpesaPaybill;
import com.nw.entity.ApiResponse;
import com.nw.entity.RequestB2C;
import com.nw.entity.RequestBuyGoods;
import com.nw.entity.RequestC2BCheckout;
import com.nw.entity.RequestPaybill;
import com.nw.entity.Response;


@Path("/keapi")
public class KenyaService {
    
    /* Get actual class name to be printed on */
    static Logger log = Logger.getLogger(KenyaService.class.getName());
    
    public KenyaService() {
  
        //PropertiesConfigurator is used to configure logger from properties file
    
        PropertyConfigurator.configure("/opt/log4j/B2CWebserviceLog4j.properties");
    }
   
    @POST
    @Path("/mobilewallet")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ApiResponse postHandler(RequestB2C request) {
        
        log.info("postHandler()...");
        
       
        String mobile = "0";
        String mwallet = "NONE";
        String country_code = "254";
        String transactionId = "0";
        String destMobile = "0";
        String amount = "0";
        int sourceID = 0;
        String source = "0";
        String beneficiary_name = "0";
        String initiatorID = "NONE";
        String initiatorPassword = "NONE";

        mobile = request.getSourceMobile();
        mwallet = request.getMwallet();
        country_code = request.getCountryCode();
        transactionId = request.getOriginTransactionID();
        destMobile = request.getDestMobile();
        amount = String.valueOf(request.getAmount());
        sourceID = request.getSourceID();
        source = request.getSourceName();
        beneficiary_name = request.getBeneficiaryName();
        initiatorID = request.getInitiatorID();
        initiatorPassword = request.getInitiatorPassword();
        
        Response response = new Response();
        
        Card2Mobile b2c = new Card2Mobile();
                                
        response = b2c.card2Mobile(mwallet,country_code,transactionId,
                        mobile,destMobile,amount,sourceID,source,beneficiary_name);
                
        ApiResponse apiResponse = new ApiResponse();
        
        apiResponse.setOriginatorTransactionID(transactionId);
        apiResponse.setResponseCode(response.getResponseStatus());
        apiResponse.setResponseDetails(response.getResponseDescription());
        
         return apiResponse;
    }

    @POST
    @Path("/billpayment")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ApiResponse paybillPostHandler(RequestPaybill paybillRequest) {
        
        log.info("paybillPostHandler().. ");
                
        String mobile = "0";
        String mwallet = "NONE";
        String country_code = "254";
        String transactionId = "0";
        String amount = "0";
        int sourceID = 0;
        String source = "0";
        String paybill = "0";
        String accountReference = "0";
        String initiatorID = "NONE";
        String initiatorPassword = "NONE";
        String organizationCode = "NONE";
        
        mobile = paybillRequest.getSourceMobile();
        mwallet = paybillRequest.getMwallet();
        country_code = paybillRequest.getCountryCode();
        transactionId = paybillRequest.getOriginTransactionID();
        paybill = paybillRequest.getPaybill();
        amount = String.valueOf(paybillRequest.getAmount());
        sourceID = paybillRequest.getSourceID();
        source = paybillRequest.getSourceName();
        accountReference = paybillRequest.getReferenceNumber();
        initiatorID = paybillRequest.getInitiatorID();
        initiatorPassword = paybillRequest.getInitiatorPassword();
        organizationCode = paybillRequest.getOrganizationCode();
               
        Response response = new Response();
        
        
                
        MpesaPaybill mpesaBillPay = new MpesaPaybill();
                                
        response = mpesaBillPay.billPayment(mwallet,country_code,transactionId,
                        mobile,amount,sourceID,source,organizationCode,accountReference);
                
             
        ApiResponse apiResponse = new ApiResponse();
                
        apiResponse.setOriginatorTransactionID(transactionId);
        apiResponse.setResponseCode(response.getResponseStatus());
        apiResponse.setResponseDetails(response.getResponseDescription());
                
        return apiResponse;
    }
    
    @POST
    @Path("/mpesabuygoods")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ApiResponse buyGoodsPostHandler(RequestBuyGoods buyGoodsRequest) {
        
        log.info("buyGoodsPostHandler() : ");
                
        String mobile = "0";
        String mwallet = "NONE";
        String country_code = "254";
        String transactionId = "0";
        String destMobile = "0";
        String amount = "0";
        int sourceID = 0;
        String source = "0";
        String till_number = "0";
        String accountReference = "0";
        String initiatorID = "NONE";
        String initiatorPassword = "NONE";
        
        mobile = buyGoodsRequest.getSourceMobile();
        mwallet = buyGoodsRequest.getMwallet();
        country_code = buyGoodsRequest.getCountryCode();
        transactionId = buyGoodsRequest.getOriginTransactionID();
        till_number = buyGoodsRequest.getTillNumber();
        amount = String.valueOf(buyGoodsRequest.getAmount());
        sourceID = buyGoodsRequest.getSourceID();
        source = buyGoodsRequest.getSourceName();
        accountReference = buyGoodsRequest.getReferenceNumber();
        initiatorID = buyGoodsRequest.getInitiatorID();
        initiatorPassword = buyGoodsRequest.getInitiatorPassword();
       
        
        Response response = new Response();
                
        MpesaBuyGoods buyGoods = new MpesaBuyGoods();
                                
        response = buyGoods.tillPayment(mwallet,country_code,transactionId,
                        mobile,amount,sourceID,source,till_number,accountReference);
                
        ApiResponse apiResponse = new ApiResponse();
        
        apiResponse.setOriginatorTransactionID(transactionId);
        apiResponse.setResponseCode(response.getResponseStatus());
        apiResponse.setResponseDetails(response.getResponseDescription());
                
        return apiResponse;
    }
    
    @POST
    @Path("/mpesac2bonlinecheckout")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public TrxResponse c2bCheckoutPostHandler(RequestC2BCheckout requestC2BCheckout0) {
        
        log.info("c2bCheckoutPostHandler().. ");
                
        String mobile = "0";
        String mwallet = "NONE";
        String country_code = "254";
        String currency_code = "404";
        String transactionId = "0";
        double amount = 0;
        int sourceID = 0;
        String source = "0";
        String merchantAccount = "0";
        String referenceNumber = "0";

        
        mobile = requestC2BCheckout0.getSourceMobile();
        mwallet = requestC2BCheckout0.getMwallet();
        country_code = requestC2BCheckout0.getCountryCode();
        transactionId = requestC2BCheckout0.getOriginTransactionID();
        merchantAccount = requestC2BCheckout0.getMerchantAccount();
        amount = requestC2BCheckout0.getAmount();
        sourceID = requestC2BCheckout0.getSourceID();
        source = requestC2BCheckout0.getSourceName();
        referenceNumber = requestC2BCheckout0.getReferenceNumber();
        currency_code = requestC2BCheckout0.getCurrencyCode();
               
        
        TrxResponse checkoutTrxResponse1 = new TrxResponse();
                
        CheckoutController checkoutController0 = new CheckoutController();
        
        checkoutTrxResponse1 = checkoutController0.processC2BCheckout(mobile, mwallet, 
        		country_code, currency_code, transactionId, amount, sourceID, source, 
        		merchantAccount, referenceNumber);
        
        checkoutTrxResponse1.setTransactionID(transactionId);
                
        return checkoutTrxResponse1;
    }
    
    @POST
    @Path("/confirmmpesac2bonlinecheckout")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public TrxResponse confirmC2bCheckoutPostHandler(RequestC2BCheckout requestC2BCheckout0) {
        
        log.info("confirmC2bCheckoutPostHandler().. ");
                
        String mwallet = "NONE";;
        String transactionId = "0";
        int sourceID = 0;
        String source = "0";
        
        mwallet = requestC2BCheckout0.getMwallet();
        transactionId = requestC2BCheckout0.getOriginTransactionID();
        sourceID = requestC2BCheckout0.getSourceID();
        source = requestC2BCheckout0.getSourceName();

        
        TrxResponse checkoutTrxResponse2 = new TrxResponse();
                
        CheckoutController checkoutController1 = new CheckoutController();
        
        checkoutTrxResponse2 = checkoutController1.processC2BCheckoutConfirmation(mwallet, 
        		transactionId, sourceID, source);
        
        checkoutTrxResponse2.setTransactionID(transactionId);
                
        return checkoutTrxResponse2;
    }
    
    @POST
    @Path("/querympesac2bonlinecheckouttransaction")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public MpesaC2BCheckoutResult queryC2bCheckoutTransactionPostHandler(RequestC2BCheckout requestC2BCheckout0) {
        
        log.info("queryC2bCheckoutTransactionPostHandler().. ");
                
        String mwallet = "NONE";;
        String transactionId = "0";
        int sourceID = 0;
        String source = "0";
        
        mwallet = requestC2BCheckout0.getMwallet();
        transactionId = requestC2BCheckout0.getOriginTransactionID();
        sourceID = requestC2BCheckout0.getSourceID();
        source = requestC2BCheckout0.getSourceName();

        
        MpesaC2BCheckoutResult mpesaC2BCheckoutResult0 = new MpesaC2BCheckoutResult();
                
        CheckoutController checkoutController2 = new CheckoutController();
        
        mpesaC2BCheckoutResult0 = checkoutController2.processC2BQueryTransaction(mwallet, 
        		transactionId, sourceID, source);
                
        return mpesaC2BCheckoutResult0;
    }
}

/**
 * Lnmo_checkout_ServiceSkeletonInterface.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
package ns;

/**
 * Lnmo_checkout_ServiceSkeletonInterface java skeleton interface for the
 * axisService
 */
public interface Lnmo_checkout_ServiceSkeletonInterface {

	/**
	 * Auto generated method signature
	 * 
	 * @param transactionStatusRequest
	 */

	public ns.TransactionStatusResponse transactionStatusQuery(
			ns.TransactionStatusRequest transactionStatusRequest);

	/**
	 * Auto generated method signature
	 * 
	 * @param resultMsg
	 */

	public ns.ResponseMsg lNMOResult(ns.ResultMsg resultMsg);

	/**
	 * Auto generated method signature
	 * 
	 * @param transactionConfirmRequest
	 */

	public ns.TransactionConfirmResponse confirmTransaction(
			ns.TransactionConfirmRequest transactionConfirmRequest);

	/**
	 * Auto generated method signature
	 * 
	 * @param processCheckOutRequest
	 */

	public ns.ProcessCheckOutResponse processCheckOut(
			ns.ProcessCheckOutRequest processCheckOutRequest);

}

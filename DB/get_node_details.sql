-- --------------------------------------------------------------------------------
-- Routine DDL
-- Note: comments before and after the routine body will not be stored by the server
-- --------------------------------------------------------------------------------
USE `forge`;
DROP procedure IF EXISTS `get_node_details`;


DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_node_details`(IN serverName varchar(20),OUT nodeID INT,OUT extAccID INT, 
 OUT nodeName varchar(20),OUT hostIP varchar(15),OUT hostPort varchar(11),
 OUT timeOut INT,OUT uName varchar(45),OUT pwd VARCHAR(255),
 OUT nodeURL varchar(100),OUT nodeStatus INT,OUT instCode varchar(45),
 OUT accID varchar(45), OUT currencyCode varchar(5),OUT float_bank_acc varchar(20),
 OUT comm_bank_acc varchar(20),OUT node_call_back_url varchar(100),
 OUT node_callback_method varchar(45))

BEGIN

DECLARE nodeStatusCur CURSOR FOR SELECT n.id,n.external_account_id,n.name,
                                        n.host_ip,n.host_port,n.timeout,
										n.username,AES_DECRYPT(n.node_passwd,SHA1('@ut0b073')) node_passwd,
										n.url,n.status,n.call_back_url,n.call_back_method,
										e.institution_code,e.account_id,
										e.currency_code, e.float_bank_acc_id,
										e.comm_bank_acc_id FROM node n 
										INNER JOIN external_account e 
										ON e.id = n.external_account_id 
										WHERE n.name = serverName AND 
										e.status = 'ACTIVE' ORDER BY n.id 
										ASC LIMIT 1;

DECLARE EXIT HANDLER FOR NOT FOUND 

BEGIN 

	CALL log_forge_error (CONCAT('procedure name: get_node_details() NO RECORD FOUND FOR',serverName));

END;

DECLARE EXIT HANDLER FOR SQLEXCEPTION 

BEGIN 

	CALL log_forge_error (CONCAT('procedure name: get_node_details() SQL EXCEPTION'));

END;

DECLARE CONTINUE HANDLER FOR SQLWARNING 

BEGIN 

	CALL log_forge_error (CONCAT('procedure name: get_node_details() SQL WARNING'));

END;

OPEN nodeStatusCur;

FETCH nodeStatusCur INTO nodeID,extAccID,nodeName,hostIP,hostPort,timeOut,
						 uName,pwd,nodeURL,nodeStatus,node_call_back_url,
						 node_callback_method,instCode,accID,currencyCode,
						 float_bank_acc,comm_bank_acc;

CLOSE nodeStatusCur;

END$$

DELIMITER ;

GRANT EXECUTE ON PROCEDURE forge.get_node_details TO 'restb2c'@'localhost' IDENTIFIED BY 'restb2c';

FLUSH PRIVILEGES;

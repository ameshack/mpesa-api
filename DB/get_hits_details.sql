-- --------------------------------------------------------------------------------
-- Routine DDL
-- Note: comments before and after the routine body will not be stored by the server
-- --------------------------------------------------------------------------------
USE `forge`;
DROP procedure IF EXISTS `get_hits_details`;


DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_hits_details`()

BEGIN

/**DECLARE nodeStatusCur CURSOR FOR SELECT id,req_timestamp,trx_type,payload FROM hits limit 20;*/

DECLARE EXIT HANDLER FOR NOT FOUND 

BEGIN 

	CALL log_forge_error (CONCAT('procedure name: get_node_details() NO RECORD FOUND FOR'));

END;

DECLARE EXIT HANDLER FOR SQLEXCEPTION 

BEGIN 

	CALL log_forge_error (CONCAT('procedure name: get_node_details() SQL EXCEPTION'));

END;

DECLARE CONTINUE HANDLER FOR SQLWARNING 

BEGIN 

	CALL log_forge_error (CONCAT('procedure name: get_node_details() SQL WARNING'));

END;

/**OPEN nodeStatusCur;

FETCH nodeStatusCur INTO hit_id,hit_timestamp,hit_trx_type,hit_payload;

CLOSE nodeStatusCur;*/

SELECT id,req_timestamp,trx_type,payload FROM hits limit 20;

END$$

DELIMITER ;

GRANT EXECUTE ON PROCEDURE forge.get_hits_details TO 'restb2c'@'localhost' IDENTIFIED BY 'restb2c';

FLUSH PRIVILEGES;

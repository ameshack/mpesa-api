/**
 * MpesaB2CGenericAPIResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
package com.vodafone.mm.gen.api_v1.mminterface.resultws.result;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.vodafone.mm.gen.api_v1.mminterface.resultws.controller.MpesaResultController;
import com.vodafone.mm.gen.api_v1.mminterface.resultws.response.Response;

/**
 * MpesaB2CGenericAPIResult java skeleton for the axisService
 */
public class MpesaB2CGenericAPIResult {

    /* Get actual class name to be printed on */
    static Logger log = Logger.getLogger(MpesaB2CGenericAPIResult.class.getName());
    /**
     * Auto generated method signature
     * 
     * @param resultMsg
     * @return responseMsg
     */

    public com.vodafone.mm.gen.api_v1.mminterface.resultws.result.ResponseMsg genericAPIResult
            (
                    com.vodafone.mm.gen.api_v1.mminterface.resultws.result.ResultMsg resultMsg
            )
    {
        
      //PropertiesConfigurator is used to configure logger from properties file
        PropertyConfigurator.configure("/opt/log4j/mpesalog4j.properties");
        
        log.info("Recieved GenericAPIResult....." + resultMsg.getResultMsg());
        
        Result sentResult = new Result();
                
        try {
            JAXBContext jc = JAXBContext.newInstance(Result.class);
            
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            
            StringBuffer xmlStr = new StringBuffer(resultMsg.getResultMsg());
            
            sentResult = (Result) ((javax.xml.bind.JAXBElement) unmarshaller.unmarshal(new StringReader(xmlStr.toString()))).getValue();
            
        } catch (JAXBException e) {
            // TODO Auto-generated catch block
//            e.printStackTrace();
            log.error("Exception: ",e.fillInStackTrace());
        }
        
//        AMB2CDAO amb2cdao = new AMB2CDAO();
//        
//        amb2cdao.updateC2BTrxResult(sentResult);
//        Result sentResult = result.getValue();
        MpesaResultController mpesaResultController = new MpesaResultController();
        
        mpesaResultController.processResultMsg(sentResult);
        
        String responseCode = "0";
        
        String responseDesc = "success";
        
        Response response = new Response();
        
        response.setResponseCode(responseCode);
        
        response.setResponseDesc(responseDesc);
        
        StringWriter stringWriter = new StringWriter();
       
        try {
            
            JAXBContext jc = JAXBContext.newInstance(Response.class);
            Marshaller marshaller = jc.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            
            marshaller.marshal(response,stringWriter);
            
        } catch (JAXBException e) {
            // TODO Auto-generated catch block
//            e.printStackTrace();
            log.error("Exception: ",e.fillInStackTrace());
        }
        
        String xmlResponse = stringWriter.toString();
        
        ResponseMsg responseMsg = new ResponseMsg();
        
        responseMsg.setResponseMsg("<![CDATA[" + xmlResponse + "]]>");
        
        log.info("GenericAPIResult response: " + responseMsg.getResponseMsg());
        
        return responseMsg;
        // TODO : fill this with the necessary business logic
//        throw new java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#genericAPIResult");
    }

}

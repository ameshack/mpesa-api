USE `forge`;
DROP procedure IF EXISTS `log_mpesa_c2b_confirmation_request`;


DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `log_mpesa_c2b_confirmation_request`(IN c2b_val_trans_id varchar(100),
IN c2b_val_trans_type varchar(45),IN c2b_val_trans_time varchar(45),IN c2b_val_trans_amount double,
IN c2b_val_business_shortcode varchar(45),IN c2b_val_bill_ref_number varchar(45),
IN c2b_val_org_account_balance double,IN c2b_val_third_party_trx_id varchar(45),
IN c2b_val_invoice_number varchar(45),IN c2b_val_msisdn varchar(14),IN c2b_val_kyc_first_name varchar(45),
IN c2b_val_kyc_middle_name varchar(45),IN c2b_val_kyc_last_name varchar(45),
OUT c2b_val_confirmation_id INTEGER)

BEGIN

	DECLARE c2b_confirmation_cur CURSOR FOR SELECT LAST_INSERT_ID();

	DECLARE EXIT HANDLER FOR SQLSTATE '23000' 

	BEGIN

		CALL log_forge_error (CONCAT('procedure name: log_mpesa_c2b_confirmation_request() Constraint Violation.'));

	END;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION 

	BEGIN 

		CALL log_forge_error (CONCAT('procedure name: log_mpesa_c2b_confirmation_request() SQL EXCEPTION'));

	END;

	DECLARE CONTINUE HANDLER FOR SQLWARNING 

	BEGIN 

		CALL log_forge_error (CONCAT('procedure name: log_mpesa_c2b_confirmation_request() SQL WARNING'));

	END;

		
			INSERT INTO mpesa_c2b_confirmation_request (`trans_id`,`trans_type`,`trans_time`,
				`trans_amount`,`business_shortcode`,`bill_ref_number`,`invoice_number`,
				`org_account_balance`,`third_party_transaction_id`,`msisdn`,`kyc_first_name`,
				`kyc_middle_name`,`kyc_last_name`,`date_created`,`date_modified`)
			VALUES(c2b_val_trans_id,c2b_val_trans_type,c2b_val_trans_time,
				c2b_val_trans_amount,c2b_val_business_shortcode,c2b_val_bill_ref_number,
				c2b_val_invoice_number,c2b_val_org_account_balance,c2b_val_third_party_trx_id,
				c2b_val_msisdn,c2b_val_kyc_first_name,c2b_val_kyc_middle_name,
				c2b_val_kyc_last_name,NOW(),NOW());

			COMMIT;

			OPEN c2b_confirmation_cur;

			FETCH c2b_confirmation_cur INTO c2b_val_confirmation_id;

			CLOSE c2b_confirmation_cur;
	

END$$
DELIMITER ;

GRANT EXECUTE ON PROCEDURE forge.log_mpesa_c2b_confirmation_request TO 'restb2c'@'localhost' IDENTIFIED BY 'restb2c';

FLUSH PRIVILEGES;

package com.nw.resource;


import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.nw.controller.TanzaniaController;
import com.nw.entity.MethodCall;
import com.nw.entity.Response;


@XmlRootElement
@Path("/tz/")
public class TanzaniaResource {
    
    /* Get actual class name to be printed on */
    static Logger log = Logger.getLogger(TanzaniaResource.class.getName());
    
    public TanzaniaResource() {
  
        //PropertiesConfigurator is used to configure logger from properties file
    
        PropertyConfigurator.configure("/opt/log4j/B2CWebserviceLog4j.properties");
    }

    @POST
    @Path("mobilewallet")
    @Consumes("text/xml")
    @Produces({MediaType.TEXT_XML})
    public MethodCall postHandler(MethodCall call) {
        
        log.info("postHandler() : " + call);
        
        MethodCall.Params params = call.getParams();
        
        List<MethodCall.Params.Param> paramList = params.getParam();
        
        MethodCall.Params.Param param = paramList.get(0);
        
        MethodCall.Params.Param.Value value = param.getValue();
        
        MethodCall.Params.Param.Value.Struct struct = value.getStruct();
        
        List <MethodCall.Params.Param.Value.Struct.Member> memberList = struct.getMember();
          
        MethodCall.Params.Param param2 = paramList.get(1);
        
        MethodCall.Params.Param.Value valueTwo = param2.getValue();

        List<MethodCall.Params.Param.Value.Struct.Member.Value2.Struct2.Member2> memberList2 = null;
        
        String method = valueTwo.getString();
//        String method = call.getMethodName();
                
        String mcMno = "NONE";
        String mcCountryCode = "255";
        String mcTransactionId = "0";
        String mcDestMobile = "0";
        String mcSourceMobile = "0";
        String mcSourceAmount = "0";
        String mcDestAmount = "0";
        int mcSourceID = 0;
        String mcSource = "0";
        String mcFxRate = "0";
        String mcCurrencyTo = "Tanzanian Shilling";
        String mcCurrencyFrom = "NONE";

        
        int arraySize = memberList.size();
        
        Response response = new Response();
        
        if (arraySize > 1 ) {
            
       
        for (int i=0; i<arraySize; i++){
            
            MethodCall.Params.Param.Value.Struct.Member member = memberList.get(i);
            
            MethodCall.Params.Param.Value.Struct.Member.Value2 value2 = member.getValue();
                 
            switch (member.getName()) {
                
                case "source_msisdn": mcSourceMobile = value2.getString();
                     break;  
                case "mno": mcMno = value2.getString();
                     break;
                case "country_code" : mcCountryCode = value2.getString();
                     break;
                case "transaction_id" : mcTransactionId = value2.getString();
                     break;
                case "dest_msisdn" : mcDestMobile = value2.getString();
                     break;
                case "source_amount" : 	mcSourceAmount = value2.getString();
                     break;
                case "destination_amount" : mcDestAmount = value2.getString();
                     break;
                case "source_id" : mcSourceID = Integer.valueOf(value2.getString());
                     break;
                case "source" : mcSource = value2.getString();
                     break;
                case "exchange_rate" : mcFxRate = value2.getString();
                     break;
                case "currency_to" : mcCurrencyTo = value2.getString();
                     break;
                case "currency_from" : mcCurrencyFrom = value2.getString();
                     break;
            }
            
         }
//        }
        
        log.info("method : " + method);
        
        switch (method) {
            
            
            case "sendMoney":
                
            	TanzaniaController tzController = new TanzaniaController();
                                
                response = tzController.card2MobileTanzania(mcMno, mcCountryCode, mcTransactionId, 
                		mcSourceMobile, mcDestMobile, mcSourceAmount, mcDestAmount, mcSourceID, mcSource, 
                		mcFxRate, mcCurrencyTo, mcCurrencyFrom);
                
                break;

        }
        
        }

        MethodCall callResponse = new MethodCall();

        callResponse = methodCallResponse(response,method);
        
         return callResponse;
    }

  
    public MethodCall methodCallResponse(Response response, String methodName){
        
        List <MethodCall.Params.Param.Value.Struct.Member> memberList1 = new ArrayList<MethodCall.Params.Param.Value.Struct.Member>();

        MethodCall.Params.Param.Value.Struct.Member member1 = new MethodCall.Params.Param.Value.Struct.Member();
                
        MethodCall.Params.Param.Value.Struct.Member.Value2 value3 = new MethodCall.Params.Param.Value.Struct.Member.Value2();

        value3.setString(response.getTransactionID());

        member1.setName("transactionID");

        member1.setValue(value3);

        memberList1.add(member1);

        
        MethodCall.Params.Param.Value.Struct.Member member2 = new MethodCall.Params.Param.Value.Struct.Member();
        
        MethodCall.Params.Param.Value.Struct.Member.Value2 value5 = new MethodCall.Params.Param.Value.Struct.Member.Value2();

        
        value5.setString(response.getResponseStatus());

        member2.setName("response_status");

        member2.setValue(value5);

        memberList1.add(member2);
        
        
        MethodCall.Params.Param.Value.Struct.Member member3 = new MethodCall.Params.Param.Value.Struct.Member();
        
        MethodCall.Params.Param.Value.Struct.Member.Value2 value6 = new MethodCall.Params.Param.Value.Struct.Member.Value2();

        value6.setString(response.getResponseDescription());

        member3.setName("responseDescription");

        member3.setValue(value6);

        memberList1.add(member3);

        
        MethodCall.Params.Param.Value.Struct struct1 = new MethodCall.Params.Param.Value.Struct();

        struct1.getMember().addAll(memberList1);

        MethodCall.Params.Param.Value value4 = new MethodCall.Params.Param.Value();

        value4.setStruct(struct1);

        MethodCall.Params.Param param1 = new MethodCall.Params.Param();

        param1.setValue(value4);

        List<MethodCall.Params.Param> paramList1 = new ArrayList<MethodCall.Params.Param>();
        
        paramList1.add(param1);

        MethodCall.Params params1 = new MethodCall.Params(); 

        params1.getParam().addAll(paramList1);

        MethodCall methodCall1 = new MethodCall();

        methodCall1.setMethodName(methodName);
        
        methodCall1.setParams(params1);
        
        return methodCall1;
    }
}

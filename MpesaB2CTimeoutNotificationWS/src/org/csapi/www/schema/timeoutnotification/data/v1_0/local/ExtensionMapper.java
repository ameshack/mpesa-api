
/**
 * ExtensionMapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

        
            package org.csapi.www.schema.timeoutnotification.data.v1_0.local;
        
            /**
            *  ExtensionMapper class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ExtensionMapper{

          public static java.lang.Object getTypeObject(java.lang.String namespaceURI,
                                                       java.lang.String typeName,
                                                       javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{

              
                  if (
                  "http://www.csapi.org/schema/timeoutnotification/data/v1_0/local".equals(namespaceURI) &&
                  "notifyQueueTimeout".equals(typeName)){
                   
                            return  org.csapi.www.schema.timeoutnotification.data.v1_0.local.NotifyQueueTimeout.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://api-v1.gen.mm.vodafone.com/mminterface/result".equals(namespaceURI) &&
                  "ParameterType".equals(typeName)){
                   
                            return  org.csapi.www.wsdl.timeoutnotification.data.v1_0.result.ParameterType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://api-v1.gen.mm.vodafone.com/mminterface/result".equals(namespaceURI) &&
                  "ResultDesc_type1".equals(typeName)){
                   
                            return  org.csapi.www.wsdl.timeoutnotification.data.v1_0.result.ResultDesc_type1.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://api-v1.gen.mm.vodafone.com/mminterface/result".equals(namespaceURI) &&
                  "ResultCode_type1".equals(typeName)){
                   
                            return  org.csapi.www.wsdl.timeoutnotification.data.v1_0.result.ResultCode_type1.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://api-v1.gen.mm.vodafone.com/mminterface/result".equals(namespaceURI) &&
                  "ResultParameters_type0".equals(typeName)){
                   
                            return  org.csapi.www.wsdl.timeoutnotification.data.v1_0.result.ResultParameters_type0.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.csapi.org/schema/timeoutnotification/data/v1_0/local".equals(namespaceURI) &&
                  "extensionInfo_type1".equals(typeName)){
                   
                            return  org.csapi.www.schema.timeoutnotification.data.v1_0.local.ExtensionInfo_type1.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.csapi.org/schema/timeoutnotification/data/v1_0/local".equals(namespaceURI) &&
                  "extensionInfo_type0".equals(typeName)){
                   
                            return  org.csapi.www.schema.timeoutnotification.data.v1_0.local.ExtensionInfo_type0.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://api-v1.gen.mm.vodafone.com/mminterface/result".equals(namespaceURI) &&
                  "Result".equals(typeName)){
                   
                            return  org.csapi.www.wsdl.timeoutnotification.data.v1_0.result.Result.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.csapi.org/schema/timeoutnotification/data/v1_0/local".equals(namespaceURI) &&
                  "notifyQueueTimeoutResponse".equals(typeName)){
                   
                            return  org.csapi.www.schema.timeoutnotification.data.v1_0.local.NotifyQueueTimeoutResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://api-v1.gen.mm.vodafone.com/mminterface/result".equals(namespaceURI) &&
                  "ReferenceData_type0".equals(typeName)){
                   
                            return  org.csapi.www.wsdl.timeoutnotification.data.v1_0.result.ReferenceData_type0.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://api-v1.gen.mm.vodafone.com/mminterface/result".equals(namespaceURI) &&
                  "ResultDesc_type1".equals(typeName)){
                   
                            return  org.csapi.www.wsdl.timeoutnotification.data.v1_0.result.ResultDesc_type1.Factory.parse(reader);
                        

                  }

              
             throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
          }

        }
    
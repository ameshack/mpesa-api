/**
 * @author Alex Mbolonzi
 * @date 19/08/2013
 */
package org.csapi.www.wsdl.transaction.data.v1_0.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.csapi.www.wsdl.transaction.data.v1_0.dao.ConnectionHelper;
import org.csapi.www.wsdl.transaction.data.v1_0.entity.Node;


public class NodeDAO {
    
    /* Get actual class name to be printed on */
    static Logger log = Logger.getLogger(NodeDAO.class.getName());

    public NodeDAO() {
      //PropertiesConfigurator is used to configure logger from properties file
        PropertyConfigurator.configure("/opt/mpesa/log4j/log4j.properties");
    }
    
    public Node getServerDetails(String serverName) {

        log.info("getServerDetails()....");
        
        Node nodeDetails = new Node();

        Connection conn = null;

        String sql = "SELECT n.id,n.external_account_id,n.name,n.host_ip," +
                "n.host_port,n.timeout,n.username,n.node_passwd,n.url,n.status," +
                "e.institution_code,e.account_id,e.currency_code FROM node n " +
                "INNER JOIN external_account e ON e.id = n.external_account_id " +
                "WHERE n.name = ? AND e.status = ? ORDER BY n.id ASC LIMIT 1";

        log.debug("Query : " + sql);
        
        try {

            log.info("Opening DB connection...");
            
            conn = ConnectionHelper.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, serverName);
            ps.setString(2, "ACTIVE");
            ResultSet rs = ps.executeQuery();
            
            if (!rs.isBeforeFirst() ) {
                
                nodeDetails.setStatus(98);
                
                nodeDetails.setStatusDescription("ERROR: Unable to retrive " +
                		"server details, server details may not exist in DB or " +
                		"Merchant account is Inactive");
                
            }else {
            
                while (rs.next()) {
                    nodeDetails = processRow(rs);
                }
            }
        } catch (SQLException e) {
            
            nodeDetails.setStatus(99);
            
            nodeDetails.setStatusDescription("ERROR: Unable to login to DB");
            
//            e.printStackTrace();
            log.error("Exception: ",e.fillInStackTrace());
            throw new RuntimeException(e);
        } finally {
            
            log.info("Closing DB connection...");
            
            ConnectionHelper.close(conn);
        }

        return nodeDetails;
    }

    
    protected Node processRow(ResultSet rs) throws SQLException {

        log.info("processRow()....");
        
        Node nodeDetails = new Node();

        nodeDetails.setNodeID(rs.getInt("id"));
        nodeDetails.setNodeName(rs.getString("name"));
        nodeDetails.setNodeIP(rs.getString("host_ip"));
        nodeDetails.setNodePort(rs.getInt("host_port"));
        nodeDetails.setTimeout(rs.getInt("timeout"));
        nodeDetails.setUrl(rs.getString("url"));
        nodeDetails.setStatus(rs.getInt("status"));
        nodeDetails.setUsername(rs.getString("username"));
        nodeDetails.setPassword(rs.getString("node_passwd"));
        nodeDetails.setInstitutionCode(rs.getString("institution_code"));
        nodeDetails.setAccountcode(rs.getString("account_id"));
        nodeDetails.setCurrencyCode(rs.getString("currency_code"));

        return nodeDetails;
    }
}

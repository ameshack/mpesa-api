USE `forge`;
DROP procedure IF EXISTS `update_Agent`;

DELIMITER $$
USE `forge`$$
CREATE PROCEDURE `update_Agent` (IN agent_msisdn varchar(15),
IN agent_pin varbinary(45),IN agent_status boolean, OUT sql_state INTEGER)
BEGIN

	DECLARE EXIT HANDLER FOR SQLSTATE '23000' 

	BEGIN

		CALL log_forge_error (CONCAT('procedure name: add_subscriber() Constraint Violation.'));

		SET sql_state = 23000;

	END;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION 

	BEGIN 

		CALL log_forge_error (CONCAT('procedure name: add_subscriber() SQL EXCEPTION'));

		SET sql_state = 23001;

	END;

	DECLARE CONTINUE HANDLER FOR SQLWARNING 

	BEGIN 

	CALL log_forge_error (CONCAT('procedure name: add_subscriber() SQL WARNING'));

		SET sql_state = 23002;

	END;

	UPDATE account_holder_profile SET pin = agent_pin,is_active = agent_status,
						last_updated = NOW() WHERE mobile_number = agent_msisdn LIMIT 1;

	COMMIT;

	SET sql_state = 0;

END$$

DELIMITER ;

GRANT EXECUTE ON PROCEDURE forge.update_Agent TO 'restb2c'@'localhost' IDENTIFIED BY 'restb2c';

FLUSH PRIVILEGES;
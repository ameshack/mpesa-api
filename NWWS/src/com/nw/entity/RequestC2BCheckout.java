package com.nw.entity;

public class RequestC2BCheckout {
	
	private String countryCode;
	private String currencyCode;
	private String mwallet;
	private int sourceID;
	private String sourceName;
	private String sourceMobile;
	private String merchantAccount;
	private String referenceNumber;
	private Double amount;
	private String originTransactionID;
	private String initiatorID;
//	private String initiatorPassword;
//	private String organizationCode;
	
	public RequestC2BCheckout() {
		
	}
	
	public RequestC2BCheckout(String countryCode,String currencyCode,String mWallet,int sourceID,
			String sourceName,String sourceMobile,String paybill,String referenceNumber,
			Double amount,String originTransactionID,String initiatorID,
			String initiatorPassword,String organizationCode) {
		this.countryCode = countryCode;
		this.currencyCode = currencyCode;
		this.mwallet = mWallet;
		this.sourceID = sourceID;
		this.sourceName = sourceName;
		this.sourceMobile = sourceMobile;
		this.merchantAccount = paybill;
		this.referenceNumber = referenceNumber;
		this.amount = amount;
		this.originTransactionID = originTransactionID;
		this.initiatorID = initiatorID;
//		this.initiatorPassword = initiatorPassword;
//		this.organizationCode = organizationCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getMwallet() {
		return mwallet;
	}

	public void setMwallet(String mwallet) {
		this.mwallet = mwallet;
	}

	public int getSourceID() {
		return sourceID;
	}

	public void setSourceID(int sourceID) {
		this.sourceID = sourceID;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public String getSourceMobile() {
		return sourceMobile;
	}

	public void setSourceMobile(String sourceMobile) {
		this.sourceMobile = sourceMobile;
	}

	public String getMerchantAccount() {
		return merchantAccount;
	}

	public void setMerchantAccount(String merchantAccount) {
		this.merchantAccount = merchantAccount;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getOriginTransactionID() {
		return originTransactionID;
	}

	public void setOriginTransactionID(String originTransactionID) {
		this.originTransactionID = originTransactionID;
	}

	public String getInitiatorID() {
		return initiatorID;
	}

	public void setInitiatorID(String initiatorID) {
		this.initiatorID = initiatorID;
	}

//	public String getInitiatorPassword() {
//		return initiatorPassword;
//	}
//
//	public void setInitiatorPassword(String initiatorPassword) {
//		this.initiatorPassword = initiatorPassword;
//	}
	
	@Override
	public String toString() {

	        return new StringBuffer(" country_code : ").append(this.countryCode)
	        		.append(" currency_code : ").append(this.currencyCode)
	        		.append(" mwallet : ").append(this.mwallet)
	        		.append(" source_id : ").append(this.sourceID)
	        		.append(" source_name : ").append(this.sourceName)
	        		.append(" source_mobile : ").append(this.sourceMobile)
	        		.append(" paybill : ").append(this.merchantAccount)
	        		.append(" baccount_reference : ").append(this.referenceNumber)
	        		.append(" amount : ").append(this.amount)
	        		.append(" transaction_id : ").append(this.originTransactionID)
	        		.append(" initiator_id : ").append(this.initiatorID).toString();
//	        		.append(" initiator_password : ").append(this.initiatorPassword)
//	        		.append(" organizationCode : ").append(this.organizationCode).toString();

	}

//	public String getOrganizationCode() {
//		return organizationCode;
//	}
//
//	public void setOrganizationCode(String organizationCode) {
//		this.organizationCode = organizationCode;
//	}
}

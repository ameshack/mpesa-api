-- --------------------------------------------------------------------------------
-- Routine DDL
-- Note: comments before and after the routine body will not be stored by the server
-- --------------------------------------------------------------------------------
USE `forge`;
DROP procedure IF EXISTS `update_transaction_result`;

DELIMITER $$

CREATE PROCEDURE `update_transaction_result` (IN transaction_id INTEGER, IN status_id varchar(45), IN status_desc varchar(45))
BEGIN

DECLARE EXIT HANDLER FOR NOT FOUND 

BEGIN 

	CALL log_forge_error (CONCAT('procedure name: update_transaction_result() NO RECORD FOUND FOR ',transaction_id));

END;

DECLARE EXIT HANDLER FOR SQLEXCEPTION 

BEGIN 

	CALL log_forge_error (CONCAT('procedure name: update_transaction_result() SQL EXCEPTION'));

END;

DECLARE CONTINUE HANDLER FOR SQLWARNING 

BEGIN 

	CALL log_forge_error (CONCAT('procedure name: update_transaction_result() SQL WARNING'));

END;

	UPDATE transfers SET resultStatus = status_id, resultDesc = status_desc WHERE hits_id = transaction_id LIMIT 1;

	COMMIT;

END$$
DELIMITER ;

GRANT EXECUTE ON PROCEDURE forge.update_transaction_result TO 'restb2c'@'localhost' IDENTIFIED BY 'restb2c';

FLUSH PRIVILEGES;
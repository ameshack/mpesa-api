package com.sendy.entity;


public class SendyMpesaResult {

	private String result_code;
	private String result_desc;
	private String conversation_id;
	private String mpesa_transaction_id;
	private String amount;
	private String original_trx_id;
	
	public String getResult_code() {
		return result_code;
	}
	public void setResult_code(String result_code) {
		this.result_code = result_code;
	}
	public String getResult_desc() {
		return result_desc;
	}
	public void setResult_desc(String result_desc) {
		this.result_desc = result_desc;
	}
	public String getConversation_id() {
		return conversation_id;
	}
	public void setConversation_id(String conversation_id) {
		this.conversation_id = conversation_id;
	}
	public String getMpesa_transaction_id() {
		return mpesa_transaction_id;
	}
	public void setMpesa_transaction_id(String mpesa_transaction_id) {
		this.mpesa_transaction_id = mpesa_transaction_id;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getOriginal_trx_id() {
		return original_trx_id;
	}
	public void setOriginal_trx_id(String original_trx_id) {
		this.original_trx_id = original_trx_id;
	}
}

USE `forge`;
DROP procedure IF EXISTS `log_forge_error`;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `log_forge_error`(IN error_message varchar(100))
BEGIN 

	INSERT INTO error_log (`error_date`,`error_log_message`)VALUES(NOW(),error_message);

END$$
DELIMITER ;

GRANT EXECUTE ON PROCEDURE forge.log_forge_error TO 'restb2c'@'localhost' IDENTIFIED BY 'restb2c';

FLUSH PRIVILEGES;
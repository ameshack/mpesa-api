/**
 * C2BPaymentConfirmationRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

package com.huawei.cps.cpsinterface.c2bpayment;

/**
 * C2BPaymentConfirmationRequest bean class
 */
@SuppressWarnings({ "unchecked", "unused" })
public class C2BPaymentConfirmationRequest implements
		org.apache.axis2.databinding.ADBBean {

	public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
			"http://cps.huawei.com/cpsinterface/c2bpayment",
			"C2BPaymentConfirmationRequest", "ns2");

	/**
	 * field for TransType
	 */

	protected java.lang.String localTransType;

	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTransType() {
		return localTransType;
	}

	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            TransType
	 */
	public void setTransType(java.lang.String param) {

		this.localTransType = param;

	}

	/**
	 * field for TransID
	 */

	protected java.lang.String localTransID;

	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTransID() {
		return localTransID;
	}

	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            TransID
	 */
	public void setTransID(java.lang.String param) {

		this.localTransID = param;

	}

	/**
	 * field for TransTime
	 */

	protected java.lang.String localTransTime;

	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTransTime() {
		return localTransTime;
	}

	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            TransTime
	 */
	public void setTransTime(java.lang.String param) {

		this.localTransTime = param;

	}

	/**
	 * field for TransAmount
	 */

	protected java.lang.String localTransAmount;

	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTransAmount() {
		return localTransAmount;
	}

	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            TransAmount
	 */
	public void setTransAmount(java.lang.String param) {

		this.localTransAmount = param;

	}

	/**
	 * field for BusinessShortCode
	 */

	protected java.lang.String localBusinessShortCode;

	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBusinessShortCode() {
		return localBusinessShortCode;
	}

	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            BusinessShortCode
	 */
	public void setBusinessShortCode(java.lang.String param) {

		this.localBusinessShortCode = param;

	}

	/**
	 * field for BillRefNumber
	 */

	protected java.lang.String localBillRefNumber;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBillRefNumberTracker = false;

	public boolean isBillRefNumberSpecified() {
		return localBillRefNumberTracker;
	}

	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBillRefNumber() {
		return localBillRefNumber;
	}

	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            BillRefNumber
	 */
	public void setBillRefNumber(java.lang.String param) {
		localBillRefNumberTracker = param != null;

		this.localBillRefNumber = param;

	}

	/**
	 * field for InvoiceNumber
	 */

	protected java.lang.String localInvoiceNumber;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localInvoiceNumberTracker = false;

	public boolean isInvoiceNumberSpecified() {
		return localInvoiceNumberTracker;
	}

	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getInvoiceNumber() {
		return localInvoiceNumber;
	}

	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            InvoiceNumber
	 */
	public void setInvoiceNumber(java.lang.String param) {
		localInvoiceNumberTracker = param != null;

		this.localInvoiceNumber = param;

	}

	/**
	 * field for OrgAccountBalance
	 */

	protected java.lang.String localOrgAccountBalance;

	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getOrgAccountBalance() {
		return localOrgAccountBalance;
	}

	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            OrgAccountBalance
	 */
	public void setOrgAccountBalance(java.lang.String param) {

		this.localOrgAccountBalance = param;

	}

	/**
	 * field for ThirdPartyTransID
	 */

	protected java.lang.String localThirdPartyTransID;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localThirdPartyTransIDTracker = false;

	public boolean isThirdPartyTransIDSpecified() {
		return localThirdPartyTransIDTracker;
	}

	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getThirdPartyTransID() {
		return localThirdPartyTransID;
	}

	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            ThirdPartyTransID
	 */
	public void setThirdPartyTransID(java.lang.String param) {
		localThirdPartyTransIDTracker = param != null;

		this.localThirdPartyTransID = param;

	}

	/**
	 * field for MSISDN
	 */

	protected java.lang.String localMSISDN;

	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getMSISDN() {
		return localMSISDN;
	}

	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            MSISDN
	 */
	public void setMSISDN(java.lang.String param) {

		this.localMSISDN = param;

	}

	/**
	 * field for KYCInfo This was an Array!
	 */

	protected com.huawei.cps.cpsinterface.c2bpayment.KYCInfo_type1[] localKYCInfo;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localKYCInfoTracker = false;

	public boolean isKYCInfoSpecified() {
		return localKYCInfoTracker;
	}

	/**
	 * Auto generated getter method
	 * 
	 * @return com.huawei.cps.cpsinterface.c2bpayment.KYCInfo_type1[]
	 */
	public com.huawei.cps.cpsinterface.c2bpayment.KYCInfo_type1[] getKYCInfo() {
		return localKYCInfo;
	}

	/**
	 * validate the array for KYCInfo
	 */
	protected void validateKYCInfo(
			com.huawei.cps.cpsinterface.c2bpayment.KYCInfo_type1[] param) {

	}

	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            KYCInfo
	 */
	public void setKYCInfo(
			com.huawei.cps.cpsinterface.c2bpayment.KYCInfo_type1[] param) {

		validateKYCInfo(param);

		localKYCInfoTracker = param != null;

		this.localKYCInfo = param;
	}

	/**
	 * Auto generated add method for the array for convenience
	 * 
	 * @param param
	 *            com.huawei.cps.cpsinterface.c2bpayment.KYCInfo_type1
	 */
	public void addKYCInfo(
			com.huawei.cps.cpsinterface.c2bpayment.KYCInfo_type1 param) {
		if (localKYCInfo == null) {
			localKYCInfo = new com.huawei.cps.cpsinterface.c2bpayment.KYCInfo_type1[] {};
		}

		// update the setting tracker
		localKYCInfoTracker = true;

		java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil
				.toList(localKYCInfo);
		list.add(param);
		this.localKYCInfo = (com.huawei.cps.cpsinterface.c2bpayment.KYCInfo_type1[]) list
				.toArray(new com.huawei.cps.cpsinterface.c2bpayment.KYCInfo_type1[list
						.size()]);

	}

	/**
	 *
	 * @param parentQName
	 * @param factory
	 * @return org.apache.axiom.om.OMElement
	 */
	public org.apache.axiom.om.OMElement getOMElement(
			final javax.xml.namespace.QName parentQName,
			final org.apache.axiom.om.OMFactory factory)
			throws org.apache.axis2.databinding.ADBException {

		org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(
				this, MY_QNAME);
		return factory.createOMElement(dataSource, MY_QNAME);

	}

	public void serialize(final javax.xml.namespace.QName parentQName,
			javax.xml.stream.XMLStreamWriter xmlWriter)
			throws javax.xml.stream.XMLStreamException,
			org.apache.axis2.databinding.ADBException {
		serialize(parentQName, xmlWriter, false);
	}

	public void serialize(final javax.xml.namespace.QName parentQName,
			javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
			throws javax.xml.stream.XMLStreamException,
			org.apache.axis2.databinding.ADBException {

		java.lang.String prefix = null;
		java.lang.String namespace = null;

		prefix = parentQName.getPrefix();
		namespace = parentQName.getNamespaceURI();
		writeStartElement(prefix, namespace, parentQName.getLocalPart(),
				xmlWriter);

		if (serializeType) {

			java.lang.String namespacePrefix = registerPrefix(xmlWriter,
					"http://cps.huawei.com/cpsinterface/c2bpayment");
			if ((namespacePrefix != null)
					&& (namespacePrefix.trim().length() > 0)) {
				writeAttribute("xsi",
						"http://www.w3.org/2001/XMLSchema-instance", "type",
						namespacePrefix + ":C2BPaymentConfirmationRequest",
						xmlWriter);
			} else {
				writeAttribute("xsi",
						"http://www.w3.org/2001/XMLSchema-instance", "type",
						"C2BPaymentConfirmationRequest", xmlWriter);
			}

		}

		namespace = "";
		writeStartElement(null, namespace, "TransType", xmlWriter);

		if (localTransType == null) {
			// write the nil attribute

			throw new org.apache.axis2.databinding.ADBException(
					"TransType cannot be null!!");

		} else {

			xmlWriter.writeCharacters(localTransType);

		}

		xmlWriter.writeEndElement();

		namespace = "";
		writeStartElement(null, namespace, "TransID", xmlWriter);

		if (localTransID == null) {
			// write the nil attribute

			throw new org.apache.axis2.databinding.ADBException(
					"TransID cannot be null!!");

		} else {

			xmlWriter.writeCharacters(localTransID);

		}

		xmlWriter.writeEndElement();

		namespace = "";
		writeStartElement(null, namespace, "TransTime", xmlWriter);

		if (localTransTime == null) {
			// write the nil attribute

			throw new org.apache.axis2.databinding.ADBException(
					"TransTime cannot be null!!");

		} else {

			xmlWriter.writeCharacters(localTransTime);

		}

		xmlWriter.writeEndElement();

		namespace = "";
		writeStartElement(null, namespace, "TransAmount", xmlWriter);

		if (localTransAmount == null) {
			// write the nil attribute

			throw new org.apache.axis2.databinding.ADBException(
					"TransAmount cannot be null!!");

		} else {

			xmlWriter.writeCharacters(localTransAmount);

		}

		xmlWriter.writeEndElement();

		namespace = "";
		writeStartElement(null, namespace, "BusinessShortCode", xmlWriter);

		if (localBusinessShortCode == null) {
			// write the nil attribute

			throw new org.apache.axis2.databinding.ADBException(
					"BusinessShortCode cannot be null!!");

		} else {

			xmlWriter.writeCharacters(localBusinessShortCode);

		}

		xmlWriter.writeEndElement();
		if (localBillRefNumberTracker) {
			namespace = "";
			writeStartElement(null, namespace, "BillRefNumber", xmlWriter);

			if (localBillRefNumber == null) {
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException(
						"BillRefNumber cannot be null!!");

			} else {

				xmlWriter.writeCharacters(localBillRefNumber);

			}

			xmlWriter.writeEndElement();
		}
		if (localInvoiceNumberTracker) {
			namespace = "";
			writeStartElement(null, namespace, "InvoiceNumber", xmlWriter);

			if (localInvoiceNumber == null) {
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException(
						"InvoiceNumber cannot be null!!");

			} else {

				xmlWriter.writeCharacters(localInvoiceNumber);

			}

			xmlWriter.writeEndElement();
		}
		namespace = "";
		writeStartElement(null, namespace, "OrgAccountBalance", xmlWriter);

		if (localOrgAccountBalance == null) {
			// write the nil attribute

			throw new org.apache.axis2.databinding.ADBException(
					"OrgAccountBalance cannot be null!!");

		} else {

			xmlWriter.writeCharacters(localOrgAccountBalance);

		}

		xmlWriter.writeEndElement();
		if (localThirdPartyTransIDTracker) {
			namespace = "";
			writeStartElement(null, namespace, "ThirdPartyTransID", xmlWriter);

			if (localThirdPartyTransID == null) {
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException(
						"ThirdPartyTransID cannot be null!!");

			} else {

				xmlWriter.writeCharacters(localThirdPartyTransID);

			}

			xmlWriter.writeEndElement();
		}
		namespace = "";
		writeStartElement(null, namespace, "MSISDN", xmlWriter);

		if (localMSISDN == null) {
			// write the nil attribute

			throw new org.apache.axis2.databinding.ADBException(
					"MSISDN cannot be null!!");

		} else {

			xmlWriter.writeCharacters(localMSISDN);

		}

		xmlWriter.writeEndElement();
		if (localKYCInfoTracker) {
			if (localKYCInfo != null) {
				for (int i = 0; i < localKYCInfo.length; i++) {
					if (localKYCInfo[i] != null) {
						localKYCInfo[i].serialize(
								new javax.xml.namespace.QName("", "KYCInfo"),
								xmlWriter);
					} else {

						// we don't have to do any thing since minOccures is
						// zero

					}

				}
			} else {

				throw new org.apache.axis2.databinding.ADBException(
						"KYCInfo cannot be null!!");

			}
		}
		xmlWriter.writeEndElement();

	}

	private static java.lang.String generatePrefix(java.lang.String namespace) {
		if (namespace.equals("http://cps.huawei.com/cpsinterface/c2bpayment")) {
			return "ns2";
		}
		return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
	}

	/**
	 * Utility method to write an element start tag.
	 */
	private void writeStartElement(java.lang.String prefix,
			java.lang.String namespace, java.lang.String localPart,
			javax.xml.stream.XMLStreamWriter xmlWriter)
			throws javax.xml.stream.XMLStreamException {
		java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
		if (writerPrefix != null) {
			xmlWriter.writeStartElement(namespace, localPart);
		} else {
			if (namespace.length() == 0) {
				prefix = "";
			} else if (prefix == null) {
				prefix = generatePrefix(namespace);
			}

			xmlWriter.writeStartElement(prefix, localPart, namespace);
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
		}
	}

	/**
	 * Util method to write an attribute with the ns prefix
	 */
	private void writeAttribute(java.lang.String prefix,
			java.lang.String namespace, java.lang.String attName,
			java.lang.String attValue,
			javax.xml.stream.XMLStreamWriter xmlWriter)
			throws javax.xml.stream.XMLStreamException {
		if (xmlWriter.getPrefix(namespace) == null) {
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
		}
		xmlWriter.writeAttribute(namespace, attName, attValue);
	}

	/**
	 * Util method to write an attribute without the ns prefix
	 */
	private void writeAttribute(java.lang.String namespace,
			java.lang.String attName, java.lang.String attValue,
			javax.xml.stream.XMLStreamWriter xmlWriter)
			throws javax.xml.stream.XMLStreamException {
		if (namespace.equals("")) {
			xmlWriter.writeAttribute(attName, attValue);
		} else {
			registerPrefix(xmlWriter, namespace);
			xmlWriter.writeAttribute(namespace, attName, attValue);
		}
	}

	/**
	 * Util method to write an attribute without the ns prefix
	 */
	private void writeQNameAttribute(java.lang.String namespace,
			java.lang.String attName, javax.xml.namespace.QName qname,
			javax.xml.stream.XMLStreamWriter xmlWriter)
			throws javax.xml.stream.XMLStreamException {

		java.lang.String attributeNamespace = qname.getNamespaceURI();
		java.lang.String attributePrefix = xmlWriter
				.getPrefix(attributeNamespace);
		if (attributePrefix == null) {
			attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
		}
		java.lang.String attributeValue;
		if (attributePrefix.trim().length() > 0) {
			attributeValue = attributePrefix + ":" + qname.getLocalPart();
		} else {
			attributeValue = qname.getLocalPart();
		}

		if (namespace.equals("")) {
			xmlWriter.writeAttribute(attName, attributeValue);
		} else {
			registerPrefix(xmlWriter, namespace);
			xmlWriter.writeAttribute(namespace, attName, attributeValue);
		}
	}

	/**
	 * method to handle Qnames
	 */

	private void writeQName(javax.xml.namespace.QName qname,
			javax.xml.stream.XMLStreamWriter xmlWriter)
			throws javax.xml.stream.XMLStreamException {
		java.lang.String namespaceURI = qname.getNamespaceURI();
		if (namespaceURI != null) {
			java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
			if (prefix == null) {
				prefix = generatePrefix(namespaceURI);
				xmlWriter.writeNamespace(prefix, namespaceURI);
				xmlWriter.setPrefix(prefix, namespaceURI);
			}

			if (prefix.trim().length() > 0) {
				xmlWriter.writeCharacters(prefix
						+ ":"
						+ org.apache.axis2.databinding.utils.ConverterUtil
								.convertToString(qname));
			} else {
				// i.e this is the default namespace
				xmlWriter
						.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
								.convertToString(qname));
			}

		} else {
			xmlWriter
					.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
							.convertToString(qname));
		}
	}

	private void writeQNames(javax.xml.namespace.QName[] qnames,
			javax.xml.stream.XMLStreamWriter xmlWriter)
			throws javax.xml.stream.XMLStreamException {

		if (qnames != null) {
			// we have to store this data until last moment since it is not
			// possible to write any
			// namespace data after writing the charactor data
			java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
			java.lang.String namespaceURI = null;
			java.lang.String prefix = null;

			for (int i = 0; i < qnames.length; i++) {
				if (i > 0) {
					stringToWrite.append(" ");
				}
				namespaceURI = qnames[i].getNamespaceURI();
				if (namespaceURI != null) {
					prefix = xmlWriter.getPrefix(namespaceURI);
					if ((prefix == null) || (prefix.length() == 0)) {
						prefix = generatePrefix(namespaceURI);
						xmlWriter.writeNamespace(prefix, namespaceURI);
						xmlWriter.setPrefix(prefix, namespaceURI);
					}

					if (prefix.trim().length() > 0) {
						stringToWrite
								.append(prefix)
								.append(":")
								.append(org.apache.axis2.databinding.utils.ConverterUtil
										.convertToString(qnames[i]));
					} else {
						stringToWrite
								.append(org.apache.axis2.databinding.utils.ConverterUtil
										.convertToString(qnames[i]));
					}
				} else {
					stringToWrite
							.append(org.apache.axis2.databinding.utils.ConverterUtil
									.convertToString(qnames[i]));
				}
			}
			xmlWriter.writeCharacters(stringToWrite.toString());
		}

	}

	/**
	 * Register a namespace prefix
	 */
	private java.lang.String registerPrefix(
			javax.xml.stream.XMLStreamWriter xmlWriter,
			java.lang.String namespace)
			throws javax.xml.stream.XMLStreamException {
		java.lang.String prefix = xmlWriter.getPrefix(namespace);
		if (prefix == null) {
			prefix = generatePrefix(namespace);
			javax.xml.namespace.NamespaceContext nsContext = xmlWriter
					.getNamespaceContext();
			while (true) {
				java.lang.String uri = nsContext.getNamespaceURI(prefix);
				if (uri == null || uri.length() == 0) {
					break;
				}
				prefix = org.apache.axis2.databinding.utils.BeanUtil
						.getUniquePrefix();
			}
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
		}
		return prefix;
	}

	/**
	 * databinding method to get an XML representation of this object
	 *
	 */
	public javax.xml.stream.XMLStreamReader getPullParser(
			javax.xml.namespace.QName qName)
			throws org.apache.axis2.databinding.ADBException {

		java.util.ArrayList elementList = new java.util.ArrayList();
		java.util.ArrayList attribList = new java.util.ArrayList();

		elementList.add(new javax.xml.namespace.QName("", "TransType"));

		if (localTransType != null) {
			elementList.add(org.apache.axis2.databinding.utils.ConverterUtil
					.convertToString(localTransType));
		} else {
			throw new org.apache.axis2.databinding.ADBException(
					"TransType cannot be null!!");
		}

		elementList.add(new javax.xml.namespace.QName("", "TransID"));

		if (localTransID != null) {
			elementList.add(org.apache.axis2.databinding.utils.ConverterUtil
					.convertToString(localTransID));
		} else {
			throw new org.apache.axis2.databinding.ADBException(
					"TransID cannot be null!!");
		}

		elementList.add(new javax.xml.namespace.QName("", "TransTime"));

		if (localTransTime != null) {
			elementList.add(org.apache.axis2.databinding.utils.ConverterUtil
					.convertToString(localTransTime));
		} else {
			throw new org.apache.axis2.databinding.ADBException(
					"TransTime cannot be null!!");
		}

		elementList.add(new javax.xml.namespace.QName("", "TransAmount"));

		if (localTransAmount != null) {
			elementList.add(org.apache.axis2.databinding.utils.ConverterUtil
					.convertToString(localTransAmount));
		} else {
			throw new org.apache.axis2.databinding.ADBException(
					"TransAmount cannot be null!!");
		}

		elementList.add(new javax.xml.namespace.QName("", "BusinessShortCode"));

		if (localBusinessShortCode != null) {
			elementList.add(org.apache.axis2.databinding.utils.ConverterUtil
					.convertToString(localBusinessShortCode));
		} else {
			throw new org.apache.axis2.databinding.ADBException(
					"BusinessShortCode cannot be null!!");
		}
		if (localBillRefNumberTracker) {
			elementList.add(new javax.xml.namespace.QName("", "BillRefNumber"));

			if (localBillRefNumber != null) {
				elementList
						.add(org.apache.axis2.databinding.utils.ConverterUtil
								.convertToString(localBillRefNumber));
			} else {
				throw new org.apache.axis2.databinding.ADBException(
						"BillRefNumber cannot be null!!");
			}
		}
		if (localInvoiceNumberTracker) {
			elementList.add(new javax.xml.namespace.QName("", "InvoiceNumber"));

			if (localInvoiceNumber != null) {
				elementList
						.add(org.apache.axis2.databinding.utils.ConverterUtil
								.convertToString(localInvoiceNumber));
			} else {
				throw new org.apache.axis2.databinding.ADBException(
						"InvoiceNumber cannot be null!!");
			}
		}
		elementList.add(new javax.xml.namespace.QName("", "OrgAccountBalance"));

		if (localOrgAccountBalance != null) {
			elementList.add(org.apache.axis2.databinding.utils.ConverterUtil
					.convertToString(localOrgAccountBalance));
		} else {
			throw new org.apache.axis2.databinding.ADBException(
					"OrgAccountBalance cannot be null!!");
		}
		if (localThirdPartyTransIDTracker) {
			elementList.add(new javax.xml.namespace.QName("",
					"ThirdPartyTransID"));

			if (localThirdPartyTransID != null) {
				elementList
						.add(org.apache.axis2.databinding.utils.ConverterUtil
								.convertToString(localThirdPartyTransID));
			} else {
				throw new org.apache.axis2.databinding.ADBException(
						"ThirdPartyTransID cannot be null!!");
			}
		}
		elementList.add(new javax.xml.namespace.QName("", "MSISDN"));

		if (localMSISDN != null) {
			elementList.add(org.apache.axis2.databinding.utils.ConverterUtil
					.convertToString(localMSISDN));
		} else {
			throw new org.apache.axis2.databinding.ADBException(
					"MSISDN cannot be null!!");
		}
		if (localKYCInfoTracker) {
			if (localKYCInfo != null) {
				for (int i = 0; i < localKYCInfo.length; i++) {

					if (localKYCInfo[i] != null) {
						elementList.add(new javax.xml.namespace.QName("",
								"KYCInfo"));
						elementList.add(localKYCInfo[i]);
					} else {

						// nothing to do

					}

				}
			} else {

				throw new org.apache.axis2.databinding.ADBException(
						"KYCInfo cannot be null!!");

			}

		}

		return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(
				qName, elementList.toArray(), attribList.toArray());

	}

	/**
	 * Factory class that keeps the parse method
	 */
	public static class Factory {

		/**
		 * static method to create the object Precondition: If this object is an
		 * element, the current or next start element starts this object and any
		 * intervening reader events are ignorable If this object is not an
		 * element, it is a complex type and the reader is at the event just
		 * after the outer start element Postcondition: If this object is an
		 * element, the reader is positioned at its end element If this object
		 * is a complex type, the reader is positioned at the end element of its
		 * outer element
		 */
		public static C2BPaymentConfirmationRequest parse(
				javax.xml.stream.XMLStreamReader reader)
				throws java.lang.Exception {
			C2BPaymentConfirmationRequest object = new C2BPaymentConfirmationRequest();

			int event;
			java.lang.String nillableValue = null;
			java.lang.String prefix = "";
			java.lang.String namespaceuri = "";
			try {

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.getAttributeValue(
						"http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
					java.lang.String fullTypeName = reader
							.getAttributeValue(
									"http://www.w3.org/2001/XMLSchema-instance",
									"type");
					if (fullTypeName != null) {
						java.lang.String nsPrefix = null;
						if (fullTypeName.indexOf(":") > -1) {
							nsPrefix = fullTypeName.substring(0,
									fullTypeName.indexOf(":"));
						}
						nsPrefix = nsPrefix == null ? "" : nsPrefix;

						java.lang.String type = fullTypeName
								.substring(fullTypeName.indexOf(":") + 1);

						if (!"C2BPaymentConfirmationRequest".equals(type)) {
							// find namespace for the prefix
							java.lang.String nsUri = reader
									.getNamespaceContext().getNamespaceURI(
											nsPrefix);
							return (C2BPaymentConfirmationRequest) com.huawei.cps.cpsinterface.c2bpayment.ExtensionMapper
									.getTypeObject(nsUri, type, reader);
						}

					}

				}

				// Note all attributes that were handled. Used to differ normal
				// attributes
				// from anyAttributes.
				java.util.Vector handledAttributes = new java.util.Vector();

				reader.next();

				java.util.ArrayList list11 = new java.util.ArrayList();

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement()
						&& new javax.xml.namespace.QName("", "TransType")
								.equals(reader.getName())) {

					nillableValue = reader.getAttributeValue(
							"http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue)
							|| "1".equals(nillableValue)) {
						throw new org.apache.axis2.databinding.ADBException(
								"The element: " + "TransType"
										+ "  cannot be null");
					}

					java.lang.String content = reader.getElementText();

					object.setTransType(org.apache.axis2.databinding.utils.ConverterUtil
							.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else {
					// A start element we are not expecting indicates an invalid
					// parameter was passed
					throw new org.apache.axis2.databinding.ADBException(
							"Unexpected subelement " + reader.getName());
				}

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement()
						&& new javax.xml.namespace.QName("", "TransID")
								.equals(reader.getName())) {

					nillableValue = reader.getAttributeValue(
							"http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue)
							|| "1".equals(nillableValue)) {
						throw new org.apache.axis2.databinding.ADBException(
								"The element: " + "TransID"
										+ "  cannot be null");
					}

					java.lang.String content = reader.getElementText();

					object.setTransID(org.apache.axis2.databinding.utils.ConverterUtil
							.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else {
					// A start element we are not expecting indicates an invalid
					// parameter was passed
					throw new org.apache.axis2.databinding.ADBException(
							"Unexpected subelement " + reader.getName());
				}

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement()
						&& new javax.xml.namespace.QName("", "TransTime")
								.equals(reader.getName())) {

					nillableValue = reader.getAttributeValue(
							"http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue)
							|| "1".equals(nillableValue)) {
						throw new org.apache.axis2.databinding.ADBException(
								"The element: " + "TransTime"
										+ "  cannot be null");
					}

					java.lang.String content = reader.getElementText();

					object.setTransTime(org.apache.axis2.databinding.utils.ConverterUtil
							.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else {
					// A start element we are not expecting indicates an invalid
					// parameter was passed
					throw new org.apache.axis2.databinding.ADBException(
							"Unexpected subelement " + reader.getName());
				}

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement()
						&& new javax.xml.namespace.QName("", "TransAmount")
								.equals(reader.getName())) {

					nillableValue = reader.getAttributeValue(
							"http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue)
							|| "1".equals(nillableValue)) {
						throw new org.apache.axis2.databinding.ADBException(
								"The element: " + "TransAmount"
										+ "  cannot be null");
					}

					java.lang.String content = reader.getElementText();

					object.setTransAmount(org.apache.axis2.databinding.utils.ConverterUtil
							.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else {
					// A start element we are not expecting indicates an invalid
					// parameter was passed
					throw new org.apache.axis2.databinding.ADBException(
							"Unexpected subelement " + reader.getName());
				}

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement()
						&& new javax.xml.namespace.QName("",
								"BusinessShortCode").equals(reader.getName())) {

					nillableValue = reader.getAttributeValue(
							"http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue)
							|| "1".equals(nillableValue)) {
						throw new org.apache.axis2.databinding.ADBException(
								"The element: " + "BusinessShortCode"
										+ "  cannot be null");
					}

					java.lang.String content = reader.getElementText();

					object.setBusinessShortCode(org.apache.axis2.databinding.utils.ConverterUtil
							.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else {
					// A start element we are not expecting indicates an invalid
					// parameter was passed
					throw new org.apache.axis2.databinding.ADBException(
							"Unexpected subelement " + reader.getName());
				}

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement()
						&& new javax.xml.namespace.QName("", "BillRefNumber")
								.equals(reader.getName())) {

					nillableValue = reader.getAttributeValue(
							"http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue)
							|| "1".equals(nillableValue)) {
						throw new org.apache.axis2.databinding.ADBException(
								"The element: " + "BillRefNumber"
										+ "  cannot be null");
					}

					java.lang.String content = reader.getElementText();

					object.setBillRefNumber(org.apache.axis2.databinding.utils.ConverterUtil
							.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else {

				}

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement()
						&& new javax.xml.namespace.QName("", "InvoiceNumber")
								.equals(reader.getName())) {

					nillableValue = reader.getAttributeValue(
							"http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue)
							|| "1".equals(nillableValue)) {
						throw new org.apache.axis2.databinding.ADBException(
								"The element: " + "InvoiceNumber"
										+ "  cannot be null");
					}

					java.lang.String content = reader.getElementText();

					object.setInvoiceNumber(org.apache.axis2.databinding.utils.ConverterUtil
							.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else {

				}

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement()
						&& new javax.xml.namespace.QName("",
								"OrgAccountBalance").equals(reader.getName())) {

					nillableValue = reader.getAttributeValue(
							"http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue)
							|| "1".equals(nillableValue)) {
						throw new org.apache.axis2.databinding.ADBException(
								"The element: " + "OrgAccountBalance"
										+ "  cannot be null");
					}

					java.lang.String content = reader.getElementText();

					object.setOrgAccountBalance(org.apache.axis2.databinding.utils.ConverterUtil
							.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else {
					// A start element we are not expecting indicates an invalid
					// parameter was passed
					throw new org.apache.axis2.databinding.ADBException(
							"Unexpected subelement " + reader.getName());
				}

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement()
						&& new javax.xml.namespace.QName("",
								"ThirdPartyTransID").equals(reader.getName())) {

					nillableValue = reader.getAttributeValue(
							"http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue)
							|| "1".equals(nillableValue)) {
						throw new org.apache.axis2.databinding.ADBException(
								"The element: " + "ThirdPartyTransID"
										+ "  cannot be null");
					}

					java.lang.String content = reader.getElementText();

					object.setThirdPartyTransID(org.apache.axis2.databinding.utils.ConverterUtil
							.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else {

				}

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement()
						&& new javax.xml.namespace.QName("", "MSISDN")
								.equals(reader.getName())) {

					nillableValue = reader.getAttributeValue(
							"http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue)
							|| "1".equals(nillableValue)) {
						throw new org.apache.axis2.databinding.ADBException(
								"The element: " + "MSISDN" + "  cannot be null");
					}

					java.lang.String content = reader.getElementText();

					object.setMSISDN(org.apache.axis2.databinding.utils.ConverterUtil
							.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else {
					// A start element we are not expecting indicates an invalid
					// parameter was passed
					throw new org.apache.axis2.databinding.ADBException(
							"Unexpected subelement " + reader.getName());
				}

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement()
						&& new javax.xml.namespace.QName("", "KYCInfo")
								.equals(reader.getName())) {

					// Process the array and step past its final element's end.
					list11.add(com.huawei.cps.cpsinterface.c2bpayment.KYCInfo_type1.Factory
							.parse(reader));

					// loop until we find a start element that is not part of
					// this array
					boolean loopDone11 = false;
					while (!loopDone11) {
						// We should be at the end element, but make sure
						while (!reader.isEndElement())
							reader.next();
						// Step out of this element
						reader.next();
						// Step to next element event.
						while (!reader.isStartElement()
								&& !reader.isEndElement())
							reader.next();
						if (reader.isEndElement()) {
							// two continuous end elements means we are exiting
							// the xml structure
							loopDone11 = true;
						} else {
							if (new javax.xml.namespace.QName("", "KYCInfo")
									.equals(reader.getName())) {
								list11.add(com.huawei.cps.cpsinterface.c2bpayment.KYCInfo_type1.Factory
										.parse(reader));

							} else {
								loopDone11 = true;
							}
						}
					}
					// call the converter utility to convert and set the array

					object.setKYCInfo((com.huawei.cps.cpsinterface.c2bpayment.KYCInfo_type1[]) org.apache.axis2.databinding.utils.ConverterUtil
							.convertToArray(
									com.huawei.cps.cpsinterface.c2bpayment.KYCInfo_type1.class,
									list11));

				} // End of if for expected property start element

				else {

				}

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement())
					// A start element we are not expecting indicates a trailing
					// invalid property
					throw new org.apache.axis2.databinding.ADBException(
							"Unexpected subelement " + reader.getName());

			} catch (javax.xml.stream.XMLStreamException e) {
				throw new java.lang.Exception(e);
			}

			return object;
		}

	}// end of factory class

}

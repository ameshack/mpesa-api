package com.ars.c2b.checkout;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import ns.ProcessCheckOutResponse;
import ns.TransactionConfirmResponse;
import ns.TransactionStatusResponse;
import ke.co.ars.dao.MpesaResultDAO;
import ke.co.ars.dao.TransferB2CDAO;
import ke.co.ars.entity.C2BCheckoutRequest;
import ke.co.ars.entity.MpesaC2BCheckoutResult;
import ke.co.ars.entity.Transfer;
import ke.co.ars.entity.TrxRequest;
import ke.co.ars.entity.TrxResponse;

public class CheckoutController {
	
	/* Get actual class name to be printed on */
    static Logger log = Logger.getLogger(CheckoutController.class.getName());
    
    public CheckoutController() {
    	//PropertiesConfigurator is used to configure logger from properties file
        PropertyConfigurator.configure("E:\\log4j\\mpesalog4j.properties");
    }

	public TrxResponse processC2BCheckout(String checkoutMsisdn,String checkoutWalletType,String checkoutCountryCode,
			String checkoutCurrenyCode,String checkoutOrigTransactionID,
			double checkoutReqAmount,int checkoutOrigID,
			String checkoutOrigSource,String checkoutMerchantAccount,
			String checkoutRefNumber) {
		
		String serverName0 = "SRV"+checkoutWalletType+"0"+checkoutOrigID;

		log.info("Initiating processC2BCheckout request for ....." + serverName0);
		
		Transfer c2bCheckoutTransfer = new Transfer();
		
		c2bCheckoutTransfer.setServerName(serverName0);
		c2bCheckoutTransfer.setTransactionType("C2BOnlineCheckout");
		c2bCheckoutTransfer.setDestAccount(checkoutMerchantAccount);
		c2bCheckoutTransfer.setAmount(String.valueOf(checkoutReqAmount));
		c2bCheckoutTransfer.setTrxCode(checkoutOrigTransactionID);
		c2bCheckoutTransfer.setSourceMSISDN(checkoutMsisdn);
		c2bCheckoutTransfer.setOrigID(checkoutOrigID);
		c2bCheckoutTransfer.setOrig(checkoutOrigSource);
		c2bCheckoutTransfer.setAccountReference(checkoutRefNumber);
		
		log.info("Getting API details for " + serverName0);
		
		TrxRequest c2bCheckoutTrxRequest = new TrxRequest();
		
		TransferB2CDAO c2bCheckoutTransferDAO = new TransferB2CDAO();
		
		c2bCheckoutTrxRequest = c2bCheckoutTransferDAO.logTransaction(c2bCheckoutTransfer);
		
		//send request to SAG
		C2BCheckoutRequest c2BCheckoutRequest1 = new C2BCheckoutRequest();
		
		c2BCheckoutRequest1.setAmount(checkoutReqAmount);
		c2BCheckoutRequest1.setCallbackMethod(c2bCheckoutTrxRequest.getCallbackMethod());
		c2BCheckoutRequest1.setCallbackURL(c2bCheckoutTrxRequest.getCallbackURL());
		c2BCheckoutRequest1.setEncryptedData(null);
		c2BCheckoutRequest1.setMerchantId(c2bCheckoutTrxRequest.getMerchantCode());
		c2BCheckoutRequest1.setMerchantPassword(c2bCheckoutTrxRequest.getApiPasswd());
		c2BCheckoutRequest1.setMerchantTransactionId(c2bCheckoutTrxRequest.getTransactionID());
		c2BCheckoutRequest1.setMsisdn(checkoutMsisdn);
		c2BCheckoutRequest1.setReferenceId(checkoutRefNumber);
		c2BCheckoutRequest1.setRequestTimeout(c2bCheckoutTrxRequest.getTimeout());
		c2BCheckoutRequest1.setSagULR(c2bCheckoutTrxRequest.getApiUrl());
		
		CheckoutClient c2bCheckoutClient0 = new CheckoutClient();
		
		ProcessCheckOutResponse c2bCheckoutProcessCheckOutResponse0 = new ProcessCheckOutResponse();
		
		c2bCheckoutProcessCheckOutResponse0 = c2bCheckoutClient0.sendMpesaC2BCheckout(c2BCheckoutRequest1);
		
		TrxResponse c2bCheckoutTrxResponse0 = new TrxResponse();
		
		//process SAG response
		c2bCheckoutTrxResponse0.setStatusCode(Integer.valueOf(c2bCheckoutProcessCheckOutResponse0.getRETURN_CODE()));
		c2bCheckoutTrxResponse0.setStatusDescription(c2bCheckoutProcessCheckOutResponse0.getDESCRIPTION());
		c2bCheckoutTrxResponse0.setTraceNumber(c2bCheckoutProcessCheckOutResponse0.getTRX_ID());
		c2bCheckoutTrxResponse0.setTransactionData(c2bCheckoutProcessCheckOutResponse0.getCUST_MSG());
		c2bCheckoutTrxResponse0.setTransactionID(c2bCheckoutTrxRequest.getTransactionID());
		c2bCheckoutTrxResponse0.setMsisdn(checkoutMsisdn);
		
		c2bCheckoutTransferDAO.updateTransactionResponse(c2bCheckoutTrxResponse0);

		return c2bCheckoutTrxResponse0;
	}
	
	public TrxResponse processC2BCheckoutConfirmation(String confirmWalletType,String confirmOrigTransactionID,
			int confirmOrigID,String confirmOrigSource) {
		
		log.info("Initiating processC2BCheckoutConfirmation request.....");
		
		String serverName1 = "SRV"+confirmWalletType+"0"+confirmOrigID;
		
		Transfer confirmCheckoutTransfer = new Transfer();
		
		confirmCheckoutTransfer.setServerName(serverName1);
		confirmCheckoutTransfer.setTrxCode(confirmOrigTransactionID);
		confirmCheckoutTransfer.setOrigID(confirmOrigID);
		confirmCheckoutTransfer.setOrig(confirmOrigSource);
		
		log.info("Getting API details for " + serverName1);
		
		//log request details to DB
		
		TrxRequest c2bConfirmCheckoutTrxRequest = new TrxRequest();
		
		TransferB2CDAO c2bConfirmCheckoutTransferDAO = new TransferB2CDAO();
		
		c2bConfirmCheckoutTrxRequest = c2bConfirmCheckoutTransferDAO.getC2BCheckoutRequest(confirmCheckoutTransfer);
		
		//send request to SAG
		C2BCheckoutRequest c2BConfirmCheckoutRequest1 = new C2BCheckoutRequest();
		
		c2BConfirmCheckoutRequest1.setMerchantId(c2bConfirmCheckoutTrxRequest.getMerchantCode());
		c2BConfirmCheckoutRequest1.setMerchantPassword(c2bConfirmCheckoutTrxRequest.getApiPasswd());
		c2BConfirmCheckoutRequest1.setMerchantTransactionId(c2bConfirmCheckoutTrxRequest.getTraceNumber());
		c2BConfirmCheckoutRequest1.setMsisdn(c2bConfirmCheckoutTrxRequest.getMsisdn());
		c2BConfirmCheckoutRequest1.setRequestTimeout(c2bConfirmCheckoutTrxRequest.getTimeout());
		c2BConfirmCheckoutRequest1.setSagULR(c2bConfirmCheckoutTrxRequest.getApiUrl());
		
		CheckoutClient c2bCheckoutClient1 = new CheckoutClient();
		
		TransactionConfirmResponse c2bTransactionConfirmResponse0 = new TransactionConfirmResponse(); 
		
		c2bTransactionConfirmResponse0 = c2bCheckoutClient1.sendConfirmC2BPayment(c2BConfirmCheckoutRequest1);
		
		TrxResponse c2bConfirmCheckoutTrxResponse0 = new TrxResponse();
		
		//process SAG response
		c2bConfirmCheckoutTrxResponse0.setTransactionID(c2bConfirmCheckoutTrxRequest.getTransactionID());
		c2bConfirmCheckoutTrxResponse0.setStatusCode(Integer.valueOf(c2bTransactionConfirmResponse0.getRETURN_CODE()));
		c2bConfirmCheckoutTrxResponse0.setStatusDescription(c2bTransactionConfirmResponse0.getDESCRIPTION());
		c2bConfirmCheckoutTrxResponse0.setTraceNumber(c2bTransactionConfirmResponse0.getTRX_ID());
		c2bConfirmCheckoutTrxResponse0.setMsisdn(c2bConfirmCheckoutTrxRequest.getMsisdn());
		
		c2bConfirmCheckoutTransferDAO.updateC2BCheckoutConfirmationResponse(c2bConfirmCheckoutTrxResponse0);
		
		return c2bConfirmCheckoutTrxResponse0;
	}
	
	public MpesaC2BCheckoutResult processC2BQueryTransaction(String queryWalletType,String queryOrigTransactionID,
			int queryOrigID,String queryOrigSource) {
		
		log.info("Initiating processC2BQueryTransaction request.....");
		
		String serverName2 = "SRV"+queryWalletType+"0"+queryOrigID;
		
		Transfer queryC2BCheckoutTransferStatus = new Transfer();
		
		queryC2BCheckoutTransferStatus.setServerName(serverName2);
		queryC2BCheckoutTransferStatus.setTrxCode(queryOrigTransactionID);
		queryC2BCheckoutTransferStatus.setOrigID(queryOrigID);
		queryC2BCheckoutTransferStatus.setOrig(queryOrigSource);
		
		log.info("Getting API details for " + serverName2);
		
		//log request details to DB
		
		TrxRequest c2bQueryCheckoutTrxStatus = new TrxRequest();
				
		TransferB2CDAO c2bQueryCheckoutTransferStatusDAO = new TransferB2CDAO();
				
		c2bQueryCheckoutTrxStatus = c2bQueryCheckoutTransferStatusDAO.getC2BCheckoutRequest(queryC2BCheckoutTransferStatus);
				
		//send request to SAG
		C2BCheckoutRequest c2BConfirmCheckoutRequest2 = new C2BCheckoutRequest();
				
		c2BConfirmCheckoutRequest2.setMerchantId(c2bQueryCheckoutTrxStatus.getMerchantCode());
		c2BConfirmCheckoutRequest2.setMerchantPassword(c2bQueryCheckoutTrxStatus.getApiPasswd());
		c2BConfirmCheckoutRequest2.setMerchantTransactionId(c2bQueryCheckoutTrxStatus.getTransactionID());
		c2BConfirmCheckoutRequest2.setSagTransactionId(c2bQueryCheckoutTrxStatus.getTraceNumber());
		c2BConfirmCheckoutRequest2.setRequestTimeout(c2bQueryCheckoutTrxStatus.getTimeout());
		c2BConfirmCheckoutRequest2.setSagULR(c2bQueryCheckoutTrxStatus.getApiUrl());
		
		CheckoutClient c2bCheckoutClient2 = new CheckoutClient();
		
		TransactionStatusResponse c2bQueryCheckoutTransactionStatusResponse = new TransactionStatusResponse();
		
		c2bQueryCheckoutTransactionStatusResponse = c2bCheckoutClient2.sendQueryMpesaC2BTransactionStatus(c2BConfirmCheckoutRequest2);
		
		MpesaC2BCheckoutResult mpesaC2BCheckoutResult1 = new MpesaC2BCheckoutResult();
		
		mpesaC2BCheckoutResult1.setAmount(c2bQueryCheckoutTransactionStatusResponse.getAMOUNT());
		mpesaC2BCheckoutResult1.setDescription(c2bQueryCheckoutTransactionStatusResponse.getDESCRIPTION());
		mpesaC2BCheckoutResult1.setEncParams(c2bQueryCheckoutTransactionStatusResponse.getENC_PARAMS());
		mpesaC2BCheckoutResult1.setMerchantTrxId(c2bQueryCheckoutTransactionStatusResponse.getMERCHANT_TRANSACTION_ID());
		mpesaC2BCheckoutResult1.setMsisdn(c2bQueryCheckoutTransactionStatusResponse.getMSISDN());
		mpesaC2BCheckoutResult1.setReturnCode(c2bQueryCheckoutTransactionStatusResponse.getRETURN_CODE());
		mpesaC2BCheckoutResult1.setTrxDate(c2bQueryCheckoutTransactionStatusResponse.getMPESA_TRX_DATE());
		mpesaC2BCheckoutResult1.setTrxStatus(c2bQueryCheckoutTransactionStatusResponse.getTRX_STATUS());
		mpesaC2BCheckoutResult1.setMpesaTrxId(c2bQueryCheckoutTransactionStatusResponse.getMPESA_TRX_ID());
		mpesaC2BCheckoutResult1.setTrxId(c2bQueryCheckoutTransactionStatusResponse.getTRX_ID());
		
		MpesaResultDAO c2bQueryTrxStatusMpesaResultDAO = new MpesaResultDAO();
		
		c2bQueryTrxStatusMpesaResultDAO.logMpesaC2BCheckoutResult(mpesaC2BCheckoutResult1);
		
		return mpesaC2BCheckoutResult1;
	}
}


/**
 * C2BPaymentValidationAndConfirmationServiceSkeletonInterface.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
    package com.huawei.cps.cpsinterface.c2bpayment;
    /**
     *  C2BPaymentValidationAndConfirmationServiceSkeletonInterface java skeleton interface for the axisService
     */
    public interface C2BPaymentValidationAndConfirmationServiceSkeletonInterface {
     
         
        /**
         * Auto generated method signature
         * 
                                    * @param c2BPaymentConfirmationRequest
         */

        
                public com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentConfirmationResult confirmC2BPayment
                (
                  com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentConfirmationRequest c2BPaymentConfirmationRequest
                 )
            ;
        
         
        /**
         * Auto generated method signature
         * 
                                    * @param c2BPaymentValidationRequest
         */

        
                public com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentValidationResult validateC2BPayment
                (
                  com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentValidationRequest c2BPaymentValidationRequest
                 )
            ;
        
         }
    
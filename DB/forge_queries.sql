ALTER TABLE `forge`.`external_account` 
CHANGE COLUMN `account_id` `mobile_account_id` VARCHAR(45) NOT NULL DEFAULT '0' ;

ALTER TABLE `forge`.`external_account` 
ADD COLUMN `float_bank_acc_id` VARCHAR(20) NULL DEFAULT 0 AFTER `msisdn`,
ADD COLUMN `comm_bank_acc_id` VARCHAR(20) NULL AFTER `float_bank_acc_id`;


ALTER TABLE `forge`.`transfers` 
CHANGE COLUMN `transactionDate` `transactionDate` VARCHAR(45) NULL DEFAULT 'NOW()' ;

17/01/2015
ALTER TABLE `forge`.`transfers` 
CHANGE COLUMN `responseStatus` `responseStatus` VARCHAR(45) NULL DEFAULT '100' ,
CHANGE COLUMN `resultStatus` `resultStatus` VARCHAR(45) NULL DEFAULT '100' ;


UPDATE mpesa_api SET sp_password = AES_ENCRYPT('sp_password', SHA1('@ut0b073')), caller_password = AES_ENCRYPT('caller_password', SHA1('@ut0b073')),initiator_password = AES_ENCRYPT('initiator_password', SHA1('@ut0b073')) WHERE id = 1;

commit;

05/03/2015
ALTER TABLE `forge`.`node` 
CHANGE COLUMN `password` `node_passwd` VARBINARY(45) NULL DEFAULT NULL ;

ALTER TABLE `forge`.`node` 
CHANGE COLUMN `node_passwd` `node_passwd` VARBINARY(45) NULL DEFAULT NULL ;

ALTER TABLE `forge`.`mpesa_api` 
CHANGE COLUMN `sp_password` `sp_password` VARBINARY(45) NOT NULL ,
CHANGE COLUMN `caller_password` `caller_password` VARBINARY(45) NULL DEFAULT NULL ,
CHANGE COLUMN `initiator_password` `initiator_password` VARBINARY(45) NOT NULL ;



ALTER TABLE `forge`.`external_account` 
CHANGE COLUMN `mobile_account_id` `account_id` VARCHAR(45) NOT NULL DEFAULT '0' ;



INSERT INTO `forge`.`node`
(`channel_id`,`external_account_id`,`name`,`host_ip`,`host_port`,`timeout`,`username`,`node_passwd`,`url`,`credentials_required`,`status`,`status_detail`)
VALUES
(1,1,'SRVMPESAKE01','196.201.214.136','8310','30000','','','','',0,'OK');


INSERT INTO `forge`.`mpesa_api`
(`node_id`,`idapi_account`,`sp_id`,`sp_password`,`service_id`,`command_id`,`language_code`,`queue_timeout_url`,`third_party_id`,`caller_password`,`check_sum`,`request_url`,`result_url`,`query_trx_url`,`identifier_type`,`identifier`,`initiator_password`,`caller_type`,`short_code`,`primary_party_id`,`receiver_id`,`access_id`,`access_identifier`,`key_owner`,`key_store`,`key_store_password`,`trust_store`,`trust_store_password`)
VALUES
(2,4,'100250',AES_ENCRYPT('!QAZ2wsx34',SHA1('@ut0b073')),'100250006','BusinessPayment','LanguageCode0','http://10.54.6.33:44004/axis2/services/QueueTimeoutNotificationService','ThirdPartyID0',caller_password = AES_ENCRYPT('caller_password', SHA1('@ut0b073')),'CheckSum0','http://196.201.214.136:8310/mminterface/request','http://10.54.6.33:44004/axis2/services/ResultMgrService','http://196.201.214.136:8310/queryTransactionService/services/transaction',11,'PoaPayInit',initiator_password = AES_ENCRYPT('Orange!2015', SHA1('@ut0b073')),2,'160161',4,1,1,'Identifier3',1,'/usr/java/jdk1.7.0_45/jre/lib/security/wolverine.jks','changeit','/usr/java/jdk1.7.0_45/jre/lib/security/cacerts','changeit');

------------C2B--------------------


INSERT INTO `forge`.`node`
(`channel_id`,`external_account_id`,`name`,`host_ip`,`host_port`,`timeout`,`username`,`node_passwd`,`url`,`credentials_required`,`status`,`status_detail`,`call_back_url`,`call_back_method`)
VALUES
(1,1,'SRVSAGC2BKE01','127.0.0.1','8310','30000','898945',AES_ENCRYPT('TEST',SHA1('@ut0b073')),'http://sagurl.com','',0,'OK','http://cpurl.com','cp_method_name');


INSERT INTO `forge`.`mpesa_api`
(`node_id`,`idapi_account`,`sp_id`,`sp_password`,`service_id`,`command_id`,`language_code`,`queue_timeout_url`,`third_party_id`,`caller_password`,`check_sum`,
`request_url`,`result_url`,`query_trx_url`,`identifier_type`,`identifier`,`initiator_password`,`caller_type`,`short_code`,`primary_party_id`,`receiver_id`,
`access_id`,`access_identifier`,`key_owner`,`key_store`,`key_store_password`,`trust_store`,`trust_store_password`,`validation_url`,`confirmation_url`)
VALUES
(2,4,'100250',AES_ENCRYPT('!QAZ2wsx34',SHA1('@ut0b073')),'100250006','BusinessPayment','LanguageCode0','','ThirdPartyID0',caller_password = AES_ENCRYPT('caller_password', SHA1('@ut0b073')),'CheckSum0','http://196.201.214.136:8310/mminterface/request','','',11,'PoaPayInit',initiator_password = AES_ENCRYPT('init_pwd', SHA1('@ut0b073')),2,'160161',4,1,1,'Identifier3',1,'/usr/java/jdk1.7.0_45/jre/lib/security/wolverine.jks','changeit','/usr/java/jdk1.7.0_45/jre/lib/security/cacerts','changeit','http://10.66.49.201:8099/mock0','http://10.66.49.201:8099/mock1');

INSERT INTO `forge`.`mpesa_api_command`
(`mpesa_api_id`,
`command_id`,
`command_code`)
VALUES(2,
'RegisterURL',
'M14');

COMMIT;


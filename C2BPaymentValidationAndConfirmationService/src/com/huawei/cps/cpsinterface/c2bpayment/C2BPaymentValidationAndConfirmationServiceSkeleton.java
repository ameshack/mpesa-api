
/**
 * C2BPaymentValidationAndConfirmationServiceSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
package com.huawei.cps.cpsinterface.c2bpayment;

import com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentConfirmationRequest;
import com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentConfirmationResult;
import com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentValidationAndConfirmationServiceSkeletonInterface;
import com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentValidationRequest;
import com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentValidationResult;
import com.huawei.cps.cpsinterface.c2bpayment.controller.C2BPaymentValidationAndConfirmationController;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class C2BPaymentValidationAndConfirmationServiceSkeleton
implements C2BPaymentValidationAndConfirmationServiceSkeletonInterface {
    static Logger log = Logger.getLogger((String)C2BPaymentValidationAndConfirmationServiceSkeleton.class.getName());

    public C2BPaymentConfirmationResult confirmC2BPayment(C2BPaymentConfirmationRequest c2BPaymentConfirmationRequest0) {
        PropertyConfigurator.configure((String)"E:\\log4j\\mpesalog4j.properties");
        log.info((Object)"Recieved confirmC2BPayment request.....");
        C2BPaymentValidationAndConfirmationController confirmationController0 = new C2BPaymentValidationAndConfirmationController();
        C2BPaymentConfirmationResult c2BPaymentConfirmationResult0 = new C2BPaymentConfirmationResult();
        c2BPaymentConfirmationResult0 = confirmationController0.processConfirmC2BPaymentRequest(c2BPaymentConfirmationRequest0);
        return c2BPaymentConfirmationResult0;
    }

    public C2BPaymentValidationResult validateC2BPayment(C2BPaymentValidationRequest c2BPaymentValidationRequest2) {
        PropertyConfigurator.configure((String)"E:\\log4j\\mpesalog4j.properties");
        log.info((Object)"Recieved validateC2BPayment request.....");
        C2BPaymentValidationAndConfirmationController validationController0 = new C2BPaymentValidationAndConfirmationController();
        C2BPaymentValidationResult c2BPaymentValidationResult0 = new C2BPaymentValidationResult();
        c2BPaymentValidationResult0 = validationController0.processC2BPaymentValidationRequest(c2BPaymentValidationRequest2);
        return c2BPaymentValidationResult0;
    }
}
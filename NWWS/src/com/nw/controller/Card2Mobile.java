/*
    Author: Alex Mbolonzi

 */
package com.nw.controller;

//import java.util.ArrayList;
//import java.util.List;
//import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.nw.entity.Response;
import com.vodafone.mm.gen.api_v1.mminterface.client.MpesaB2cJob;

import ke.co.ars.dao.TransferB2CDAO;
import ke.co.ars.entity.MpesaRequest;
import ke.co.ars.entity.StatusCode;
import ke.co.ars.entity.TrxResponse;

//import ke.co.abc.ISOIntegrator.AbcIsoJob;

public class Card2Mobile {
    
    /* Get actual class name to be printed on */
    static Logger log = Logger.getLogger(Card2Mobile.class.getName());
    
    public Card2Mobile() {
        
      //PropertiesConfigurator is used to configure logger from properties file
        
        PropertyConfigurator.configure("/opt/log4j/B2CWebserviceLog4j.properties");
    }

    public Response card2Mobile(String MNO,String countryCode, 
            String transactionID, String sourceMobile, String destMobile,
            String amount, int sourceID, String source, String beneficiary_name) {

        log.info("card2Mobile() : " + MNO);
        
        Response response = new Response();
        
//        String responseString = null;
       
        String host = null;
        
//        StringTokenizer stringTokenizer = null;
        
        String status = null;
        
        String responseDesc = null;
        
        TrxResponse c2mTransactionResponse = null;
        
            switch (MNO) {
                    
                    case "SAFKE":

                                        
                    switch(source.toUpperCase()){
                    
                    case "POAPAYKE01":
                    	
                    	host = "SRV" + MNO + "02";
                    	
                    	MpesaRequest mpesaReq = new MpesaRequest();
                    	
                    	mpesaReq.setDestMSISDN(destMobile);
                    	mpesaReq.setSenderMSISDN(sourceMobile);
                    	mpesaReq.setAmount(amount);
                    	mpesaReq.setOrigTrxCode(transactionID);
                    	mpesaReq.setOrigID(sourceID);
                    	mpesaReq.setOrig(source);
                    	mpesaReq.setBeneficiaryName(beneficiary_name);
                    	mpesaReq.setCommandCode("M2");
                    	
                    	MpesaB2cJob mpesab2cJob = new MpesaB2cJob();
                        
                    	c2mTransactionResponse = new TrxResponse();
                    	
                    	c2mTransactionResponse = mpesab2cJob.card2Mobile(host,mpesaReq);
                    	break;
                    	
                    }
                                    
                                    status = "00";
                        break;
                        
                        default:                            
                            status = "M0007";                            
                        break;
                    }
                    
                    response.setResponseStatus(status);
                    response.setResponseDescription(c2mTransactionResponse.getStatusDescription());
                    response.setTransactionID(transactionID);
    
        return response;
    }
    
    public StatusCode getTransactionStatus(String transactionID, int sourceID) {
        
//        List<StatusCode> trxStatusList = new ArrayList<StatusCode>();
        
        StatusCode trxStatus = new StatusCode();
        
//        int waitTime = 10000;
        
//        try {
            
//            Thread.sleep(waitTime);
            
            TransferB2CDAO transferDAO = new TransferB2CDAO();
            
            trxStatus = transferDAO.findTransactionStatus(transactionID,sourceID,3);
            
//            log.info("getMpesaValidationStatus() : List size" + trxStatusList.size());
            
//            if(trxStatusList.size() > 0) {
//                
//                trxStatus = trxStatusList.get(0);
//                
//            }
            
            log.info("getMpesaValidationStatus() : " + trxStatus.getStatusCode());
            
            if(trxStatus.getStatusCode() == 100) {
                
                trxStatus.setStatusCode(0);
                trxStatus.setStatusDescription("Processing request.");
            }

//        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
//            e.printStackTrace();
//            log.error("Exception: ",e.fillInStackTrace());
            
//        }
        return trxStatus;
    }
}

-- --------------------------------------------------------------------------------
-- Routine DDL
-- Note: comments before and after the routine body will not be stored by the server
-- --------------------------------------------------------------------------------
USE `forge`;
DROP procedure IF EXISTS `get_transaction_status_by_origin_id`;


DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_transaction_status_by_origin_id`
(IN origin_trx_id varchar(45),IN origin_id INTEGER,IN function_code INTEGER,OUT status_code INTEGER,OUT status_desc varchar(45))
BEGIN

DECLARE trx_status INTEGER;
DECLARE response_status,response_desc,result_status,result_desc varchar(45);


DECLARE transactionStatusCur CURSOR FOR SELECT status,responseStatus,responseDesc,
											resultStatus,resultDesc
											FROM transfers WHERE origTrxCode = origin_trx_id 
											AND origID = origin_id LIMIT 1;

DECLARE EXIT HANDLER FOR NOT FOUND 

BEGIN 

	CALL log_forge_error (CONCAT('procedure name: get_transaction_status_by_origin_id() NO RECORD FOUND FOR ',origin_trx_id));

END;

DECLARE EXIT HANDLER FOR SQLEXCEPTION 

BEGIN 

	CALL log_forge_error (CONCAT('procedure name: get_transaction_status_by_origin_id() SQL EXCEPTION'));

END;

DECLARE CONTINUE HANDLER FOR SQLWARNING 

BEGIN 

	CALL log_forge_error (CONCAT('procedure name: get_transaction_status_by_origin_id() SQL WARNING'));

END;
	
OPEN transactionStatusCur;

FETCH transactionStatusCur INTO trx_status,response_status,response_desc,result_status,result_desc;

CASE function_code
	WHEN 2 THEN
		SET status_code = response_status;
		SET status_desc = response_desc;
	WHEN 3 THEN 
		SET status_code = result_status;
		SET status_desc = result_desc;
	ELSE
		SET status_code = trx_status;
		SET status_desc = response_desc;
END CASE;

CLOSE transactionStatusCur;

END$$

DELIMITER ;

GRANT EXECUTE ON PROCEDURE forge.get_transaction_status_by_origin_id TO 'restb2c'@'localhost' IDENTIFIED BY 'restb2c';

FLUSH PRIVILEGES;
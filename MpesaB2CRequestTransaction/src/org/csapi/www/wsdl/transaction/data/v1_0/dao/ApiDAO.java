/**
 * @author Alex Mbolonzi
 * @date 19/08/2013
 */
package org.csapi.www.wsdl.transaction.data.v1_0.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import org.csapi.www.wsdl.transaction.data.v1_0.entity.*;

public class ApiDAO {
    
    /* Get actual class name to be printed on */
    static Logger log = Logger.getLogger(ApiDAO.class.getName());

    public ApiDAO() {
      //PropertiesConfigurator is used to configure logger from properties file
        PropertyConfigurator.configure("/opt/mpesa/log4j/log4j.properties");
    }
    
    public ApiInfo getApiDetails(int nodeId, int apiCode) {

        log.info("getServerDetails()....");
        
        ApiInfo apiDetails = new ApiInfo();

        Connection conn = null;

        String sql = "SELECT a.node_id,a.sp_id,AES_DECRYPT(a.sp_password, SHA1('@ut0b073')) sp_password,a.service_id," +
        		"a.command_id,a.language_code,a.queue_timeout_url," +
        		"a.third_party_id,AES_DECRYPT(a.caller_password, SHA1('@ut0b073')) caller_password,a.check_sum,a.result_url," +
        		"a.request_url,a.query_trx_url,a.identifier,AES_DECRYPT(a.initiator_password, SHA1('@ut0b073')) initiator_password," +
        		"a.short_code,a.primary_party_id,a.receiver_id,a.access_id," +
        		"a.access_identifier,a.key_owner,a.key_store,a.key_store_password,a.caller_type," +
        		"a.trust_store,a.trust_store_password,a.identifier_type FROM mpesa_api a " +
        		"INNER JOIN api_account b ON b.idapi_account = a.idapi_account " +
        		"WHERE a.node_id = ? AND b.code = ? AND b.status = ? ORDER BY a.id ASC LIMIT 1";

        log.debug("Query : " + sql);
        
        try {

            log.info("Opening DB connection...");
            
            conn = ConnectionHelper.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1,nodeId);
            ps.setInt(2,apiCode);
            ps.setString(3,"ACTIVE");
            ResultSet rs = ps.executeQuery();
            
           
                while (rs.next()) {
                    apiDetails = processRow(rs);
                }

        } catch (SQLException e) {
            
//            e.printStackTrace();
            log.error("Exception: ",e.fillInStackTrace());
            throw new RuntimeException(e);
        } finally {
            
            log.info("Closing DB connection...");
            
            ConnectionHelper.close(conn);
        }

        return apiDetails;
    }

    
    protected ApiInfo processRow(ResultSet rs) throws SQLException {

        log.info("processRow()....");
        
        ApiInfo apiDetails = new ApiInfo();

        apiDetails.setSp_id(rs.getString("sp_id"));
        apiDetails.setSp_password(rs.getString("sp_password"));
        apiDetails.setService_id(rs.getString("service_id"));
        apiDetails.setCommand_id(rs.getString("command_id"));
        apiDetails.setLanguage_code(rs.getString("language_code"));
        apiDetails.setQueue_timeout_url(rs.getString("queue_timeout_url"));
        apiDetails.setThird_party_id(rs.getString("third_party_id"));
        apiDetails.setCaller_password(rs.getString("caller_password"));
        apiDetails.setCheck_sum(rs.getString("check_sum"));
        apiDetails.setResult_url(rs.getString("result_url"));
        apiDetails.setRequestUrl(rs.getString("request_url"));
        apiDetails.setQueryTrxUrl(rs.getString("query_trx_url"));     
        apiDetails.setIdentifier(rs.getString("identifier"));
        apiDetails.setInitiator_password(rs.getString("initiator_password"));
        apiDetails.setShort_code(rs.getString("short_code"));
        apiDetails.setPrimary_party_id(rs.getInt("primary_party_id"));
        apiDetails.setReceiver_id(rs.getInt("receiver_id"));
        apiDetails.setAccess_id(rs.getInt("access_id"));     
        apiDetails.setAccess_identifier(rs.getString("access_identifier"));
        apiDetails.setKey_owner(rs.getInt("key_owner"));
        apiDetails.setKey_store(rs.getString("key_store"));
        apiDetails.setKey_store_password(rs.getString("key_store_password"));
        apiDetails.setTrust_store(rs.getString("trust_store"));
        apiDetails.setTrust_store_password(rs.getString("trust_store_password"));
        apiDetails.setCallerType(rs.getInt("caller_type"));
        apiDetails.setIdentifierType(rs.getInt("identifier_type"));

        return apiDetails;
    }
}

package com.huawei.cps.cpsinterface.c2bpayment.controller;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import ke.co.ars.dao.MpesaC2BValidationConfirmationDAO;
import ke.co.ars.entity.MpesaC2BValidationPaymentConfirmation;
import ke.co.ars.entity.TrxResult;
import axis2.apache.org.ResultCode_type0;

import com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentConfirmationRequest;
import com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentConfirmationResult;
import com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentValidationRequest;
import com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentValidationResult;
import com.huawei.cps.cpsinterface.c2bpayment.KYCInfo_type0;
import com.huawei.cps.cpsinterface.c2bpayment.KYCInfo_type1;
import com.huawei.cps.cpsinterface.c2bpayment.KYCName_type1;
import com.huawei.cps.cpsinterface.c2bpayment.KYCValue_type1;
import com.huawei.cps.cpsinterface.c2bpayment.ResultCode_type1;
import com.huawei.cps.cpsinterface.c2bpayment.ResultDesc_type1;
import com.sendy.client.SendyJob;

public class C2BPaymentValidationAndConfirmationController {
	
	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(C2BPaymentValidationAndConfirmationController.class.getName());
	
	public C2BPaymentValidationAndConfirmationController(){
		
		// PropertiesConfigurator is used to configure logger from properties file
		PropertyConfigurator.configure("E:\\log4j\\mpesalog4j.properties");
	}

	public C2BPaymentValidationResult processC2BPaymentValidationRequest(C2BPaymentValidationRequest processValidationRequest) {
		
		log.info("processC2BPaymentValidationRequest.....");
		
		MpesaC2BValidationPaymentConfirmation mpesaC2BValidation1 = new MpesaC2BValidationPaymentConfirmation(); 
		
		//Get validation request parameters
		mpesaC2BValidation1.setTransType(processValidationRequest.getTransType());
		mpesaC2BValidation1.setTransId(processValidationRequest.getTransID());
		mpesaC2BValidation1.setTransTime(processValidationRequest.getTransTime());
		mpesaC2BValidation1.setTransAmount(Double.valueOf(processValidationRequest.getTransAmount()));
		mpesaC2BValidation1.setBusinessShortcode(processValidationRequest.getBusinessShortCode());
		mpesaC2BValidation1.setBillRefNumber(processValidationRequest.getBillRefNumber());
		mpesaC2BValidation1.setInvoiceNumber(processValidationRequest.getInvoiceNumber());
		mpesaC2BValidation1.setMsisdn(processValidationRequest.getMSISDN());
		
		String kycFirstName = null;
		String kycMiddleName = null;
		String kycLastName = null;
		
		// get customer KYC info
		KYCInfo_type0[] kycInfotype0List = processValidationRequest.getKYCInfo();
		
		for(int k=0; k<kycInfotype0List.length; k++) {
			
			KYCInfo_type0 kycInfotype0 = new KYCInfo_type0();
			
			kycInfotype0 = kycInfotype0List[k];
			
			KYCName_type1 kycNameType1 = new KYCName_type1();
			
			kycNameType1 = kycInfotype0.getKYCName();
			
			KYCValue_type1 kycValuetype1 = new KYCValue_type1();
			
			kycValuetype1 = kycInfotype0.getKYCValue();
			
			switch(kycNameType1.getKYCName_type0()) {
			
			case "[Personal Details][First Name]" :
				
					kycFirstName = kycValuetype1.getKYCValue_type0();
				
				break;
				
			case "[Personal Details][Middle Name]" :
				
					kycMiddleName = kycValuetype1.getKYCValue_type0();
				
				break;
				
			case "[Personal Details][Last Name]" : 
				
					kycLastName = kycValuetype1.getKYCValue_type0();
				
				break;
			}
		}
		
		mpesaC2BValidation1.setKycFirstName(kycFirstName);
		mpesaC2BValidation1.setKycMiddleName(kycMiddleName);
		mpesaC2BValidation1.setKycLastName(kycLastName);
	
		TrxResult processC2BValidationTrxResult = new TrxResult();
		
		MpesaC2BValidationConfirmationDAO mpesaC2BValidationDAO0 = new MpesaC2BValidationConfirmationDAO();
		
		processC2BValidationTrxResult = mpesaC2BValidationDAO0.logMpesaC2BValidationRequest(mpesaC2BValidation1);
		
		//TODO implement logging
		
		C2BPaymentValidationResult c2bPaymentValidationResult = new C2BPaymentValidationResult();
		
		//return response to SAG
		ResultCode_type1 resultCode_type1 = new ResultCode_type1();
		
		ResultDesc_type1 resultDesc_type1 = new ResultDesc_type1();
		
		resultCode_type1.setResultCode_type0("0");
		
		resultDesc_type1.setResultDesc_type0("Service processing successful");
		
		c2bPaymentValidationResult.setResultCode(resultCode_type1);
		c2bPaymentValidationResult.setResultDesc(resultDesc_type1);
		c2bPaymentValidationResult.setThirdPartyTransID(processC2BValidationTrxResult.getTransactionID());
		
		return c2bPaymentValidationResult;
	}
	
	public C2BPaymentConfirmationResult processConfirmC2BPaymentRequest(C2BPaymentConfirmationRequest processPaymentConfirmationRequest) {
		
		log.info("processConfirmC2BPaymentRequest.....");
		
		MpesaC2BValidationPaymentConfirmation mpesaC2BConfirmation1 = new MpesaC2BValidationPaymentConfirmation();
		
		//get confirm payment request details
		mpesaC2BConfirmation1.setTransType(processPaymentConfirmationRequest.getTransType());
		mpesaC2BConfirmation1.setTransId(processPaymentConfirmationRequest.getTransID());
		mpesaC2BConfirmation1.setTransTime(processPaymentConfirmationRequest.getTransTime());
		mpesaC2BConfirmation1.setTransAmount(Double.valueOf(processPaymentConfirmationRequest.getTransAmount()));
		mpesaC2BConfirmation1.setBusinessShortcode(processPaymentConfirmationRequest.getBusinessShortCode());
		mpesaC2BConfirmation1.setBillRefNumber(processPaymentConfirmationRequest.getBillRefNumber());
		mpesaC2BConfirmation1.setInvoiceNumber(processPaymentConfirmationRequest.getInvoiceNumber());
		mpesaC2BConfirmation1.setOrgAccountBalance(Double.valueOf(processPaymentConfirmationRequest.getOrgAccountBalance()));
		mpesaC2BConfirmation1.setThirdPartyTransactionId(processPaymentConfirmationRequest.getThirdPartyTransID());
		mpesaC2BConfirmation1.setMsisdn(processPaymentConfirmationRequest.getMSISDN());
		
		String kycFirstName = null;
		String kycMiddleName = null;
		String kycLastName = null;
		
		// get customer KYC info
		KYCInfo_type1[] kycInfotype1List = processPaymentConfirmationRequest.getKYCInfo();
		
		for(int k=0; k<kycInfotype1List.length; k++) {
			
			KYCInfo_type1 kycInfotype1 = new KYCInfo_type1();
			
			kycInfotype1 = kycInfotype1List[k];
			
//			KYCName_type1 kycNameType1 = new KYCName_type1();
//			
//			kycNameType1 = kycInfotype1.getKYCName();
//			
//			KYCValue_type1 kycValuetype1 = new KYCValue_type1();
//			
//			kycValuetype1 = kycInfotype1.getKYCValue();
			
			switch(kycInfotype1.getKYCName()) {
			
			case "[Personal Details][First Name]" :
				
					kycFirstName = kycInfotype1.getKYCValue();
				
				break;
				
			case "[Personal Details][Middle Name]" :
				
					kycMiddleName = kycInfotype1.getKYCValue();
				
				break;
				
			case "[Personal Details][Last Name]" : 
				
					kycLastName = kycInfotype1.getKYCValue();
				
				break;
			}
		}
		
		mpesaC2BConfirmation1.setKycFirstName(kycFirstName);
		mpesaC2BConfirmation1.setKycMiddleName(kycMiddleName);
		mpesaC2BConfirmation1.setKycLastName(kycLastName);
		
		TrxResult c2bConfirmationTrxResult = new TrxResult();
		
		MpesaC2BValidationConfirmationDAO mpesaC2BConfrimationDAO0 = new MpesaC2BValidationConfirmationDAO();
		
		c2bConfirmationTrxResult = mpesaC2BConfrimationDAO0.logMpesaC2BConfirmationRequest(mpesaC2BConfirmation1);
		
		//TODO: implement logging
		
		C2BPaymentConfirmationResult c2bPaymentConfirmationResult = new C2BPaymentConfirmationResult();
		
		
		c2bPaymentConfirmationResult.setC2BPaymentConfirmationResult("C2B Payment Transaction " +
				processPaymentConfirmationRequest.getTransID() + " result received.");
		
		//forward request to sendy
		SendyJob sendyJob1 = new SendyJob();
		
		sendyJob1.sendyC2BConfirmatiomRequest(mpesaC2BConfirmation1);
		
		return c2bPaymentConfirmationResult;
	}
}

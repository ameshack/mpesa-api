package ns.controller;

import ke.co.ars.dao.MpesaResultDAO;
import ke.co.ars.entity.MpesaC2BCheckoutResult;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.sendy.client.SendyJob;

public class PaymentNotificationController {

	/* Get actual class name to be printed on */
    static Logger log = Logger.getLogger(PaymentNotificationController.class.getName());
    
    public PaymentNotificationController() {
    	
    	//PropertiesConfigurator is used to configure logger from properties file
        
        PropertyConfigurator.configure("E:\\log4j\\mpesalog4j.properties");
    }
    
	public void processC2BPaymentNotification(ns.ResultMsg c2bPaymentNotificationResultMsg) {
		
		log.info("processC2BPaymentNotification() Merchant Trx Id : " + c2bPaymentNotificationResultMsg.getMERCHANT_TRANSACTION_ID());
		
		MpesaC2BCheckoutResult mpesaC2BCheckoutResult1 = new MpesaC2BCheckoutResult();
		
		//get result Msg parameters
		mpesaC2BCheckoutResult1.setMsisdn(c2bPaymentNotificationResultMsg.getMSISDN());
		mpesaC2BCheckoutResult1.setAmount(c2bPaymentNotificationResultMsg.getAMOUNT());
		mpesaC2BCheckoutResult1.setTrxDate(c2bPaymentNotificationResultMsg.getMPESA_TRX_DATE());
		mpesaC2BCheckoutResult1.setMpesaTrxId(c2bPaymentNotificationResultMsg.getMPESA_TRX_ID());
		mpesaC2BCheckoutResult1.setTrxStatus(c2bPaymentNotificationResultMsg.getTRX_STATUS());
		mpesaC2BCheckoutResult1.setReturnCode(c2bPaymentNotificationResultMsg.getRETURN_CODE());
		mpesaC2BCheckoutResult1.setDescription(c2bPaymentNotificationResultMsg.getDESCRIPTION());
		mpesaC2BCheckoutResult1.setMerchantTrxId(c2bPaymentNotificationResultMsg.getMERCHANT_TRANSACTION_ID());
		mpesaC2BCheckoutResult1.setEncParams(c2bPaymentNotificationResultMsg.getENC_PARAMS());
		mpesaC2BCheckoutResult1.setTrxId(c2bPaymentNotificationResultMsg.getTRX_ID());
		
		//log data into DB
		MpesaResultDAO c2bCheckoutMpesaResultDAO = new MpesaResultDAO();
		
		int sqlStatus = c2bCheckoutMpesaResultDAO.logMpesaC2BCheckoutResult(mpesaC2BCheckoutResult1);
		
		log.info("processC2BPaymentNotification() logged data to DB..");
		
		switch (sqlStatus){
		
		case 0: 
			
			SendyJob sendyJob3 = new SendyJob();
			
			sendyJob3.c2bPaymentNotification(mpesaC2BCheckoutResult1);
			
			break;
		}
		
	}
}

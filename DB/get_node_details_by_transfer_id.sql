-- --------------------------------------------------------------------------------
-- Routine DDL
-- Note: comments before and after the routine body will not be stored by the server
-- --------------------------------------------------------------------------------
USE `forge`;
DROP procedure IF EXISTS `get_node_details_by_transfer_id`;


DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_node_details_by_transfer_id`(IN transfer_id INTEGER,OUT nodeID INTEGER,OUT extAccID INTEGER, 
 OUT nodeName varchar(20),OUT hostIP varchar(15),OUT hostPort varchar(11),
 OUT timeOut INTEGER,OUT uName varchar(45),OUT pwd VARCHAR(45),
 OUT nodeURL varchar(100),OUT nodeStatus INT,OUT node_call_back_url varchar(100),
 OUT node_callback_method varchar(45),OUT orig_trx_code varchar(45))

BEGIN

DECLARE nodeStatusCur CURSOR FOR SELECT n.id,n.external_account_id,n.name,n.host_ip,n.host_port,
					n.timeout,n.username,AES_DECRYPT(n.node_passwd,SHA1('@ut0b073')) node_passwd,
					n.url,n.status,n.call_back_url,n.call_back_method,t.origTrxCode FROM node n INNER JOIN transfers t 
					ON n.id = t.destNodeID WHERE t.id = transfer_id LIMIT 1;

DECLARE EXIT HANDLER FOR NOT FOUND 

BEGIN 

	CALL log_forge_error (CONCAT('procedure name: get_node_details_by_transfer_id() NO RECORD FOUND FOR'));

END;

DECLARE EXIT HANDLER FOR SQLEXCEPTION 

BEGIN 

	CALL log_forge_error (CONCAT('procedure name: get_node_details_by_transfer_id() SQL EXCEPTION'));

END;

DECLARE CONTINUE HANDLER FOR SQLWARNING 

BEGIN 

	CALL log_forge_error (CONCAT('procedure name: get_node_details_by_transfer_id() SQL WARNING'));

END;

OPEN nodeStatusCur;

FETCH nodeStatusCur INTO nodeID,extAccID,nodeName,hostIP,hostPort,timeOut,uName,pwd,nodeURL,nodeStatus,node_call_back_url,node_callback_method,orig_trx_code;

CLOSE nodeStatusCur;

END$$

DELIMITER ;

GRANT EXECUTE ON PROCEDURE forge.get_node_details_by_transfer_id TO 'restb2c'@'localhost' IDENTIFIED BY 'restb2c';

FLUSH PRIVILEGES;

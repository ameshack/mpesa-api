USE `forge`;
DROP procedure IF EXISTS `get_register_url_details`;


DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_register_url_details`(IN server_name varchar(20),
IN origin_id INTEGER,IN mpesa_command_code varchar(5),
OUT mpesa_sp_id varchar(10),OUT mpesa_sp_password varchar(45),OUT mpesa_service_id varchar(15),
OUT mpesa_command_id varchar(64),OUT mpesa_language_code varchar(15),
OUT mpesa_third_party_id varchar(15),OUT mpesa_check_sum varchar(15),
OUT mpesa_request_url varchar(100),OUT mpesa_identifier varchar(45),
OUT mpesa_short_code varchar(10),OUT mpesa_primary_party_id INTEGER,
OUT mpesa_receiver_id INTEGER,OUT mpesa_access_id INTEGER,
OUT mpesa_access_identifier varchar(15),OUT mpesa_key_owner INTEGER,
OUT mpesa_key_store varchar(100),OUT mpesa_key_store_password varchar(45),
OUT mpesa_caller_type INTEGER,OUT mpesa_trust_store varchar(100),
OUT mpesa_trust_store_password varchar(45),OUT mpesa_identifier_type INTEGER,
OUT mpesa_sql_state INTEGER,OUT mpesa_reciever_shortcode varchar(12),
OUT mpesa_initiator_cert varchar(100),OUT mpesa_validation_url varchar(100),
OUT mpesa_confirmation_url varchar(100))

BEGIN

		DECLARE mpesa_api_cur CURSOR FOR SELECT b.sp_id,AES_DECRYPT(b.sp_password, SHA1('@ut0b073')) sp_password,
						b.service_id,d.command_id,b.language_code,b.third_party_id,
						b.check_sum,b.request_url,b.identifier,b.short_code,b.primary_party_id,b.receiver_id,b.access_id,
						b.access_identifier,b.key_owner,b.key_store,b.key_store_password,
						b.caller_type,b.trust_store,b.trust_store_password,
						b.identifier_type,b.initiator_cert,b.validation_url,b.confirmation_url FROM node a INNER JOIN mpesa_api b
						ON a.id = b.node_id INNER JOIN api_account c 
						ON c.idapi_account = b.idapi_account INNER JOIN mpesa_api_command d
						ON b.id = d.mpesa_api_id WHERE a.name = server_name 
						AND c.code = origin_id AND c.status = 'ACTIVE' AND d.command_code = mpesa_command_code 
						ORDER BY a.id ASC LIMIT 1;

		DECLARE EXIT HANDLER FOR NOT FOUND 

			BEGIN 

				CALL log_forge_error (CONCAT('procedure name: get_register_url_details() NO RECORD FOUND FOR',serverName));

			END;

		DECLARE EXIT HANDLER FOR SQLEXCEPTION 

			BEGIN 

				CALL log_forge_error (CONCAT('procedure name: get_register_url_details() SQL EXCEPTION'));

			END;

		DECLARE CONTINUE HANDLER FOR SQLWARNING

			BEGIN 

				CALL log_forge_error (CONCAT('procedure name: get_register_url_details() SQL WARNING'));

			END;

		

						OPEN mpesa_api_cur;

						FETCH mpesa_api_cur INTO mpesa_sp_id,mpesa_sp_password,mpesa_service_id,mpesa_command_id,
												mpesa_language_code,mpesa_third_party_id,mpesa_check_sum,mpesa_request_url,
												mpesa_identifier,mpesa_short_code,mpesa_primary_party_id,mpesa_receiver_id,
												mpesa_access_id,mpesa_access_identifier,mpesa_key_owner,mpesa_key_store,
												mpesa_key_store_password,mpesa_caller_type,mpesa_trust_store,
												mpesa_trust_store_password,mpesa_identifier_type,mpesa_initiator_cert,
												mpesa_validation_url,mpesa_confirmation_url;

						CLOSE mpesa_api_cur;

					
END$$
DELIMITER ;

GRANT EXECUTE ON PROCEDURE forge.get_register_url_details TO 'restb2c'@'localhost' IDENTIFIED BY 'restb2c';

FLUSH PRIVILEGES;

package com.sendy.entity;

public class SendyPaymentConfirmation {

	private String transaction_Type;
	private String transaction_Id;
	private String transaction_Time;
	private String transaction_Amount;
	private String business_Shortcode;
	private String bill_Ref_Number;
	private String invoice_Number;
	private String org_Account_Balance;
	private String third_Party_Transaction_Id;
	private String msisdn;
	private String first_Name;
	private String middle_Name;
	private String last_Name;
	
	public String getTransaction_Type() {
		return transaction_Type;
	}
	public void setTransaction_Type(String transaction_Type) {
		this.transaction_Type = transaction_Type;
	}
	public String getTransaction_Id() {
		return transaction_Id;
	}
	public void setTransaction_Id(String transaction_Id) {
		this.transaction_Id = transaction_Id;
	}
	public String getTransaction_Time() {
		return transaction_Time;
	}
	public void setTransaction_Time(String transaction_Time) {
		this.transaction_Time = transaction_Time;
	}
	public String getTransaction_Amount() {
		return transaction_Amount;
	}
	public void setTransaction_Amount(String transaction_Amount) {
		this.transaction_Amount = transaction_Amount;
	}
	public String getBusiness_Shortcode() {
		return business_Shortcode;
	}
	public void setBusiness_Shortcode(String business_Shortcode) {
		this.business_Shortcode = business_Shortcode;
	}
	public String getBill_Ref_Number() {
		return bill_Ref_Number;
	}
	public void setBill_Ref_Number(String bill_Ref_Number) {
		this.bill_Ref_Number = bill_Ref_Number;
	}
	public String getInvoice_Number() {
		return invoice_Number;
	}
	public void setInvoice_Number(String invoice_Number) {
		this.invoice_Number = invoice_Number;
	}
	public String getOrg_Account_Balance() {
		return org_Account_Balance;
	}
	public void setOrg_Account_Balance(String org_Account_Balance) {
		this.org_Account_Balance = org_Account_Balance;
	}
	public String getThird_Party_Transaction_Id() {
		return third_Party_Transaction_Id;
	}
	public void setThird_Party_Transaction_Id(String third_Party_Transaction_Id) {
		this.third_Party_Transaction_Id = third_Party_Transaction_Id;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getFirst_Name() {
		return first_Name;
	}
	public void setFirst_Name(String first_Name) {
		this.first_Name = first_Name;
	}
	public String getMiddle_Name() {
		return middle_Name;
	}
	public void setMiddle_Name(String middle_Name) {
		this.middle_Name = middle_Name;
	}
	public String getLast_Name() {
		return last_Name;
	}
	public void setLast_Name(String last_Name) {
		this.last_Name = last_Name;
	}

}

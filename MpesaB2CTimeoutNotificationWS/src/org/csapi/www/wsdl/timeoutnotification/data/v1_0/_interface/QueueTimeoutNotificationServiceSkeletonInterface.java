
/**
 * QueueTimeoutNotificationServiceSkeletonInterface.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
    package org.csapi.www.wsdl.timeoutnotification.data.v1_0._interface;
    /**
     *  QueueTimeoutNotificationServiceSkeletonInterface java skeleton interface for the axisService
     */
    public interface QueueTimeoutNotificationServiceSkeletonInterface {
     
         
        /**
         * Auto generated method signature
         * 
                                    * @param notifyQueueTimeout
         */

        
                public org.csapi.www.schema.timeoutnotification.data.v1_0.local.NotifyQueueTimeoutResponseE notifyQueueTimeout
                (
                  org.csapi.www.schema.timeoutnotification.data.v1_0.local.NotifyQueueTimeoutE notifyQueueTimeout
                 )
            ;
        
         }
    
package com.ars.c2b.register.url.client;

import java.io.StringWriter;
import java.math.BigInteger;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import ke.co.ars.entity.ApiInfo;

import org.apache.axis2.AxisFault;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.vodafone.mm.gen.api_v1.mminterface.client.SHAHashEncryptor;

import cn.com.huawei.www.schema.common.v2_1.RequestSOAPHeaderE;

import com.vodafone.mm.c2b.api_v1.mminterface.request.RequestMgrServiceStub;
import com.vodafone.mm.c2b.api_v1.mminterface.request.ObjectFactory;
import com.vodafone.mm.c2b.api_v1.mminterface.request.RequestMsg;
import com.vodafone.mm.c2b.api_v1.mminterface.request.ResponseMsg;
import com.vodafone.mm.c2b.api_v1.mminterface.request.Request;
import com.vodafone.mm.c2b.api_v1.mminterface.request.Request.Identity;
import com.vodafone.mm.c2b.api_v1.mminterface.request.Request.Identity.PrimartyParty;
import com.vodafone.mm.c2b.api_v1.mminterface.request.Request.Identity.Initiator;
import com.vodafone.mm.c2b.api_v1.mminterface.request.Request.Identity.Caller;
import com.vodafone.mm.c2b.api_v1.mminterface.request.Request.Transaction.ReferenceData;
import com.vodafone.mm.c2b.api_v1.mminterface.request.ParameterType;
import com.vodafone.mm.c2b.api_v1.mminterface.request.Request.Transaction.Parameters;
import com.vodafone.mm.c2b.api_v1.mminterface.request.Request.Transaction.Parameters.Parameter;
import com.vodafone.mm.c2b.api_v1.mminterface.request.Request.Transaction;

import cn.com.huawei.www.schema.common.v2_1.RequestSOAPHeader;

public class RegisterURLClient {
	
	/* Get actual class name to be printed on */
    static Logger log = Logger.getLogger(RegisterURLClient.class.getName());
    
    public RegisterURLClient() {
    	
    	//PropertiesConfigurator is used to configure logger from properties file
        PropertyConfigurator.configure("E:\\log4j\\mpesalog4j.properties");
    }

	public void registerC2BUrl(ApiInfo registerUrlApiIno) {
		
		log.info("Initiating registerC2BUrl Request.....");
		
		 	SimpleDateFormat timeStamp = new SimpleDateFormat("yyyyMMddHHmmss");
	        
	        Date date = new Date();
	        
	        String passwordString = registerUrlApiIno.getSp_id() + registerUrlApiIno.getSp_password() + timeStamp.format(date);
	        
	        String registerURLhashedPasswordString = "";
	        
	        //Encrypt and hash password
	        SHAHashEncryptor registerURLEncryptor = new SHAHashEncryptor();
	        
	        try {
	            registerURLhashedPasswordString = registerURLEncryptor.hashPassword(passwordString);
	        } catch (Exception e1) {
	            // TODO Auto-generated catch block
//	            e1.printStackTrace();
	            log.error("Exception: ",e1.fillInStackTrace());
	        }
		
		//Set SOAP request header
		RequestSOAPHeader registerURLRequestHeader = new RequestSOAPHeader();
		
		registerURLRequestHeader.setSpId(registerUrlApiIno.getSp_id());
		registerURLRequestHeader.setSpPassword(registerURLhashedPasswordString);
		registerURLRequestHeader.setTimeStamp(timeStamp.format(date));
		registerURLRequestHeader.setServiceId(registerUrlApiIno.getService_id());
		
		//set request Msg SOAP body parameters
		Transaction registerTransaction = new Transaction();
		
		 //set the transaction details
        registerTransaction.setCommandID(registerUrlApiIno.getCommand_id());
        registerTransaction.setOriginatorConversationID(registerUrlApiIno.getShort_code() + 
        		"_" + registerUrlApiIno.getThird_party_id() + "_" + timeStamp.format(date));
        
        Parameter registerURLTrxParam = new Parameter();
        
        Parameter registerURLtrxParam1 = new Parameter();
        
        Parameters registerURLtrxParams = new Parameters();
        
        registerURLtrxParam1.setKey("ResponseType");
        registerURLtrxParam1.setValue("Completed");
        
        registerURLtrxParams.getParameter().add(registerURLtrxParam1);
        
        registerTransaction.setParameters(registerURLtrxParams);
        
        
        ParameterType registerURLTrxParamType = new ParameterType();
        
        ParameterType registerURLTrxParamType1 = new ParameterType();
        
        ParameterType trxParamType1 = new ParameterType();
        
        ReferenceData registerURLRefData = new ReferenceData();
        
        //set reference data
        registerURLTrxParamType.setKey("ValidationURL");
        registerURLTrxParamType.setValue(registerUrlApiIno.getValidationURL());
        registerURLRefData.getReferenceItem().add(registerURLTrxParamType);
        
//        registerTransaction.setReferenceData(registerURLRefData);
        
        registerURLTrxParamType1.setKey("ConfirmationURL");
        registerURLTrxParamType1.setValue(registerUrlApiIno.getConfirmationURL());
        registerURLRefData.getReferenceItem().add(registerURLTrxParamType1);
        
        registerTransaction.setReferenceData(registerURLRefData);
        
//        BigInteger t = null;
        
        Caller caller = new Caller();
        //set caller details
        caller.setCallerType(BigInteger.valueOf(registerUrlApiIno.getCallerType()));
        caller.setThirdPartyID(null);
        caller.setPassword(null);
        caller.setCheckSum(null);
        caller.setResultURL(null);
        
        Initiator initiator = new Initiator();
        //set initiator details
        initiator.setIdentifierType(BigInteger.valueOf(registerUrlApiIno.getIdentifierType()));
        initiator.setIdentifier(null);
        initiator.setSecurityCredential(null);
        initiator.setShortCode(null);
        
        PrimartyParty primaryParty = new PrimartyParty();
        //set primary party details
        primaryParty.setIdentifierType(BigInteger.valueOf(registerUrlApiIno.getPrimary_party_id()));
        primaryParty.setIdentifier(null);
        primaryParty.setShortCode(registerUrlApiIno.getShort_code());
        
        Identity identity = new Identity();
        
        identity.setCaller(caller);
        identity.setInitiator(initiator);
        identity.setPrimartyParty(primaryParty);
//        identity.setReceiverParty(receiverParty);
//        identity.setAccessDevice(accessDevice);
        
        Request request = new Request();
        
        request.setTransaction(registerTransaction);
        
        request.setIdentity(identity);
      //set request details
        
        request.setKeyOwner(BigInteger.valueOf(registerUrlApiIno.getKey_owner()));
        
        ObjectFactory registerURLObjFactory = new ObjectFactory();
        
        JAXBElement<Request> rq = registerURLObjFactory.createRequest(request);
        
        Request registerURLFormatedRequest = rq.getValue();
        
        JAXBContext jc;
        
		try {
			
			jc = JAXBContext.newInstance("com.vodafone.mm.c2b.api_v1.mminterface.request");
			
			Marshaller marshaller = jc.createMarshaller();
	        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
//	        marshaller.marshal(formatedRequest, System.out);
	        
	        StringWriter stringWriter = new StringWriter();

	        marshaller.marshal(registerURLFormatedRequest,stringWriter);
	        
	        // Convert StringWriter to String
	        
	        String xml = stringWriter.toString();
	        
	        log.info("Request xml: " + xml);
	        
	        RequestMsg registerURLRequestMsg = new RequestMsg();
	        
	        registerURLRequestMsg.setRequestMsg(xml);
	        
	        ResponseMsg registerURLResponseMsg = new ResponseMsg();
	        
	        //set necessary keystore properties - using a p12 file
            System.setProperty("javax.net.ssl.keyStore",registerUrlApiIno.getKey_store());
            System.setProperty("javax.net.ssl.keyStorePassword",registerUrlApiIno.getKey_store_password());
//            System.setProperty("javax.net.ssl.keyStoreType", "PKCS12");       
                        
            //set necessary truststore properties - using JKS
            System.setProperty("javax.net.ssl.trustStore",registerUrlApiIno.getTrust_store());
            System.setProperty("javax.net.ssl.trustStorePassword",registerUrlApiIno.getTrust_store_password());
            
	        
	        long timeout = registerUrlApiIno.getTimeout();

            log.info("Timeout currently set to: " + timeout);    

            RequestMgrServiceStub registerURLRequestMgrServiceStub = new RequestMgrServiceStub(registerUrlApiIno.getRequestUrl());
            
            registerURLRequestMgrServiceStub._getServiceClient().getOptions().setTimeOutInMilliSeconds(timeout);
            
            RequestSOAPHeaderE registerURLRequestSOAPHeaderE = new RequestSOAPHeaderE();
            
            registerURLRequestSOAPHeaderE.setRequestSOAPHeader(registerURLRequestHeader);
            
            registerURLResponseMsg = registerURLRequestMgrServiceStub.genericAPIRequest(registerURLRequestMsg,registerURLRequestSOAPHeaderE);
	   
            registerURLResponseMsg.getResponseMsg();
            
            log.info("SAG URL Request Respone : " + registerURLResponseMsg.getResponseMsg());
            
		} catch (JAXBException | RemoteException e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("Exception: ",e.fillInStackTrace());
		} 

	}
}

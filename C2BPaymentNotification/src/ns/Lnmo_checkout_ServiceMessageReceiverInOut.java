/**
 * Lnmo_checkout_ServiceMessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
package ns;

/**
 * Lnmo_checkout_ServiceMessageReceiverInOut message receiver
 */

public class Lnmo_checkout_ServiceMessageReceiverInOut extends
		org.apache.axis2.receivers.AbstractInOutMessageReceiver {

	public void invokeBusinessLogic(
			org.apache.axis2.context.MessageContext msgContext,
			org.apache.axis2.context.MessageContext newMsgContext)
			throws org.apache.axis2.AxisFault {

		try {

			// get the implementation class for the Web Service
			Object obj = getTheImplementationObject(msgContext);

			Lnmo_checkout_ServiceSkeletonInterface skel = (Lnmo_checkout_ServiceSkeletonInterface) obj;
			// Out Envelop
			org.apache.axiom.soap.SOAPEnvelope envelope = null;
			// Find the axisOperation that has been set by the Dispatch phase.
			org.apache.axis2.description.AxisOperation op = msgContext
					.getOperationContext().getAxisOperation();
			if (op == null) {
				throw new org.apache.axis2.AxisFault(
						"Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
			}

			java.lang.String methodName;
			if ((op.getName() != null)
					&& ((methodName = org.apache.axis2.util.JavaUtils
							.xmlNameToJavaIdentifier(op.getName()
									.getLocalPart())) != null)) {

				if ("transactionStatusQuery".equals(methodName)) {

					ns.TransactionStatusResponse transactionStatusResponse37 = null;
					ns.TransactionStatusRequest wrappedParam = (ns.TransactionStatusRequest) fromOM(
							msgContext.getEnvelope().getBody()
									.getFirstElement(),
							ns.TransactionStatusRequest.class,
							getEnvelopeNamespaces(msgContext.getEnvelope()));

					transactionStatusResponse37 =

					skel.transactionStatusQuery(wrappedParam);

					envelope = toEnvelope(getSOAPFactory(msgContext),
							transactionStatusResponse37, false,
							new javax.xml.namespace.QName("tns:ns",
									"transactionStatusQuery"));
				} else

				if ("lNMOResult".equals(methodName)) {

					ns.ResponseMsg responseMsg39 = null;
					ns.ResultMsg wrappedParam = (ns.ResultMsg) fromOM(
							msgContext.getEnvelope().getBody()
									.getFirstElement(), ns.ResultMsg.class,
							getEnvelopeNamespaces(msgContext.getEnvelope()));

					responseMsg39 =

					skel.lNMOResult(wrappedParam);

					envelope = toEnvelope(getSOAPFactory(msgContext),
							responseMsg39, false,
							new javax.xml.namespace.QName("tns:ns",
									"lNMOResult"));
				} else

				if ("confirmTransaction".equals(methodName)) {

					ns.TransactionConfirmResponse transactionConfirmResponse42 = null;
					ns.TransactionConfirmRequest wrappedParam = (ns.TransactionConfirmRequest) fromOM(
							msgContext.getEnvelope().getBody()
									.getFirstElement(),
							ns.TransactionConfirmRequest.class,
							getEnvelopeNamespaces(msgContext.getEnvelope()));

					transactionConfirmResponse42 =

					skel.confirmTransaction(wrappedParam);

					envelope = toEnvelope(getSOAPFactory(msgContext),
							transactionConfirmResponse42, false,
							new javax.xml.namespace.QName("tns:ns",
									"confirmTransaction"));
				} else

				if ("processCheckOut".equals(methodName)) {

					ns.ProcessCheckOutResponse processCheckOutResponse45 = null;
					ns.ProcessCheckOutRequest wrappedParam = (ns.ProcessCheckOutRequest) fromOM(
							msgContext.getEnvelope().getBody()
									.getFirstElement(),
							ns.ProcessCheckOutRequest.class,
							getEnvelopeNamespaces(msgContext.getEnvelope()));

					processCheckOutResponse45 =

					skel.processCheckOut(wrappedParam);

					envelope = toEnvelope(getSOAPFactory(msgContext),
							processCheckOutResponse45, false,
							new javax.xml.namespace.QName("tns:ns",
									"processCheckOut"));

				} else {
					throw new java.lang.RuntimeException("method not found");
				}

				newMsgContext.setEnvelope(envelope);
			}
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	//
	private org.apache.axiom.om.OMElement toOM(
			ns.TransactionStatusRequest param, boolean optimizeContent)
			throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ns.TransactionStatusRequest.MY_QNAME,
					org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}

	}

	private org.apache.axiom.om.OMElement toOM(
			ns.TransactionStatusResponse param, boolean optimizeContent)
			throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ns.TransactionStatusResponse.MY_QNAME,
					org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}

	}

	private org.apache.axiom.om.OMElement toOM(ns.CheckOutHeader param,
			boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ns.CheckOutHeader.MY_QNAME,
					org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}

	}

	private org.apache.axiom.om.OMElement toOM(ns.ResultMsg param,
			boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ns.ResultMsg.MY_QNAME,
					org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}

	}

	private org.apache.axiom.om.OMElement toOM(ns.ResponseMsg param,
			boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ns.ResponseMsg.MY_QNAME,
					org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}

	}

	private org.apache.axiom.om.OMElement toOM(
			ns.TransactionConfirmRequest param, boolean optimizeContent)
			throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ns.TransactionConfirmRequest.MY_QNAME,
					org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}

	}

	private org.apache.axiom.om.OMElement toOM(
			ns.TransactionConfirmResponse param, boolean optimizeContent)
			throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ns.TransactionConfirmResponse.MY_QNAME,
					org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}

	}

	private org.apache.axiom.om.OMElement toOM(ns.ProcessCheckOutRequest param,
			boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ns.ProcessCheckOutRequest.MY_QNAME,
					org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}

	}

	private org.apache.axiom.om.OMElement toOM(
			ns.ProcessCheckOutResponse param, boolean optimizeContent)
			throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ns.ProcessCheckOutResponse.MY_QNAME,
					org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}

	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
			org.apache.axiom.soap.SOAPFactory factory,
			ns.TransactionStatusResponse param, boolean optimizeContent,
			javax.xml.namespace.QName methodQName)
			throws org.apache.axis2.AxisFault {
		try {
			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory
					.getDefaultEnvelope();

			emptyEnvelope.getBody().addChild(
					param.getOMElement(ns.TransactionStatusResponse.MY_QNAME,
							factory));

			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private ns.TransactionStatusResponse wraptransactionStatusQuery() {
		ns.TransactionStatusResponse wrappedElement = new ns.TransactionStatusResponse();
		return wrappedElement;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
			org.apache.axiom.soap.SOAPFactory factory, ns.ResponseMsg param,
			boolean optimizeContent, javax.xml.namespace.QName methodQName)
			throws org.apache.axis2.AxisFault {
		try {
			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory
					.getDefaultEnvelope();

			emptyEnvelope.getBody().addChild(
					param.getOMElement(ns.ResponseMsg.MY_QNAME, factory));

			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private ns.ResponseMsg wrapLNMOResult() {
		ns.ResponseMsg wrappedElement = new ns.ResponseMsg();
		return wrappedElement;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
			org.apache.axiom.soap.SOAPFactory factory,
			ns.TransactionConfirmResponse param, boolean optimizeContent,
			javax.xml.namespace.QName methodQName)
			throws org.apache.axis2.AxisFault {
		try {
			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory
					.getDefaultEnvelope();

			emptyEnvelope.getBody().addChild(
					param.getOMElement(ns.TransactionConfirmResponse.MY_QNAME,
							factory));

			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private ns.TransactionConfirmResponse wrapconfirmTransaction() {
		ns.TransactionConfirmResponse wrappedElement = new ns.TransactionConfirmResponse();
		return wrappedElement;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
			org.apache.axiom.soap.SOAPFactory factory,
			ns.ProcessCheckOutResponse param, boolean optimizeContent,
			javax.xml.namespace.QName methodQName)
			throws org.apache.axis2.AxisFault {
		try {
			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory
					.getDefaultEnvelope();

			emptyEnvelope.getBody().addChild(
					param.getOMElement(ns.ProcessCheckOutResponse.MY_QNAME,
							factory));

			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private ns.ProcessCheckOutResponse wrapprocessCheckOut() {
		ns.ProcessCheckOutResponse wrappedElement = new ns.ProcessCheckOutResponse();
		return wrappedElement;
	}

	/**
	 * get the default envelope
	 */
	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
			org.apache.axiom.soap.SOAPFactory factory) {
		return factory.getDefaultEnvelope();
	}

	private java.lang.Object fromOM(org.apache.axiom.om.OMElement param,
			java.lang.Class type, java.util.Map extraNamespaces)
			throws org.apache.axis2.AxisFault {

		try {

			if (ns.TransactionStatusRequest.class.equals(type)) {

				return ns.TransactionStatusRequest.Factory.parse(param
						.getXMLStreamReaderWithoutCaching());

			}

			if (ns.TransactionStatusResponse.class.equals(type)) {

				return ns.TransactionStatusResponse.Factory.parse(param
						.getXMLStreamReaderWithoutCaching());

			}

			if (ns.CheckOutHeader.class.equals(type)) {

				return ns.CheckOutHeader.Factory.parse(param
						.getXMLStreamReaderWithoutCaching());

			}

			if (ns.ResultMsg.class.equals(type)) {

				return ns.ResultMsg.Factory.parse(param
						.getXMLStreamReaderWithoutCaching());

			}

			if (ns.ResponseMsg.class.equals(type)) {

				return ns.ResponseMsg.Factory.parse(param
						.getXMLStreamReaderWithoutCaching());

			}

			if (ns.TransactionConfirmRequest.class.equals(type)) {

				return ns.TransactionConfirmRequest.Factory.parse(param
						.getXMLStreamReaderWithoutCaching());

			}

			if (ns.TransactionConfirmResponse.class.equals(type)) {

				return ns.TransactionConfirmResponse.Factory.parse(param
						.getXMLStreamReaderWithoutCaching());

			}

			if (ns.CheckOutHeader.class.equals(type)) {

				return ns.CheckOutHeader.Factory.parse(param
						.getXMLStreamReaderWithoutCaching());

			}

			if (ns.ProcessCheckOutRequest.class.equals(type)) {

				return ns.ProcessCheckOutRequest.Factory.parse(param
						.getXMLStreamReaderWithoutCaching());

			}

			if (ns.ProcessCheckOutResponse.class.equals(type)) {

				return ns.ProcessCheckOutResponse.Factory.parse(param
						.getXMLStreamReaderWithoutCaching());

			}

			if (ns.CheckOutHeader.class.equals(type)) {

				return ns.CheckOutHeader.Factory.parse(param
						.getXMLStreamReaderWithoutCaching());

			}

		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
		return null;
	}

	/**
	 * A utility method that copies the namepaces from the SOAPEnvelope
	 */
	private java.util.Map getEnvelopeNamespaces(
			org.apache.axiom.soap.SOAPEnvelope env) {
		java.util.Map returnMap = new java.util.HashMap();
		java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
		while (namespaceIterator.hasNext()) {
			org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator
					.next();
			returnMap.put(ns.getPrefix(), ns.getNamespaceURI());
		}
		return returnMap;
	}

	private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
		org.apache.axis2.AxisFault f;
		Throwable cause = e.getCause();
		if (cause != null) {
			f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
		} else {
			f = new org.apache.axis2.AxisFault(e.getMessage());
		}

		return f;
	}

}// end of class

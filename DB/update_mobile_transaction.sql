-- --------------------------------------------------------------------------------
-- Routine DDL
-- Note: comments before and after the routine body will not be stored by the server
-- --------------------------------------------------------------------------------
USE `forge`;
DROP procedure IF EXISTS `update_mobile_transaction`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_mobile_transaction`(IN trxID INTEGER, IN trx_status_id INTEGER, 
IN response_desc varchar(255),IN response_data varchar(255),IN mpesa_trx_number varchar(70),
IN response_status varchar(45),IN process_date varchar(45))
BEGIN

DECLARE EXIT HANDLER FOR NOT FOUND 

BEGIN 

	CALL log_forge_error (CONCAT('procedure name: update_mobile_transaction() NO RECORD FOUND FOR ',trxID));

END;

DECLARE EXIT HANDLER FOR SQLEXCEPTION 

BEGIN 

	CALL log_forge_error (CONCAT('procedure name: update_mobile_transaction() SQL EXCEPTION'));

END;

DECLARE CONTINUE HANDLER FOR SQLWARNING 

BEGIN 

	CALL log_forge_error (CONCAT('procedure name: update_mobile_transaction() SQL WARNING'));

END;

	UPDATE transfers SET responseDesc = response_desc,responseData = mpesa_trx_number,status = 0,responseStatus = response_status, dateProcessed = process_date WHERE id = trxID LIMIT 1;

	COMMIT;

END$$

DELIMITER ;

GRANT EXECUTE ON PROCEDURE forge.update_mobile_transaction TO 'restb2c'@'localhost' IDENTIFIED BY 'restb2c';

FLUSH PRIVILEGES;

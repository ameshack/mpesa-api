USE `forge`;
DROP procedure IF EXISTS `log_mpesa_api_result`;


DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `log_mpesa_api_result`(IN api_result_type INTEGER,
IN api_transfer_id INTEGER,IN api_result_code varchar(10),IN api_result_desc varchar(255),
IN api_originator_conversation_id varchar(70),IN api_conversation_id varchar(100),
IN api_transaction_id varchar(20),IN api_initiator_account_current_balance varchar(255),
IN api_debit_account_current_balance varchar(255),IN api_amount varchar(15),
IN api_debit_party_affected_account_balance varchar(255),IN api_trans_completed_time varchar(15),
IN api_debit_party_charges varchar(100),IN api_receiver_party_public_name varchar(70),
IN api_currency varchar(3),IN api_bill_reference_number varchar(45))

BEGIN

	DECLARE EXIT HANDLER FOR SQLSTATE '23000' 

	BEGIN

		CALL log_forge_error (CONCAT('procedure name: log_mpesa_api_result() Constraint Violation.'));

	END;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION 

	BEGIN 

		CALL log_forge_error (CONCAT('procedure name: log_mpesa_api_result() SQL EXCEPTION'));

	END;

	DECLARE CONTINUE HANDLER FOR SQLWARNING 

	BEGIN 

		CALL log_forge_error (CONCAT('procedure name: log_mpesa_api_result() SQL WARNING'));

	END;

		
			INSERT INTO mpesa_result (result_type,transfer_id,result_code,result_desc,
				originator_conversation_id,conversation_id,transaction_id,
				initiator_account_current_balance,debit_account_current_balance,amount,
				debit_party_affected_account_balance,trans_completed_time,debit_party_charges,
				receiver_party_public_name,currency,bill_reference_number) VALUES (api_result_type,
				api_transfer_id,api_result_code,api_result_desc,api_originator_conversation_id,
				api_conversation_id,api_transaction_id,api_initiator_account_current_balance,
				api_debit_account_current_balance,api_amount,api_debit_party_affected_account_balance,
				api_trans_completed_time,api_debit_party_charges,api_receiver_party_public_name,
				api_currency,api_bill_reference_number);

			COMMIT;

	

END$$
DELIMITER ;

GRANT EXECUTE ON PROCEDURE forge.log_mpesa_api_result TO 'restb2c'@'localhost' IDENTIFIED BY 'restb2c';

FLUSH PRIVILEGES;


package com.nw.resource;


import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.nw.controller.Card2Mobile;
import com.nw.controller.MpesaBuyGoods;
import com.nw.controller.MpesaPaybill;
import com.nw.entity.MethodCall;
import com.nw.entity.Response;


@XmlRootElement
@Path("/ke/")
public class KenyaResource {
    
    /* Get actual class name to be printed on */
    static Logger log = Logger.getLogger(KenyaResource.class.getName());
    
    public KenyaResource() {
  
        //PropertiesConfigurator is used to configure logger from properties file
    
        PropertyConfigurator.configure("/opt/log4j/B2CWebserviceLog4j.properties");
    }
    
//    TransferB2CDAO B2Cdao = new TransferB2CDAO();
   
    @POST
    @Path("mobilewallet")
    @Consumes("text/xml")
    @Produces({MediaType.TEXT_XML})
    public MethodCall postHandler(MethodCall call) {
        
        log.info("postHandler() : " + call);
        
        MethodCall.Params params = call.getParams();
        
        List<MethodCall.Params.Param> paramList = params.getParam();
        
        MethodCall.Params.Param param = paramList.get(0);
        
        MethodCall.Params.Param.Value value = param.getValue();
        
        MethodCall.Params.Param.Value.Struct struct = value.getStruct();
        
        List <MethodCall.Params.Param.Value.Struct.Member> memberList = struct.getMember();
          
        MethodCall.Params.Param param2 = paramList.get(1);
        
        MethodCall.Params.Param.Value valueTwo = param2.getValue();

        List<MethodCall.Params.Param.Value.Struct.Member.Value2.Struct2.Member2> memberList2 = null;
        
        String method = valueTwo.getString();
//        String method = call.getMethodName();
                
        String mobile = "0";
        String mno = "NONE";
        String country_code = "254";
        String transactionId = "0";
        String destMobile = "0";
        String amount = "0";
        int sourceID = 0;
        String source = "0";
        String creditBankAcc = "0";
        String bankCode = "0";
        String beneficiary_name = "0";

        
        int arraySize = memberList.size();
        
        Response response = new Response();
        
        if (arraySize > 1 ) {
            
       
        for (int i=0; i<arraySize; i++){
            
            MethodCall.Params.Param.Value.Struct.Member member = memberList.get(i);
            
            MethodCall.Params.Param.Value.Struct.Member.Value2 value2 = member.getValue();
                 
            switch (member.getName()) {
                
                case "origin_msisdn": mobile = value2.getString();
                     break;      
                case "mno": mno = value2.getString();
                     break;
                case "country_code" : country_code = value2.getString();
                     break;
                case "transaction_id" : transactionId = value2.getString();
                     break;
//                case "discount_coupon" : transactionId = value2.getString();
//                	break;
                case "dest_msisdn" : destMobile = value2.getString();
                     break;
                case "beneficiary_name" :
                	beneficiary_name = value2.getString();
                	break;
                case "kes_amount" : 
                	amount = value2.getString();
                     break;
                case "source_id" : 
                	sourceID = Integer.valueOf(value2.getString());
                     break;
                case "source" : 
                	source = value2.getString();
                     break;
                case "bank_account" : 
                	creditBankAcc = value2.getString();
                     break;
                case "bank_code" : 
                	bankCode = value2.getString();
                     break;
            }
            
         }
//        }
        
        log.info("method : " + method);
        
        switch (method) {
            
//            case "msisdnValidation": 
//                
//                ValidateMSISDN validateMSISDN = new ValidateMSISDN();
//                
//                response = validateMSISDN.validateMobileNumber(mobile,destMobile,mno,
//                        country_code,transactionId,sourceID,source);
//                
//                break;
            
            case "sendMoney":
                
                Card2Mobile b2c = new Card2Mobile();
                                
                response = b2c.card2Mobile(mno,country_code,transactionId,
                        mobile,destMobile,amount,sourceID,source,beneficiary_name);
                
                break;

        }
        
        }

        MethodCall callResponse = new MethodCall();

        callResponse = methodCallResponse(response,method);
        
         return callResponse;
    }

    @POST
    @Path("mpesapaybill")
    @Consumes("text/xml")
    @Produces({MediaType.TEXT_XML})
    public MethodCall paybillPostHandler(MethodCall call) {
        
        log.info("paybillPostHandler() : " + call);
        
        MethodCall.Params params = call.getParams();
        
        List<MethodCall.Params.Param> paramList = params.getParam();
        
        MethodCall.Params.Param param = paramList.get(0);
        
        MethodCall.Params.Param.Value value = param.getValue();
        
        MethodCall.Params.Param.Value.Struct struct = value.getStruct();
        
        List <MethodCall.Params.Param.Value.Struct.Member> memberList = struct.getMember();
          
        MethodCall.Params.Param param2 = paramList.get(1);
        
        MethodCall.Params.Param.Value valueTwo = param2.getValue();

        List<MethodCall.Params.Param.Value.Struct.Member.Value2.Struct2.Member2> memberList2 = null;
        
        String method = valueTwo.getString();
//        String method = call.getMethodName();
                
        String mobile = "0";
        String mno = "NONE";
        String country_code = "254";
        String transactionId = "0";
        String amount = "0";
        int sourceID = 0;
        String source = "0";
        String paybill = "0";
        String accountReference = "0";
        
        int arraySize = memberList.size();
        
        Response response = new Response();
        
        if (arraySize > 1 ) {
            
       
        for (int i=0; i<arraySize; i++){
            
            MethodCall.Params.Param.Value.Struct.Member member = memberList.get(i);
            
            MethodCall.Params.Param.Value.Struct.Member.Value2 value2 = member.getValue();
                 
            switch (member.getName()) {
                
                case "origin_msisdn": mobile = value2.getString();
                     break;      
                case "mno": mno = value2.getString();
                     break;
                case "country_code" : country_code = value2.getString();
                     break;
                case "transaction_id" : transactionId = value2.getString();
                     break;
                case "kes_amount" : 
                	amount = value2.getString();
                     break;
                case "source_id" : 
                	sourceID = Integer.valueOf(value2.getString());
                     break;
                case "source" : 
                	source = value2.getString();
                     break;
                case "paybill" : 
                	paybill = value2.getString();
                     break;
                case "account_reference" : 
                	accountReference = value2.getString();
                     break;
            }
            
         }
//        }
        
        log.info("method : " + method);
        
        switch (method) {
            
            case "billPayment":
                
                MpesaPaybill mpesaBillPay = new MpesaPaybill();
                                
                response = mpesaBillPay.payBill(mno,country_code,transactionId,
                        mobile,amount,sourceID,source,paybill,accountReference);
                
                break;

        }
        
        }

        MethodCall callResponse = new MethodCall();

        callResponse = methodCallResponse(response,method);
        
         return callResponse;
    }
    
    @POST
    @Path("mpesabuygoods")
    @Consumes("text/xml")
    @Produces({MediaType.TEXT_XML})
    public MethodCall buyGoodsPostHandler(MethodCall call) {
        
        log.info("buyGoodsPostHandler() : " + call);
        
        MethodCall.Params params = call.getParams();
        
        List<MethodCall.Params.Param> paramList = params.getParam();
        
        MethodCall.Params.Param param = paramList.get(0);
        
        MethodCall.Params.Param.Value value = param.getValue();
        
        MethodCall.Params.Param.Value.Struct struct = value.getStruct();
        
        List <MethodCall.Params.Param.Value.Struct.Member> memberList = struct.getMember();
          
        MethodCall.Params.Param param2 = paramList.get(1);
        
        MethodCall.Params.Param.Value valueTwo = param2.getValue();

        List<MethodCall.Params.Param.Value.Struct.Member.Value2.Struct2.Member2> memberList2 = null;
        
        String method = valueTwo.getString();
//        String method = call.getMethodName();
                
        String mobile = "0";
        String mno = "NONE";
        String country_code = "254";
        String transactionId = "0";
        String destMobile = "0";
        String amount = "0";
        int sourceID = 0;
        String source = "0";
        String paybill = "0";
        String till_number = "0";
        String accountReference = "0";
        
        int arraySize = memberList.size();
        
        Response response = new Response();
        
        if (arraySize > 1 ) {
            
       
        for (int i=0; i<arraySize; i++){
            
            MethodCall.Params.Param.Value.Struct.Member member = memberList.get(i);
            
            MethodCall.Params.Param.Value.Struct.Member.Value2 value2 = member.getValue();
                 
            switch (member.getName()) {
                
                case "origin_msisdn": mobile = value2.getString();
                     break;      
                case "mno": mno = value2.getString();
                     break;
                case "country_code" : country_code = value2.getString();
                     break;
                case "transaction_id" : transactionId = value2.getString();
                     break;
                case "kes_amount" : 
                	amount = value2.getString();
                     break;
                case "source_id" : 
                	sourceID = Integer.valueOf(value2.getString());
                     break;
                case "source" : 
                	source = value2.getString();
                     break;
                case "till_number" : 
                	till_number = value2.getString();
                     break;
                case "account_reference" : 
                	accountReference = value2.getString();
                     break;
            }
            
         }
//        }
        
        log.info("method : " + method);
        
        switch (method) {
            
            
            case "tillPayment":
                
                MpesaBuyGoods buyGoods = new MpesaBuyGoods();
                                
                response = buyGoods.tillPayment(mno,country_code,transactionId,
                        mobile,amount,sourceID,source,till_number,accountReference);
                
                break;

        }
        
        }

        MethodCall callResponse = new MethodCall();

        callResponse = methodCallResponse(response,method);
        
         return callResponse;
    }
    
    
    public MethodCall methodCallResponse(Response response, String methodName){
        
        List <MethodCall.Params.Param.Value.Struct.Member> memberList1 = new ArrayList<MethodCall.Params.Param.Value.Struct.Member>();

        MethodCall.Params.Param.Value.Struct.Member member1 = new MethodCall.Params.Param.Value.Struct.Member();
                
        MethodCall.Params.Param.Value.Struct.Member.Value2 value3 = new MethodCall.Params.Param.Value.Struct.Member.Value2();

        value3.setString(response.getTransactionID());

        member1.setName("transactionID");

        member1.setValue(value3);

        memberList1.add(member1);

        
        MethodCall.Params.Param.Value.Struct.Member member2 = new MethodCall.Params.Param.Value.Struct.Member();
        
        MethodCall.Params.Param.Value.Struct.Member.Value2 value5 = new MethodCall.Params.Param.Value.Struct.Member.Value2();

        
        value5.setString(response.getResponseStatus());

        member2.setName("response_status");

        member2.setValue(value5);

        memberList1.add(member2);
        
        
        MethodCall.Params.Param.Value.Struct.Member member3 = new MethodCall.Params.Param.Value.Struct.Member();
        
        MethodCall.Params.Param.Value.Struct.Member.Value2 value6 = new MethodCall.Params.Param.Value.Struct.Member.Value2();

        value6.setString(response.getResponseDescription());

        member3.setName("responseDescription");

        member3.setValue(value6);

        memberList1.add(member3);

        
        MethodCall.Params.Param.Value.Struct struct1 = new MethodCall.Params.Param.Value.Struct();

        struct1.getMember().addAll(memberList1);

        MethodCall.Params.Param.Value value4 = new MethodCall.Params.Param.Value();

        value4.setStruct(struct1);

        MethodCall.Params.Param param1 = new MethodCall.Params.Param();

        param1.setValue(value4);

        List<MethodCall.Params.Param> paramList1 = new ArrayList<MethodCall.Params.Param>();
        
        paramList1.add(param1);

        MethodCall.Params params1 = new MethodCall.Params(); 

        params1.getParam().addAll(paramList1);

        MethodCall methodCall1 = new MethodCall();

        methodCall1.setMethodName(methodName);
        
        methodCall1.setParams(params1);
        
        return methodCall1;
    }
}

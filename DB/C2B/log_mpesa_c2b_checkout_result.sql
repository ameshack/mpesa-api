USE `forge`;
DROP procedure IF EXISTS `log_mpesa_c2b_checkout_result`;


DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `log_mpesa_c2b_checkout_result`(IN c2b_merchant_trx_id INTEGER,
IN c2b_msisdn varchar(14), IN c2b_amount double, IN c2b_trx_date varchar(45), IN c2b_mpesa_trx_id varchar(20),
IN c2b_trx_status varchar(45), IN c2b_return_code varchar(10), IN c2b_description varchar(255),
IN c2b_enc_param varbinary(100), IN c2b_trx_id varchar(100),OUT sql_state INTEGER)

BEGIN

	DECLARE EXIT HANDLER FOR SQLSTATE '23000' 

	BEGIN

		CALL log_forge_error (CONCAT('procedure name: log_mpesa_c2b_checkout_result() Constraint Violation.'));

		SET sql_state = 23000;

	END;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION 

	BEGIN 

		CALL log_forge_error (CONCAT('procedure name: log_mpesa_c2b_checkout_result() SQL EXCEPTION'));

	END;

	DECLARE CONTINUE HANDLER FOR SQLWARNING 

	BEGIN 

		CALL log_forge_error (CONCAT('procedure name: log_mpesa_c2b_checkout_result() SQL WARNING'));

	END;

		
			INSERT INTO mpesa_c2b_checkout_result(merchant_transaction_id,msisdn,amount,
					mpesa_trx_date,mpesa_trx_id,trx_status,return_code,description,
					enc_params,trx_id,date_created,date_modified)VALUES(c2b_merchant_trx_id,
					c2b_msisdn,c2b_amount,c2b_trx_date,c2b_mpesa_trx_id,c2b_trx_status,
					c2b_return_code,c2b_description,c2b_enc_param,c2b_trx_id,NOW(),NOW());

			COMMIT;

	SET sql_state = 0;
	

END$$
DELIMITER ;

GRANT EXECUTE ON PROCEDURE forge.log_mpesa_c2b_checkout_result TO 'restb2c'@'localhost' IDENTIFIED BY 'restb2c';

FLUSH PRIVILEGES;

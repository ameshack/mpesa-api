/*
    Author: Alex Mbolonzi

 */
package org.csapi.www.wsdl.transaction.data.v1_0.entity;

public class MMMMpesaRequest {

    private String transactionID;
    
    private String msisdn;
    
    private String amount;
    
    private String transactionType;
    
    private String InstitutionCode;
    
    private String merchantCode;
    
    private String currencyCode;
    
    private String ISOServerIP;
    
    private int ISOServerPort;
    
    private int timeout;

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getInstitutionCode() {
        return InstitutionCode;
    }

    public void setInstitutionCode(String institutionCode) {
        InstitutionCode = institutionCode;
    }

    public String getMerchantCode() {
        return merchantCode;
    }

    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public String getISOServerIP() {
        return ISOServerIP;
    }

    public void setISOServerIP(String iSOServerIP) {
        ISOServerIP = iSOServerIP;
    }

    public int getISOServerPort() {
        return ISOServerPort;
    }

    public void setISOServerPort(int iSOServerPort) {
        ISOServerPort = iSOServerPort;
    }
}


/**
 * Result.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package org.csapi.www.wsdl.timeoutnotification.data.v1_0.result;
            

            /**
            *  Result bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class Result
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = Result
                Namespace URI = http://api-v1.gen.mm.vodafone.com/mminterface/result
                Namespace Prefix = ns1
                */
            

                        /**
                        * field for ResultType
                        */

                        
                                    protected java.math.BigInteger localResultType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localResultTypeTracker = false ;

                           public boolean isResultTypeSpecified(){
                               return localResultTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.math.BigInteger
                           */
                           public  java.math.BigInteger getResultType(){
                               return localResultType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ResultType
                               */
                               public void setResultType(java.math.BigInteger param){
                            localResultTypeTracker = true;
                                   
                                            this.localResultType=param;
                                    

                               }
                            

                        /**
                        * field for ResultCode
                        */

                        
                                    protected org.csapi.www.wsdl.timeoutnotification.data.v1_0.result.ResultCode_type1 localResultCode ;
                                

                           /**
                           * Auto generated getter method
                           * @return org.csapi.www.wsdl.timeoutnotification.data.v1_0.result.ResultCode_type1
                           */
                           public  org.csapi.www.wsdl.timeoutnotification.data.v1_0.result.ResultCode_type1 getResultCode(){
                               return localResultCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ResultCode
                               */
                               public void setResultCode(org.csapi.www.wsdl.timeoutnotification.data.v1_0.result.ResultCode_type1 param){
                            
                                            this.localResultCode=param;
                                    

                               }
                            

                        /**
                        * field for ResultDesc
                        */

                        
                                    protected org.csapi.www.wsdl.timeoutnotification.data.v1_0.result.ResultDesc_type1 localResultDesc ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localResultDescTracker = false ;

                           public boolean isResultDescSpecified(){
                               return localResultDescTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return org.csapi.www.wsdl.timeoutnotification.data.v1_0.result.ResultDesc_type1
                           */
                           public  org.csapi.www.wsdl.timeoutnotification.data.v1_0.result.ResultDesc_type1 getResultDesc(){
                               return localResultDesc;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ResultDesc
                               */
                               public void setResultDesc(org.csapi.www.wsdl.timeoutnotification.data.v1_0.result.ResultDesc_type1 param){
                            localResultDescTracker = param != null;
                                   
                                            this.localResultDesc=param;
                                    

                               }
                            

                        /**
                        * field for OriginatorConversationID
                        */

                        
                                    protected java.lang.String localOriginatorConversationID ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOriginatorConversationIDTracker = false ;

                           public boolean isOriginatorConversationIDSpecified(){
                               return localOriginatorConversationIDTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getOriginatorConversationID(){
                               return localOriginatorConversationID;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OriginatorConversationID
                               */
                               public void setOriginatorConversationID(java.lang.String param){
                            localOriginatorConversationIDTracker = true;
                                   
                                            this.localOriginatorConversationID=param;
                                    

                               }
                            

                        /**
                        * field for ConversationID
                        */

                        
                                    protected java.lang.String localConversationID ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localConversationIDTracker = false ;

                           public boolean isConversationIDSpecified(){
                               return localConversationIDTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getConversationID(){
                               return localConversationID;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ConversationID
                               */
                               public void setConversationID(java.lang.String param){
                            localConversationIDTracker = param != null;
                                   
                                            this.localConversationID=param;
                                    

                               }
                            

                        /**
                        * field for TransactionID
                        */

                        
                                    protected java.lang.String localTransactionID ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTransactionIDTracker = false ;

                           public boolean isTransactionIDSpecified(){
                               return localTransactionIDTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getTransactionID(){
                               return localTransactionID;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TransactionID
                               */
                               public void setTransactionID(java.lang.String param){
                            localTransactionIDTracker = param != null;
                                   
                                            this.localTransactionID=param;
                                    

                               }
                            

                        /**
                        * field for ResultParameters
                        */

                        
                                    protected org.csapi.www.wsdl.timeoutnotification.data.v1_0.result.ResultParameters_type0 localResultParameters ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localResultParametersTracker = false ;

                           public boolean isResultParametersSpecified(){
                               return localResultParametersTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return org.csapi.www.wsdl.timeoutnotification.data.v1_0.result.ResultParameters_type0
                           */
                           public  org.csapi.www.wsdl.timeoutnotification.data.v1_0.result.ResultParameters_type0 getResultParameters(){
                               return localResultParameters;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ResultParameters
                               */
                               public void setResultParameters(org.csapi.www.wsdl.timeoutnotification.data.v1_0.result.ResultParameters_type0 param){
                            localResultParametersTracker = param != null;
                                   
                                            this.localResultParameters=param;
                                    

                               }
                            

                        /**
                        * field for ReferenceData
                        */

                        
                                    protected org.csapi.www.wsdl.timeoutnotification.data.v1_0.result.ReferenceData_type0 localReferenceData ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localReferenceDataTracker = false ;

                           public boolean isReferenceDataSpecified(){
                               return localReferenceDataTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return org.csapi.www.wsdl.timeoutnotification.data.v1_0.result.ReferenceData_type0
                           */
                           public  org.csapi.www.wsdl.timeoutnotification.data.v1_0.result.ReferenceData_type0 getReferenceData(){
                               return localReferenceData;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ReferenceData
                               */
                               public void setReferenceData(org.csapi.www.wsdl.timeoutnotification.data.v1_0.result.ReferenceData_type0 param){
                            localReferenceDataTracker = param != null;
                                   
                                            this.localReferenceData=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"http://api-v1.gen.mm.vodafone.com/mminterface/result");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":Result",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "Result",
                           xmlWriter);
                   }

               
                   }
                if (localResultTypeTracker){
                                    namespace = "http://api-v1.gen.mm.vodafone.com/mminterface/result";
                                    writeStartElement(null, namespace, "ResultType", xmlWriter);
                             

                                          if (localResultType==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localResultType));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             }
                                            if (localResultCode==null){
                                                 throw new org.apache.axis2.databinding.ADBException("ResultCode cannot be null!!");
                                            }
                                           localResultCode.serialize(new javax.xml.namespace.QName("http://api-v1.gen.mm.vodafone.com/mminterface/result","ResultCode"),
                                               xmlWriter);
                                         if (localResultDescTracker){
                                            if (localResultDesc==null){
                                                 throw new org.apache.axis2.databinding.ADBException("ResultDesc cannot be null!!");
                                            }
                                           localResultDesc.serialize(new javax.xml.namespace.QName("http://api-v1.gen.mm.vodafone.com/mminterface/result","ResultDesc"),
                                               xmlWriter);
                                        } if (localOriginatorConversationIDTracker){
                                    namespace = "http://api-v1.gen.mm.vodafone.com/mminterface/result";
                                    writeStartElement(null, namespace, "OriginatorConversationID", xmlWriter);
                             

                                          if (localOriginatorConversationID==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localOriginatorConversationID);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localConversationIDTracker){
                                    namespace = "http://api-v1.gen.mm.vodafone.com/mminterface/result";
                                    writeStartElement(null, namespace, "ConversationID", xmlWriter);
                             

                                          if (localConversationID==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("ConversationID cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localConversationID);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTransactionIDTracker){
                                    namespace = "http://api-v1.gen.mm.vodafone.com/mminterface/result";
                                    writeStartElement(null, namespace, "TransactionID", xmlWriter);
                             

                                          if (localTransactionID==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("TransactionID cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localTransactionID);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localResultParametersTracker){
                                            if (localResultParameters==null){
                                                 throw new org.apache.axis2.databinding.ADBException("ResultParameters cannot be null!!");
                                            }
                                           localResultParameters.serialize(new javax.xml.namespace.QName("http://api-v1.gen.mm.vodafone.com/mminterface/result","ResultParameters"),
                                               xmlWriter);
                                        } if (localReferenceDataTracker){
                                            if (localReferenceData==null){
                                                 throw new org.apache.axis2.databinding.ADBException("ReferenceData cannot be null!!");
                                            }
                                           localReferenceData.serialize(new javax.xml.namespace.QName("http://api-v1.gen.mm.vodafone.com/mminterface/result","ReferenceData"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("http://api-v1.gen.mm.vodafone.com/mminterface/result")){
                return "res";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                 if (localResultTypeTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://api-v1.gen.mm.vodafone.com/mminterface/result",
                                                                      "ResultType"));
                                 
                                         elementList.add(localResultType==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localResultType));
                                    }
                            elementList.add(new javax.xml.namespace.QName("http://api-v1.gen.mm.vodafone.com/mminterface/result",
                                                                      "ResultCode"));
                            
                            
                                    if (localResultCode==null){
                                         throw new org.apache.axis2.databinding.ADBException("ResultCode cannot be null!!");
                                    }
                                    elementList.add(localResultCode);
                                 if (localResultDescTracker){
                            elementList.add(new javax.xml.namespace.QName("http://api-v1.gen.mm.vodafone.com/mminterface/result",
                                                                      "ResultDesc"));
                            
                            
                                    if (localResultDesc==null){
                                         throw new org.apache.axis2.databinding.ADBException("ResultDesc cannot be null!!");
                                    }
                                    elementList.add(localResultDesc);
                                } if (localOriginatorConversationIDTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://api-v1.gen.mm.vodafone.com/mminterface/result",
                                                                      "OriginatorConversationID"));
                                 
                                         elementList.add(localOriginatorConversationID==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOriginatorConversationID));
                                    } if (localConversationIDTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://api-v1.gen.mm.vodafone.com/mminterface/result",
                                                                      "ConversationID"));
                                 
                                        if (localConversationID != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localConversationID));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("ConversationID cannot be null!!");
                                        }
                                    } if (localTransactionIDTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://api-v1.gen.mm.vodafone.com/mminterface/result",
                                                                      "TransactionID"));
                                 
                                        if (localTransactionID != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTransactionID));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("TransactionID cannot be null!!");
                                        }
                                    } if (localResultParametersTracker){
                            elementList.add(new javax.xml.namespace.QName("http://api-v1.gen.mm.vodafone.com/mminterface/result",
                                                                      "ResultParameters"));
                            
                            
                                    if (localResultParameters==null){
                                         throw new org.apache.axis2.databinding.ADBException("ResultParameters cannot be null!!");
                                    }
                                    elementList.add(localResultParameters);
                                } if (localReferenceDataTracker){
                            elementList.add(new javax.xml.namespace.QName("http://api-v1.gen.mm.vodafone.com/mminterface/result",
                                                                      "ReferenceData"));
                            
                            
                                    if (localReferenceData==null){
                                         throw new org.apache.axis2.databinding.ADBException("ReferenceData cannot be null!!");
                                    }
                                    elementList.add(localReferenceData);
                                }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static Result parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            Result object =
                new Result();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"Result".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (Result)org.csapi.www.schema.timeoutnotification.data.v1_0.local.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://api-v1.gen.mm.vodafone.com/mminterface/result","ResultType").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setResultType(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInteger(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://api-v1.gen.mm.vodafone.com/mminterface/result","ResultCode").equals(reader.getName())){
                                
                                                object.setResultCode(org.csapi.www.wsdl.timeoutnotification.data.v1_0.result.ResultCode_type1.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://api-v1.gen.mm.vodafone.com/mminterface/result","ResultDesc").equals(reader.getName())){
                                
                                                object.setResultDesc(org.csapi.www.wsdl.timeoutnotification.data.v1_0.result.ResultDesc_type1.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://api-v1.gen.mm.vodafone.com/mminterface/result","OriginatorConversationID").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setOriginatorConversationID(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://api-v1.gen.mm.vodafone.com/mminterface/result","ConversationID").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"ConversationID" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setConversationID(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://api-v1.gen.mm.vodafone.com/mminterface/result","TransactionID").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"TransactionID" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTransactionID(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://api-v1.gen.mm.vodafone.com/mminterface/result","ResultParameters").equals(reader.getName())){
                                
                                                object.setResultParameters(org.csapi.www.wsdl.timeoutnotification.data.v1_0.result.ResultParameters_type0.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://api-v1.gen.mm.vodafone.com/mminterface/result","ReferenceData").equals(reader.getName())){
                                
                                                object.setReferenceData(org.csapi.www.wsdl.timeoutnotification.data.v1_0.result.ReferenceData_type0.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    
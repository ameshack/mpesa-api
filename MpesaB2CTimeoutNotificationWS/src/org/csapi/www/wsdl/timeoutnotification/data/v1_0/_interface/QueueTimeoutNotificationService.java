/**
 * QueueTimeoutNotificationService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
package org.csapi.www.wsdl.timeoutnotification.data.v1_0._interface;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.csapi.www.schema.timeoutnotification.data.v1_0.local.NotifyQueueTimeout;
import org.csapi.www.schema.timeoutnotification.data.v1_0.local.NotifyQueueTimeoutResponse;
import org.csapi.www.schema.timeoutnotification.data.v1_0.local.NotifyQueueTimeoutResponseE;
//import org.csapi.www.wsdl.timeoutnotification.data.v1_0.request.Request;
import org.csapi.www.wsdl.timeoutnotification.data.v1_0.result.Result;
import org.csapi.www.wsdl.timeoutnotification.data.v1_0.result.ResultCode_type1;
import org.csapi.www.wsdl.timeoutnotification.data.v1_0.result.ResultDesc_type1;

import com.vodafone.mm.gen.api_v1.mminterface.client.MpesaTimeoutPusher;

import ke.co.ars.dao.TransferB2CDAO;
import ke.co.ars.entity.TrxResult;


/**
 * QueueTimeoutNotificationService java skeleton for the axisService
 */
public class QueueTimeoutNotificationService implements QueueTimeoutNotificationServiceSkeletonInterface {

    /* Get actual class name to be printed on */
    static Logger log = Logger.getLogger(QueueTimeoutNotificationService.class.getName());
    /**
     * Auto generated method signature
     * 
     * @param notifyQueueTimeout0
     * @return notifyQueueTimeoutResponse1
     */

    public org.csapi.www.schema.timeoutnotification.data.v1_0.local.NotifyQueueTimeoutResponseE notifyQueueTimeout
            (
                    org.csapi.www.schema.timeoutnotification.data.v1_0.local.NotifyQueueTimeoutE notifyQueueTimeout0
            )
    {
      //PropertiesConfigurator is used to configure logger from properties file
        PropertyConfigurator.configure("/opt/log4j/mpesalog4j.properties");
        
        NotifyQueueTimeout notifyQueueTime = notifyQueueTimeout0.getNotifyQueueTimeout();
        
//        log.info("Recieved timeout notification for....." + notifyQueueTime);
        
        String originID;
        
        String originReq = null;

        originID = notifyQueueTime.getOriginatorConversationID();
        
        originReq = notifyQueueTime.getOriginReqeust();
        
        log.info("Recieved timeout notification for..... " + originID);
 
        TrxResult timeoutResult = new TrxResult();
        
        timeoutResult.setTransactionID(originID);
        timeoutResult.setStatusCode(101);
        timeoutResult.setStatusDescription("Request timeout");
        
        TransferB2CDAO amb2cdao = new TransferB2CDAO();
        
        amb2cdao.updateTransactionResult(timeoutResult);
        
        ResultCode_type1 resultCode_type1 = new ResultCode_type1();
        
        ResultDesc_type1 resultDesc_type1 = new ResultDesc_type1();
 
        String responseCode = "0";
        
        String responseDesc = "Success";
        
        resultCode_type1.setResultCode_type0(responseCode);
        
        resultDesc_type1.setResultDesc_type0(responseDesc);
        
        Result responseResult = new Result();
        
        responseResult.setResultCode(resultCode_type1);
        
        responseResult.setResultDesc(resultDesc_type1);
        
        NotifyQueueTimeoutResponse notifyQueueTimeoutResponse = new NotifyQueueTimeoutResponse();
        
        notifyQueueTimeoutResponse.setResult(responseResult);

        NotifyQueueTimeoutResponseE notifyQueueTimeoutResponseE = new NotifyQueueTimeoutResponseE();
                 
        notifyQueueTimeoutResponseE.setNotifyQueueTimeoutResponse(notifyQueueTimeoutResponse);
        
        MpesaTimeoutPusher timeoutPusher = new MpesaTimeoutPusher();
        
        timeoutPusher.timeoutRequest(originID, originReq);
        
        return notifyQueueTimeoutResponseE;
        
    }

}

package com.vodafone.mm.gen.api_v1.mminterface.resultws.controller;

import java.math.BigInteger;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import ke.co.ars.dao.MpesaResultDAO;
import ke.co.ars.entity.MpesaResult;
import ke.co.ars.entity.Transfer;

import com.vodafone.mm.gen.api_v1.mminterface.resultws.result.ParameterType;
import com.vodafone.mm.gen.api_v1.mminterface.resultws.result.Result;
import com.vodafone.mm.gen.api_v1.mminterface.resultws.result.Result.ReferenceData;
import com.vodafone.mm.gen.api_v1.mminterface.resultws.result.Result.ResultParameters;

public class MpesaResultController {

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(MpesaResultController.class.getName());

	// public MpesaResultController(){
	//
	// //PropertiesConfigurator is used to configure logger from properties file
	// PropertyConfigurator.configure("/opt/log4j/mpesalog4j.properties");
	// }

	public void processResultMsg(Result apiResult) {

		// PropertiesConfigurator is used to configure logger from properties
		// file
		PropertyConfigurator.configure("/opt/log4j/mpesalog4j.properties");

		log.info("processResultMsg.....");

		MpesaResult mpesaResultMsg = new MpesaResult();

		Transfer transferResult = new Transfer();

		JAXBElement<BigInteger> resultType = apiResult.getResultType();

		mpesaResultMsg.setResultType(resultType.getValue());

		mpesaResultMsg.setResultCode(apiResult.getResultCode());

		mpesaResultMsg.setResultDesc(apiResult.getResultDesc());

		JAXBElement<String> originatorConversationID = apiResult
				.getOriginatorConversationID();

		String origId = originatorConversationID.getValue();

		mpesaResultMsg.setOriginatorConversationID(origId);

		String transferID = null;

		// check if transaction is a retry
		boolean trxCheck = origId.contains("R");

		boolean underscoreCheck = origId.contains("_");

		if (trxCheck) {

			int rIndex = origId.indexOf("R");

			transferID = origId.substring(0, rIndex);

		} else if (underscoreCheck) {

			int lastIndex = origId.lastIndexOf("_");

			transferID = origId.substring(lastIndex + 1, origId.length());

		} else {

			transferID = origId;
		}

		transferResult.setTransferID(Integer.valueOf(transferID));

		mpesaResultMsg.setConversationID(apiResult.getConversationID());

		mpesaResultMsg.setTransactionID(apiResult.getTransactionID());

		if (apiResult.getResultCode().equals("0") || apiResult.getResultCode() == "0") {

			ResultParameters resultParameters = new ResultParameters();

			resultParameters = apiResult.getResultParameters();

			List<ParameterType> resultParameterList = null;

			resultParameterList = resultParameters.getResultParameter();

			ParameterType resultParameterType = new ParameterType();

			int resultParameterSize = resultParameterList.size();

			if (resultParameterSize > 0) {

				for (int i = 0; i < resultParameterSize; i++) {

					resultParameterType = resultParameterList.get(i);

					switch (resultParameterType.getKey().trim()) {

					case "InitiatorAccountCurrentBalance":

						mpesaResultMsg
								.setInitiatorAccountCurrentBalance(resultParameterType
										.getValue());

						break;
					case "DebitAccountCurrentBalance":

						mpesaResultMsg
								.setDebitAccountCurrentBalance(resultParameterType
										.getValue());

						break;
					case "Amount":

						mpesaResultMsg
								.setAmount(resultParameterType.getValue());

						break;
					case "DebitPartyAffectedAccountBalance":

						mpesaResultMsg
								.setDebitPartyAffectedAccountBalance(resultParameterType
										.getValue());

						break;
					case "TransCompletedTime":

						mpesaResultMsg
								.setTransCompletedTime(resultParameterType
										.getValue());

						break;
					case "DebitPartyCharges":

						mpesaResultMsg.setDebitPartyCharges(resultParameterType
								.getValue().replaceAll("amp;", ""));

						break;
					case "ReceiverPartyPublicName":

						mpesaResultMsg
								.setReceiverPartyPublicName(resultParameterType
										.getValue());

						break;
					case "Currency":

						mpesaResultMsg.setCurrency(resultParameterType
								.getValue());

						break;
					}
				}
			}

		}

		ReferenceData referenceData = new ReferenceData();

		referenceData = apiResult.getReferenceData();

		List<ParameterType> referenceParameterList = null;

		referenceParameterList = referenceData.getReferenceItem();

		int referenceDataSize = referenceParameterList.size();

		ParameterType referenceParameterType = new ParameterType();

		// String refNumber = null;

		if (referenceDataSize > 0) {

			for (int i = 0; i < referenceDataSize; i++) {

				referenceParameterType = referenceParameterList.get(i);

				switch (referenceParameterType.getKey().trim()) {

				case "BillReferenceNumber":

					mpesaResultMsg
							.setBillReferenceNumber(referenceParameterType
									.getValue());

					break;
				}
			}
		}

		MpesaResultDAO resultDAO = new MpesaResultDAO();

		log.info("calling resultDAO.updateMpesaResult()");

		resultDAO.updateMpesaResult(mpesaResultMsg, transferResult);
	}
}

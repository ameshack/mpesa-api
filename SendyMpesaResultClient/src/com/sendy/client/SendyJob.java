package com.sendy.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sendy.entity.SendyC2BCheckoutResult;
import com.sendy.entity.SendyMpesaResult;
import com.sendy.entity.SendyPaymentConfirmation;
import com.sendy.entity.SendyPaymentNotification;
import com.sendy.entity.SendyResponse;

import ke.co.ars.dao.NodeDAO;
import ke.co.ars.entity.MpesaC2BCheckoutResult;
import ke.co.ars.entity.MpesaC2BValidationPaymentConfirmation;
import ke.co.ars.entity.MpesaResult;
import ke.co.ars.entity.Node;
import ke.co.ars.entity.Transfer;


public class SendyJob {

	/* Get actual class name to be printed on */
    static Logger log = Logger.getLogger(SendyJob.class.getName());
    
	public SendyJob(){
		
		 //PropertiesConfigurator is used to configure logger from properties file
        PropertyConfigurator.configure("E:\\log4j\\mpesalog4j.properties");
		
	}
	
	public void sendyRequest(MpesaResult mpesaResult0,Transfer transfer0){
		
		log.info("Initiating Sendy request.....");
		//set sendy mpesa result
		SendyMpesaResult sendyMpesaResult0 = new SendyMpesaResult();
		
		sendyMpesaResult0.setAmount(mpesaResult0.getAmount());
		sendyMpesaResult0.setConversation_id(mpesaResult0.getConversationID());
		sendyMpesaResult0.setMpesa_transaction_id(mpesaResult0.getTransactionID());
		sendyMpesaResult0.setResult_code(mpesaResult0.getResultCode());
		sendyMpesaResult0.setResult_desc(mpesaResult0.getResultDesc());
		
		//get node details
		
		NodeDAO nodeDAO0 = new NodeDAO();
		
		Node node0 = new Node();
		
		log.info("Transfer id ....." + transfer0.getTransferID());
		
		node0 = nodeDAO0.getServerDetailsByTransferId(transfer0.getTransferID());
		
		sendyMpesaResult0.setOriginal_trx_id(node0.getNodeTransactionID());
		
		sendMpesaResultNotification(sendyMpesaResult0,node0);
	}
	
	public void sendMpesaResultNotification(SendyMpesaResult sendyMpesaResult1, Node node1){
		
		log.info("Initiating sendMpesaResultNotification request.....");
		
		
		URL sendyUrl = null;
		
		GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        log.debug("Json request..... " + gson.toJson(sendyMpesaResult1));
        
        SendyResponse sendyResponse0 = null;
        
        try {
        	
        sendyUrl = new URL(node1.getUrl());
        
        HttpURLConnection connection = (HttpURLConnection)sendyUrl.openConnection();
		connection.setDoOutput(true);
		connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setConnectTimeout(node1.getTimeout());
        connection.setReadTimeout(node1.getTimeout());
		
        OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
        out.write(gson.toJson(sendyMpesaResult1));
        out.flush();
        
        if (connection.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
				+ connection.getResponseCode());
		}
        
        BufferedReader sendyRequestBr = new BufferedReader(new InputStreamReader((connection.getInputStream())));
        
//        String output;
//      
//		System.out.println("Output from Server .... \n");
//		
//		while ((output = sendMoneyBr.readLine()) != null) {
//			System.out.println(output);
//		}
        sendyResponse0 = new SendyResponse();
        
        sendyResponse0 = gson.fromJson(sendyRequestBr, SendyResponse.class);
        
        connection.disconnect();
        
//        System.out.println(responseStatus.getStatus() + " : " + responseStatus.getMessage());
        
        log.debug("Json response..... " + sendyResponse0.getResponseCode() + " : " + sendyResponse0.getResponseDetails());
        
        
	} catch (IOException e) {
		// TODO Auto-generated catch block
//		e.printStackTrace();
		log.error("Exception: ",e.fillInStackTrace());
	}
	

	}
	
	public void sendyC2BConfirmatiomRequest(MpesaC2BValidationPaymentConfirmation mpesaC2BConfirmation2){
		
		log.info("Initiating Sendy request.....");
		//set sendy mpesa payment confirmation
		SendyPaymentConfirmation sendyPaymentConfirmation0 = new SendyPaymentConfirmation();
		
		sendyPaymentConfirmation0.setBill_Ref_Number(mpesaC2BConfirmation2.getBillRefNumber());
		sendyPaymentConfirmation0.setBusiness_Shortcode(mpesaC2BConfirmation2.getBusinessShortcode());
		sendyPaymentConfirmation0.setFirst_Name(mpesaC2BConfirmation2.getKycFirstName());
		sendyPaymentConfirmation0.setInvoice_Number(mpesaC2BConfirmation2.getInvoiceNumber());
		sendyPaymentConfirmation0.setLast_Name(mpesaC2BConfirmation2.getKycLastName());
		sendyPaymentConfirmation0.setMiddle_Name(mpesaC2BConfirmation2.getKycMiddleName());
		sendyPaymentConfirmation0.setMsisdn(mpesaC2BConfirmation2.getMsisdn());
		sendyPaymentConfirmation0.setOrg_Account_Balance(String.valueOf(mpesaC2BConfirmation2.getOrgAccountBalance()));
		sendyPaymentConfirmation0.setThird_Party_Transaction_Id(mpesaC2BConfirmation2.getThirdPartyTransactionId());
		sendyPaymentConfirmation0.setTransaction_Amount(String.valueOf(mpesaC2BConfirmation2.getTransAmount()));
		sendyPaymentConfirmation0.setTransaction_Id(mpesaC2BConfirmation2.getTransId());
		sendyPaymentConfirmation0.setTransaction_Time(mpesaC2BConfirmation2.getTransTime());
		sendyPaymentConfirmation0.setTransaction_Type(mpesaC2BConfirmation2.getTransType());
		
		
		log.info("confirmation Transaction id ....." + mpesaC2BConfirmation2.getTransId());
		
		sendMpesaC2BPaymentConfirmation(sendyPaymentConfirmation0);
	}
	
   public void sendMpesaC2BPaymentConfirmation(SendyPaymentConfirmation sendyPaymentConfirmation1){
		
		log.info("Initiating sendMpesaC2BPaymentConfirmation request.....");
		
		
		URL sendyUrl = null;
		
		GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        log.debug("Json request..... " + gson.toJson(sendyPaymentConfirmation1));
        
        SendyResponse sendyResponse0 = null;
        
        try {
        	
        sendyUrl = new URL("http://api.sendy.co.ke:8080/Sendy/mpesaconfirmhook");
        
        HttpURLConnection connection = (HttpURLConnection)sendyUrl.openConnection();
		connection.setDoOutput(true);
		connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setConnectTimeout(40000);
        connection.setReadTimeout(40000);
		
        OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
        out.write(gson.toJson(sendyPaymentConfirmation1));
        out.flush();
        
        if (connection.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
				+ connection.getResponseCode());
		}
        
        BufferedReader sendyRequestBr = new BufferedReader(new InputStreamReader((connection.getInputStream())));
        
//        String output;
//      
//		System.out.println("Output from Server .... \n");
//		
//		while ((output = sendMoneyBr.readLine()) != null) {
//			System.out.println(output);
//		}
        sendyResponse0 = new SendyResponse();
        
        sendyResponse0 = gson.fromJson(sendyRequestBr, SendyResponse.class);
        
        connection.disconnect();
        
//        System.out.println(responseStatus.getStatus() + " : " + responseStatus.getMessage());
        
        log.debug("Json response..... " + sendyResponse0.getResponseCode() + " : " + sendyResponse0.getResponseDetails());
        
        
	} catch (IOException e) {
		// TODO Auto-generated catch block
//		e.printStackTrace();
		log.error("Exception: ",e.fillInStackTrace());
	}
	

	}
   
   public void c2bPaymentNotification(MpesaC2BCheckoutResult mpesaC2BCheckoutResult0){
		
		log.info("Initiating c2bPaymentNotification request.....");
		//set sendy mpesa result
		SendyC2BCheckoutResult sendyC2BCheckoutResult0 = new SendyC2BCheckoutResult();
		
		sendyC2BCheckoutResult0.setAmount(Double.valueOf(mpesaC2BCheckoutResult0.getAmount()));
		sendyC2BCheckoutResult0.setDescription(mpesaC2BCheckoutResult0.getDescription());
		sendyC2BCheckoutResult0.setMpesa_trx_date(mpesaC2BCheckoutResult0.getTrxDate());
		sendyC2BCheckoutResult0.setMpesa_trx_id(mpesaC2BCheckoutResult0.getMpesaTrxId());
		sendyC2BCheckoutResult0.setMsisdn(mpesaC2BCheckoutResult0.getMsisdn());
		sendyC2BCheckoutResult0.setReturnCode(mpesaC2BCheckoutResult0.getReturnCode());
		sendyC2BCheckoutResult0.setTrx_id(mpesaC2BCheckoutResult0.getTrxId());
		sendyC2BCheckoutResult0.setTrx_status(mpesaC2BCheckoutResult0.getTrxStatus());
		
		//get node details
		
		NodeDAO nodeDAO1 = new NodeDAO();
		
		Node node1 = new Node();
		
		log.info("Transfer id ....." + mpesaC2BCheckoutResult0.getMerchantTrxId());
		
		node1 = nodeDAO1.getServerDetailsByTransferId(Integer.valueOf(mpesaC2BCheckoutResult0.getMerchantTrxId()));
		
		sendyC2BCheckoutResult0.setOrig_trx_id(node1.getNodeTransactionID());
		
		sendMpesaC2BOnlineCheckoutNotification(sendyC2BCheckoutResult0,node1);
	}
   
   public void sendMpesaC2BOnlineCheckoutNotification(SendyC2BCheckoutResult sendyC2BCheckoutResult1, Node node2){
		
		log.info("Initiating sendMpesaC2BOnlineCheckoutNotification request.....");
		
		
		URL sendyUrl = null;
		
		GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

       log.debug("Json request..... " + gson.toJson(sendyC2BCheckoutResult1));
       
       SendyResponse sendyResponse0 = null;
       
       try {
       	
       sendyUrl = new URL("http://api.sendy.co.ke:8080/Sendy/mpesacheckoutcallback");
       
       HttpURLConnection connection = (HttpURLConnection)sendyUrl.openConnection();
		connection.setDoOutput(true);
		connection.setRequestMethod("POST");
       connection.setRequestProperty("Content-Type", "application/json");
       connection.setConnectTimeout(node2.getTimeout());
       connection.setReadTimeout(node2.getTimeout());
		
       OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
       out.write(gson.toJson(sendyC2BCheckoutResult1));
       out.flush();
       
       if (connection.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
				+ connection.getResponseCode());
		}
       
       BufferedReader sendyRequestBr = new BufferedReader(new InputStreamReader((connection.getInputStream())));
       
//       String output;
//     
//		System.out.println("Output from Server .... \n");
//		
//		while ((output = sendMoneyBr.readLine()) != null) {
//			System.out.println(output);
//		}
       sendyResponse0 = new SendyResponse();
       
       sendyResponse0 = gson.fromJson(sendyRequestBr, SendyResponse.class);
       
       connection.disconnect();
       
//       System.out.println(responseStatus.getStatus() + " : " + responseStatus.getMessage());
       
       log.debug("Json response..... " + sendyResponse0.getResponseCode() + " : " + sendyResponse0.getResponseDetails());
       
       
	} catch (IOException e) {
		// TODO Auto-generated catch block
//		e.printStackTrace();
		log.error("Exception: ",e.fillInStackTrace());
	}
	

	}
}

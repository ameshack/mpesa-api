/*
    Author: Alex Mbolonzi

 */
package com.nw.controller;

//import java.util.ArrayList;
//import java.util.List;
//import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.nw.entity.Response;
import com.vodafone.mm.gen.api_v1.mminterface.client.MpesaB2cJob;




//import ke.co.am.ISOIntegrator.AmIsoJob;
//import ke.co.mmm.b2c.B2CResource;
import ke.co.ars.dao.TransferB2CDAO;
import ke.co.ars.entity.MpesaRequest;
import ke.co.ars.entity.StatusCode;
import ke.co.ars.entity.TrxResponse;

//import ke.co.abc.ISOIntegrator.AbcIsoJob;

public class MpesaPaybill {
    
    /* Get actual class name to be printed on */
    static Logger log = Logger.getLogger(MpesaPaybill.class.getName());
    
    public MpesaPaybill() {
        
      //PropertiesConfigurator is used to configure logger from properties file
        
        PropertyConfigurator.configure("/opt/log4j/B2CWebserviceLog4j.properties");
    }

    public Response payBill(String MNO,String countryCode, 
            String transactionID, String sourceMobile,
            String amount, int sourceID, String source,
            String paybill,String accountReference) {

        log.info("billPayment() : " + MNO);
        
        Response response = new Response();
       
        String host = null;
        
        String status = null;
        
        String responseDesc = null;
        
        TrxResponse c2mTransactionResponse = null;
        
        MpesaRequest mpesaReq = null;
        
        MpesaB2cJob mpesab2cJob = null;
                   
                    switch(source.toUpperCase()){
                    
                    case "POAPAYKE01":
                    	
                    	host = "SRV" + MNO + "03";
                    	
                    	mpesaReq = new MpesaRequest();
                    	
//                    	mpesaReq.setDestMSISDN(destMobile);
                    	mpesaReq.setSenderMSISDN(sourceMobile);
                    	mpesaReq.setAmount(amount);
                    	mpesaReq.setOrigTrxCode(transactionID);
                    	mpesaReq.setOrigID(sourceID);
                    	mpesaReq.setOrig(source);
//                    	mpesaReq.setBeneficiaryName(beneficiary_name);
                    	mpesaReq.setCommandCode("M7");
                    	mpesaReq.setPaybill(paybill);
//                    	mpesaReq.setTillNumber(tillNumber);
                    	mpesaReq.setAccountReference(accountReference);
                    	
                    	mpesab2cJob = new MpesaB2cJob();
                        
                    	c2mTransactionResponse = new TrxResponse();
                    	                 	
                    	c2mTransactionResponse = mpesab2cJob.card2Mobile(host,mpesaReq);
                    	
                    	status = "00";
                    	
                    	break;
                    	
                    case "SOLOPAYKE":
                    	
                    	host = "SRV" + MNO + "03";
                    	
                    	mpesaReq = new MpesaRequest();
                    	
//                    	mpesaReq.setDestMSISDN(destMobile);
                    	mpesaReq.setSenderMSISDN(sourceMobile);
                    	mpesaReq.setAmount(amount);
                    	mpesaReq.setOrigTrxCode(transactionID);
                    	mpesaReq.setOrigID(sourceID);
                    	mpesaReq.setOrig(source);
//                    	mpesaReq.setBeneficiaryName(beneficiary_name);
                    	mpesaReq.setCommandCode("M7");
                    	mpesaReq.setPaybill(paybill);
//                    	mpesaReq.setTillNumber(tillNumber);
                    	mpesaReq.setAccountReference(accountReference);
                    	mpesaReq.setBusinessNumber("NONE");
                    	
                    	mpesab2cJob = new MpesaB2cJob();
                        
                    	c2mTransactionResponse = new TrxResponse();
                    	                 	
                    	c2mTransactionResponse = mpesab2cJob.card2Mobile(host,mpesaReq);
                    	
                    	status = String.valueOf(c2mTransactionResponse.getStatusCode());
                    	
                    	break;
                    	 
                    }

                                    
                   

                    
                    response.setResponseStatus(status);
                    response.setResponseDescription(c2mTransactionResponse.getStatusDescription());
                    response.setTransactionID(transactionID);


        return response;
    }
    
    public Response billPayment(String MNO,String countryCode, 
            String transactionID, String sourceMobile,
            String amount, int sourceID, String source,
            String organizationCode,String accountReference) {

        log.info("billPayment() : " + MNO);
        
        Response response = new Response();
       
        String host = null;
        
        String status = null;
        
        String responseDesc = null;
        
        TrxResponse c2mTransactionResponse = null;
        
        MpesaRequest mpesaReq = null;
        
        MpesaB2cJob mpesab2cJob = null;
                   
                    switch(source.toUpperCase()){
                    	
                    case "SOLOPAYKE":
                    	
                    	host = "SRV" + MNO + "03";
                    	
                    	mpesaReq = new MpesaRequest();
                    	
//                    	mpesaReq.setDestMSISDN(destMobile);
                    	mpesaReq.setSenderMSISDN(sourceMobile);
                    	mpesaReq.setAmount(amount);
                    	mpesaReq.setOrigTrxCode(transactionID);
                    	mpesaReq.setOrigID(sourceID);
                    	mpesaReq.setOrig(source);
//                    	mpesaReq.setBeneficiaryName(beneficiary_name);
                    	mpesaReq.setCommandCode("M7");
                    	mpesaReq.setPaybill("000");
//                    	mpesaReq.setTillNumber(tillNumber);
                    	mpesaReq.setAccountReference(accountReference);
                    	mpesaReq.setBusinessNumber(organizationCode);
                    	
                    	mpesab2cJob = new MpesaB2cJob();
                        
                    	c2mTransactionResponse = new TrxResponse();
                    	                 	
                    	c2mTransactionResponse = mpesab2cJob.card2Mobile(host,mpesaReq);
                    	
                    	status = String.valueOf(c2mTransactionResponse.getStatusCode());
                    	
                    	break;
                    	 
                    }
 
                    response.setResponseStatus(status);
                    response.setResponseDescription(c2mTransactionResponse.getStatusDescription());
                    response.setTransactionID(transactionID);


        return response;
    }
    
    public StatusCode getTransactionStatus(String transactionID, int sourceID) {
        
        StatusCode trxStatus = new StatusCode();
        
            
            TransferB2CDAO transferDAO = new TransferB2CDAO();
            
            trxStatus = transferDAO.findTransactionStatus(transactionID,sourceID,3);
            
            
            log.info("getMpesaValidationStatus() : " + trxStatus.getStatusCode());
            
            if(trxStatus.getStatusCode() == 100) {
                
                trxStatus.setStatusCode(0);
                trxStatus.setStatusDescription("Processing request.");
            }

        return trxStatus;
    }
}

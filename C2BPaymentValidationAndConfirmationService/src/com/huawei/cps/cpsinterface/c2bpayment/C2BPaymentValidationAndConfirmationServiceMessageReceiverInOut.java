
/**
 * C2BPaymentValidationAndConfirmationServiceMessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
        package com.huawei.cps.cpsinterface.c2bpayment;

        /**
        *  C2BPaymentValidationAndConfirmationServiceMessageReceiverInOut message receiver
        */

        public class C2BPaymentValidationAndConfirmationServiceMessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver{


        public void invokeBusinessLogic(org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext)
        throws org.apache.axis2.AxisFault{

        try {

        // get the implementation class for the Web Service
        Object obj = getTheImplementationObject(msgContext);

        C2BPaymentValidationAndConfirmationServiceSkeletonInterface skel = (C2BPaymentValidationAndConfirmationServiceSkeletonInterface)obj;
        //Out Envelop
        org.apache.axiom.soap.SOAPEnvelope envelope = null;
        //Find the axisOperation that has been set by the Dispatch phase.
        org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext().getAxisOperation();
        if (op == null) {
        throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
        }

        java.lang.String methodName;
        if((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJavaIdentifier(op.getName().getLocalPart())) != null)){


        

            if("confirmC2BPayment".equals(methodName)){
                
                com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentConfirmationResult c2BPaymentConfirmationResult13 = null;
	                        com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentConfirmationRequest wrappedParam =
                                                             (com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentConfirmationRequest)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentConfirmationRequest.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               c2BPaymentConfirmationResult13 =
                                                   
                                                   
                                                         skel.confirmC2BPayment(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), c2BPaymentConfirmationResult13, false, new javax.xml.namespace.QName("http://cps.huawei.com/cpsinterface/c2bpayment",
                                                    "confirmC2BPayment"));
                                    } else 

            if("validateC2BPayment".equals(methodName)){
                
                com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentValidationResult c2BPaymentValidationResult15 = null;
	                        com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentValidationRequest wrappedParam =
                                                             (com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentValidationRequest)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentValidationRequest.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               c2BPaymentValidationResult15 =
                                                   
                                                   
                                                         skel.validateC2BPayment(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), c2BPaymentValidationResult15, false, new javax.xml.namespace.QName("http://cps.huawei.com/cpsinterface/c2bpayment",
                                                    "validateC2BPayment"));
                                    
            } else {
              throw new java.lang.RuntimeException("method not found");
            }
        

        newMsgContext.setEnvelope(envelope);
        }
        }
        catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
        }
        
        //
            private  org.apache.axiom.om.OMElement  toOM(com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentConfirmationRequest param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentConfirmationRequest.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentConfirmationResult param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentConfirmationResult.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentValidationRequest param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentValidationRequest.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentValidationResult param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentValidationResult.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentConfirmationResult param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentConfirmationResult.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentConfirmationResult wrapConfirmC2BPayment(){
                                com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentConfirmationResult wrappedElement = new com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentConfirmationResult();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentValidationResult param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentValidationResult.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentValidationResult wrapValidateC2BPayment(){
                                com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentValidationResult wrappedElement = new com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentValidationResult();
                                return wrappedElement;
                         }
                    


        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory){
        return factory.getDefaultEnvelope();
        }


        private  java.lang.Object fromOM(
        org.apache.axiom.om.OMElement param,
        java.lang.Class type,
        java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{

        try {
        
                if (com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentConfirmationRequest.class.equals(type)){
                
                           return com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentConfirmationRequest.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentConfirmationResult.class.equals(type)){
                
                           return com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentConfirmationResult.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentValidationRequest.class.equals(type)){
                
                           return com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentValidationRequest.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentValidationResult.class.equals(type)){
                
                           return com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentValidationResult.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
           return null;
        }



    

        /**
        *  A utility method that copies the namepaces from the SOAPEnvelope
        */
        private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
        org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
        returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
        }
        return returnMap;
        }

        private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
        org.apache.axis2.AxisFault f;
        Throwable cause = e.getCause();
        if (cause != null) {
            f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
        } else {
            f = new org.apache.axis2.AxisFault(e.getMessage());
        }

        return f;
    }

        }//end of class
    
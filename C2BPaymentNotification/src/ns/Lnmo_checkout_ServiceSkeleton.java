/**
 * Lnmo_checkout_ServiceSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
package ns;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import ns.controller.PaymentNotificationController;

/**
 * Lnmo_checkout_ServiceSkeleton java skeleton for the axisService
 */
public class Lnmo_checkout_ServiceSkeleton implements
		Lnmo_checkout_ServiceSkeletonInterface {
	
	 /* Get actual class name to be printed on */
    static Logger log = Logger.getLogger(Lnmo_checkout_ServiceSkeleton.class.getName());

    public Lnmo_checkout_ServiceSkeleton() {
    	
    	//PropertiesConfigurator is used to configure logger from properties file
        
        PropertyConfigurator.configure("/opt/log4j/B2CWebserviceLog4j.properties");
    }
    
	/**
	 * Auto generated method signature
	 * 
	 * @param transactionStatusRequest2
	 * @return transactionStatusResponse4
	 */

	public ns.TransactionStatusResponse transactionStatusQuery(
			ns.TransactionStatusRequest transactionStatusRequest2) {
		// TODO : fill this with the necessary business logic
		throw new java.lang.UnsupportedOperationException("Please implement "
				+ this.getClass().getName() + "#transactionStatusQuery");
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param resultMsg5
	 * @return responseMsg6
	 */

	public ns.ResponseMsg lNMOResult(ns.ResultMsg resultMsg5) {

		log.info("Recieved lNMOResult.....");
		
		// process resultMsg
		PaymentNotificationController paymentNotificationController = new PaymentNotificationController();

		paymentNotificationController.processC2BPaymentNotification(resultMsg5);

		ns.ResponseMsg c2bPaymentNotificationResponse = new ns.ResponseMsg();

		c2bPaymentNotificationResponse.setResponseMsg("ok");

		return c2bPaymentNotificationResponse;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param transactionConfirmRequest7
	 * @return transactionConfirmResponse9
	 */

	public ns.TransactionConfirmResponse confirmTransaction(
			ns.TransactionConfirmRequest transactionConfirmRequest7) {
		// TODO : fill this with the necessary business logic
		throw new java.lang.UnsupportedOperationException("Please implement "
				+ this.getClass().getName() + "#confirmTransaction");
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param processCheckOutRequest10
	 * @return processCheckOutResponse12
	 */

	public ns.ProcessCheckOutResponse processCheckOut(
			ns.ProcessCheckOutRequest processCheckOutRequest10) {
		// TODO : fill this with the necessary business logic
		throw new java.lang.UnsupportedOperationException("Please implement "
				+ this.getClass().getName() + "#processCheckOut");
	}

}

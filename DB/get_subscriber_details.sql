USE `forge`;
DROP procedure IF EXISTS `get_subscriber_details`;

DELIMITER $$
USE `forge`$$
CREATE PROCEDURE `get_subscriber_details` (IN msisdn varchar(20))
BEGIN

DECLARE EXIT HANDLER FOR NOT FOUND 

BEGIN 

	CALL log_forge_error (CONCAT('procedure name: get_subscriber_details() NO RECORD FOUND FOR ',msisdn));

END;

DECLARE EXIT HANDLER FOR SQLEXCEPTION 

BEGIN 

	CALL log_forge_error (CONCAT('procedure name: get_subscriber_details() SQL EXCEPTION'));

END;

DECLARE CONTINUE HANDLER FOR SQLWARNING 

BEGIN 

	CALL log_forge_error (CONCAT('procedure name: get_subscriber_details() SQL WARNING'));

END;
	
	SELECT a.mobile_number,a.subscriber_name,a.MNO,a.country_code,a.status 
	FROM mobile_money_sub a WHERE a.mobile_number LIKE msisdn ORDER BY a.mobile_number desc LIMIT 1;

END$$

DELIMITER ;

GRANT EXECUTE ON PROCEDURE forge.get_subscriber_details TO 'restb2c'@'localhost' IDENTIFIED BY 'restb2c';

FLUSH PRIVILEGES;
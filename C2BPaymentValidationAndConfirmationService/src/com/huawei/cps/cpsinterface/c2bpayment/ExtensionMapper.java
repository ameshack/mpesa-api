
/**
 * ExtensionMapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

        
            package com.huawei.cps.cpsinterface.c2bpayment;
        
            /**
            *  ExtensionMapper class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ExtensionMapper{

          public static java.lang.Object getTypeObject(java.lang.String namespaceURI,
                                                       java.lang.String typeName,
                                                       javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{

              
                  if (
                  "http://cps.huawei.com/cpsinterface/c2bpayment".equals(namespaceURI) &&
                  "ResultDesc_type1".equals(typeName)){
                   
                            return  com.huawei.cps.cpsinterface.c2bpayment.ResultDesc_type1.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://cps.huawei.com/cpsinterface/c2bpayment".equals(namespaceURI) &&
                  "KYCInfo_type1".equals(typeName)){
                   
                            return  com.huawei.cps.cpsinterface.c2bpayment.KYCInfo_type1.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://cps.huawei.com/cpsinterface/c2bpayment".equals(namespaceURI) &&
                  "KYCValue_type1".equals(typeName)){
                   
                            return  com.huawei.cps.cpsinterface.c2bpayment.KYCValue_type1.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://cps.huawei.com/cpsinterface/c2bpayment".equals(namespaceURI) &&
                  "KYCInfo_type0".equals(typeName)){
                   
                            return  com.huawei.cps.cpsinterface.c2bpayment.KYCInfo_type0.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://cps.huawei.com/cpsinterface/c2bpayment".equals(namespaceURI) &&
                  "ResultCode_type1".equals(typeName)){
                   
                            return  com.huawei.cps.cpsinterface.c2bpayment.ResultCode_type1.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://cps.huawei.com/cpsinterface/c2bpayment".equals(namespaceURI) &&
                  "KYCName_type1".equals(typeName)){
                   
                            return  com.huawei.cps.cpsinterface.c2bpayment.KYCName_type1.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://cps.huawei.com/cpsinterface/c2bpayment".equals(namespaceURI) &&
                  "ResultDesc_type1".equals(typeName)){
                   
                            return  com.huawei.cps.cpsinterface.c2bpayment.ResultDesc_type1.Factory.parse(reader);
                        

                  }

              
             throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
          }

        }
    
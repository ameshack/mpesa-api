/**
 * @author Alex Mbolonzi
 * @date 19/08/2013
 */
package org.csapi.www.wsdl.transaction.data.v1_0.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.csapi.www.wsdl.transaction.data.v1_0.entity.Hits;
import org.csapi.www.wsdl.transaction.data.v1_0.entity.Transfer;



public class AMB2CDAO {
    
    /* Get actual class name to be printed on */
    static Logger log = Logger.getLogger(AMB2CDAO.class.getName());
    
    public AMB2CDAO() {
        
      //PropertiesConfigurator is used to configure logger from properties file
        PropertyConfigurator.configure("/opt/mpesa/log4j/log4j.properties");
        
    }
    
    public Transfer findByTracetNumber (String trxID, int origID){
        
        log.info("findByTracetNumber() : " + trxID);
        
        Transfer transfer = new Transfer();
        
        Connection conn = null;
        
        String sql = "SELECT id,hits_id,destNodeID,origID,orig,businessNumber," +
        		"origTimeStamp,origTrxCode,destAccountNumber,senderMSISDN," +
        		"origTrxDate,origTrxTime,amount,senderName,currency_code," +
        		"trace_number,responseData,status,responseStatus,dateProcessed,trace_number " +
        		"FROM transfers WHERE id = ? LIMIT 1";
        
        log.debug("Query : " + sql);
        
        try {
            
            log.info("Opening DB connection...");
            
            conn = ConnectionHelper.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1,trxID);
//            ps.setInt(2,origID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                transfer = processRow(rs);
            }
        } catch (SQLException e) {
//            e.printStackTrace();
            log.error("Exception: ",e.fillInStackTrace());
            throw new RuntimeException(e);
        } finally {
            
            log.info("Closing DB connection...");
            
            ConnectionHelper.close(conn);
        }
        
        return transfer;
    }
    
    public int logHits(Hits hit) {
        
        log.info("logHits()....");
        
        Connection conn = null;
        
        PreparedStatement ps = null;
        
        int hitKey = 0;
        
        int statusId;
        
        String statusDesc = null;
        
        String sql = "INSERT INTO hits (req_timestamp,trx_type,payload) VALUES (?,?,?)";
        
        log.debug("Query : " + sql);
        
        java.sql.Timestamp date = new java.sql.Timestamp(new java.util.Date().getTime());
        
        int result = 0;
            
        try {
            
            log.info("Opening DB connection...");
            
            conn = ConnectionHelper.getConnection();
            
            ps = conn.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
            
            ps.setString(1, date.toString());
            ps.setString(2, hit.getTrxType());
            ps.setString(3, hit.getPayload());
            
            result = ps.executeUpdate();
            
            ResultSet keyset = ps.getGeneratedKeys();
            
            if ( keyset.next() ) {
                // Retrieve the auto generated key(s).
                hitKey = keyset.getInt(1);
            }
            
            if(result > 0){
                
                statusId = 0;
                statusDesc = "Accepted";
                
            }else {
             // Failed to log the request in DB.
                statusId = 99;
                statusDesc = "Could not log your request. Try again";
            }

        } catch (Exception e) {
//            e.printStackTrace();
            log.error("Exception: ",e.fillInStackTrace());
            throw new RuntimeException(e);
        } finally {
            
            log.info("Closing DB connection...");
            
            ConnectionHelper.close(conn);
        }
        
        return hitKey;
    }
    
    public void addC2BTrx(Transfer c2bTrx) {
        
        log.info("addC2BTrx()....");
        
        Connection conn = null;
        
        PreparedStatement ps = null;
        
        int statusId;
        
        String statusDesc = null;
        
        String sql = "INSERT INTO transfers (hits_id,destNodeID,origID,orig," +
        		"businessNumber,origTimeStamp,origTrxCode,destAccountNumber," +
        		"senderMSISDN,amount,currency_code,responseDesc,trace_number," +
        		"responseData,status,responseStatus,dateProcessed) " +
        		"VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        
        Transfer checkReceptNumber = new Transfer();
        
        checkReceptNumber = findByTracetNumber(c2bTrx.getTrxCode(),c2bTrx.getOrigID());
        
        java.sql.Timestamp date = new java.sql.Timestamp(new java.util.Date().getTime());
        
        int result = 0;
        
        log.info("Check number... " + checkReceptNumber.getTrxCode());
        
        if(checkReceptNumber.getTrxCode() != null){
            
            statusId = 99;
            statusDesc = "Duplicate transaction.";
            
        } else {
            
            
        try {
            
            log.info("Opening DB connection...");
            
            conn = ConnectionHelper.getConnection();
            
            log.debug("Query : " + sql);
            
            ps = conn.prepareStatement(sql);
            
            ps.setInt(1, c2bTrx.getHitsID());
            ps.setInt(2, c2bTrx.getNodeID());
            ps.setInt(3, c2bTrx.getOrigID());
            ps.setString(4, c2bTrx.getOrig());
            ps.setString(5, c2bTrx.getBusinessNumber());
            ps.setString(6, date.toString());
            ps.setString(7, c2bTrx.getTrxCode());
            ps.setString(8, c2bTrx.getDestAccount());
            ps.setString(9, c2bTrx.getSourceMSISDN());
            ps.setString(10, c2bTrx.getAmount());
            ps.setString(11, c2bTrx.getCurrencyCode());
            ps.setString(12, c2bTrx.getDescription());
            ps.setString(13, c2bTrx.getTraceNumber());
            ps.setString(14, c2bTrx.getResultData());
            ps.setInt(15, c2bTrx.getStatus());
            ps.setString(16, c2bTrx.getResultStatus());
            ps.setString(17, date.toString());
            
            result = ps.executeUpdate();
            
            if(result > 0){
                
                statusId = 0;
                statusDesc = "Accepted";
                
            }else {
             // Failed to log the request in DB.
                statusId = 99;
                statusDesc = "Could not log your request. Try again";
            }

        } catch (Exception e) {
//            e.printStackTrace();
            log.error("Exception: ",e.fillInStackTrace());
            throw new RuntimeException(e);
        } finally {
            
            log.info("Closing DB connection...");
            
            ConnectionHelper.close(conn);
        }
        }
        
    }
    
    
    protected Transfer processRow(ResultSet rs) throws SQLException {
        
        log.info("processRow()....");
        
        Transfer trxC2B = new Transfer();

        trxC2B.setTransferID(rs.getInt("id"));
        trxC2B.setHitsID(rs.getInt("hits_id"));
        trxC2B.setNodeID(rs.getInt("destNodeID"));
        trxC2B.setOrigID(rs.getInt("origID"));
        trxC2B.setOrig("orig");
        trxC2B.setBusinessNumber(rs.getString("businessNumber"));
        trxC2B.setTimestamp(rs.getString("origTimeStamp"));
        trxC2B.setTrxCode(rs.getString("origTrxCode"));
        trxC2B.setDestAccount(rs.getString("destAccountNumber"));
        trxC2B.setSourceMSISDN(rs.getString("senderMSISDN"));
        trxC2B.setAmount(rs.getString("amount"));     
        trxC2B.setCurrencyCode(rs.getString("currency_code"));  
        trxC2B.setStatus(rs.getInt("status"));
        trxC2B.setResultData(rs.getString("responseStatus"));
        trxC2B.setTraceNumber(rs.getString("trace_number"));
        
        return trxC2B;
    }
    
}

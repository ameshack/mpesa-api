/**
 * C2BPaymentValidationAndConfirmationServiceSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
package com.huawei.cps.cpsinterface.c2bpayment;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.huawei.cps.cpsinterface.c2bpayment.controller.C2BPaymentValidationAndConfirmationController;

/**
 * C2BPaymentValidationAndConfirmationServiceSkeleton java skeleton for the
 * axisService
 */
public class C2BPaymentValidationAndConfirmationServiceSkeleton implements
		C2BPaymentValidationAndConfirmationServiceSkeletonInterface {

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(C2BPaymentValidationAndConfirmationServiceSkeleton.class.getName());
	/**
	 * Auto generated method signature
	 * 
	 * @param c2BPaymentConfirmationRequest0
	 * @return c2BPaymentConfirmationResult1
	 */

	public com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentConfirmationResult confirmC2BPayment(
			com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentConfirmationRequest c2BPaymentConfirmationRequest0) {
		
		//PropertiesConfigurator is used to configure logger from properties file
        PropertyConfigurator.configure("E:\\log4j\\mpesalog4j.properties");
        
        log.info("Recieved confirmC2BPayment request.....");
        
		C2BPaymentValidationAndConfirmationController confirmationController0 = new C2BPaymentValidationAndConfirmationController();
		
		//give response back to mpesa broker
		C2BPaymentConfirmationResult c2BPaymentConfirmationResult0 = new C2BPaymentConfirmationResult();
		
		c2BPaymentConfirmationResult0 = confirmationController0.processConfirmC2BPaymentRequest(c2BPaymentConfirmationRequest0);
		
		return c2BPaymentConfirmationResult0;
		
		
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param c2BPaymentValidationRequest2
	 * @return c2BPaymentValidationResult3
	 */

	public com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentValidationResult validateC2BPayment(
			com.huawei.cps.cpsinterface.c2bpayment.C2BPaymentValidationRequest c2BPaymentValidationRequest2) {
		
		//PropertiesConfigurator is used to configure logger from properties file
        PropertyConfigurator.configure("E:\\log4j\\mpesalog4j.properties");
        
        log.info("Recieved validateC2BPayment request.....");
        
		C2BPaymentValidationAndConfirmationController validationController0 = new C2BPaymentValidationAndConfirmationController();
		
		//give response back to mpesa broker
		C2BPaymentValidationResult c2BPaymentValidationResult0 = new C2BPaymentValidationResult();
		
		c2BPaymentValidationResult0 = validationController0.processC2BPaymentValidationRequest(c2BPaymentValidationRequest2);
		
		return c2BPaymentValidationResult0;
		
	}

}

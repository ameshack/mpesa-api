USE `forge`;
DROP procedure IF EXISTS `log_mpayment_request`;


DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `log_mpayment_request`(IN serverName varchar(20),
IN reqTimestamp varchar(25),IN trxType varchar(45),IN dest_acc varchar(20),
IN amount decimal(15,2),IN origTrxID varchar(45),IN source_acc varchar(45),
IN origin_id bigint(20),IN origin_name varchar(45),IN acc_ref varchar(70),OUT trxID INTEGER,OUT inst_code varchar(45),
OUT acc_id varchar(45),OUT currency_code varchar(5),OUT time_out INTEGER,OUT host_ip varchar(15),
OUT host_port varchar(11),OUT float_account varchar(20),OUT comm_account varchar(20),OUT sql_state INTEGER,
OUT node_uname varchar(45),OUT node_passwd VARCHAR(255),OUT api_url varchar(100),
OUT callback_url varchar(100),OUT callback_method varchar(45))
BEGIN

	DECLARE hitID INT;
	DECLARE payLoad varchar(200);

	DECLARE hitCur CURSOR FOR SELECT LAST_INSERT_ID();
    DECLARE transfer_cur CURSOR FOR SELECT LAST_INSERT_ID();

	DECLARE EXIT HANDLER FOR SQLSTATE '23000' 

	BEGIN

		CALL log_forge_error (CONCAT('procedure name: log_mpayment_request() Constraint Violation.'));

		SET sql_state = 23000;
	END;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION 

	BEGIN 

		CALL log_forge_error (CONCAT('procedure name: log_mpayment_request() SQL EXCEPTION'));

	END;

	DECLARE CONTINUE HANDLER FOR SQLWARNING 

	BEGIN 

		CALL log_forge_error (CONCAT('procedure name: log_mpayment_request() SQL WARNING'));

	END;
	/*get the node details and check its status **/

	CALL get_node_details (serverName,@nodeID,@extAccID,@nodeName,@hostIP,@hostPort,@timeOut,
							@uName,@pwd,@nodeURL,@nodeStatus,@instCode,@accID,@currencyCode,
							@float_bank_acc,@comm_bank_acc,@nodeCallbackURL,@nodeCallbackMethod);

	SET inst_code = @instCode;
	SET acc_id = @accID;
	SET currency_code = @currencyCode;
	SET time_out = @timeOut;
	SET host_ip = @hostIP;
	SET host_port = @hostPort;
	SET float_account = @float_bank_acc;
	SET comm_account = @comm_bank_acc; 
	SET node_uname = @uName;
	SET node_passwd = @pwd;
	SET api_url = @nodeURL;
	SET callback_url = @nodeCallbackURL;
    SET callback_method = @nodeCallbackMethod;

	/*Check the node status if status is zero node is up, log transaction details**/
	CASE @nodeStatus

		WHEN 0 THEN

			SET payLoad = CONCAT(IFNULL(source_acc,'0'),'|',IFNULL(dest_acc,'0'),'|',IFNULL(amount,'0'),'|',IFNULL(origTrxID,'0'),'|',IFNULL(origin_id,'0'),'|',IFNULL(origin_name,'DUMMY'),'|',IFNULL(serverName,'srv'));

			INSERT INTO hits (req_timestamp,trx_type,payload) VALUES (NOW(),trxType,payLoad);
 
			COMMIT;

			OPEN hitCur;

			FETCH hitCur INTO hitID;

			CLOSE hitCur;
		
			INSERT INTO transfers (hits_id,destNodeID,origID,orig,businessNumber,
			origTimeStamp,origTrxCode,destAccountNumber,senderMSISDN,amount,
			currency_code,status,transactionDate,account_reference,description) VALUES (hitID,@nodeID,origin_id,origin_name,@accID,
			NOW(),origTrxID,dest_acc,source_acc,amount,@currencyCode,100,reqTimestamp,acc_ref,trxType);

			COMMIT;

			OPEN transfer_cur;

			FETCH transfer_cur INTO trxID;

			CLOSE transfer_cur;

			SET sql_state = 0;

		ELSE
		
			SET trxID = 0;

			SET sql_state = 95;

	END CASE;

END$$
DELIMITER ;

GRANT EXECUTE ON PROCEDURE forge.log_mpayment_request TO 'restb2c'@'localhost' IDENTIFIED BY 'restb2c';

FLUSH PRIVILEGES;

/*
    Author: Alex Mbolonzi

 */
package com.vodafone.mm.gen.api_v1.mminterface.client;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import javax.crypto.Cipher;

public class SSLKey {
    
    public static final String ALGORITHM = "RSA";

    public byte[] certKey(String initiatorPwd,String initiatorCert) throws IOException {
        
        
        String certKey = "";
        
        byte[] cipherText = null;
        
        try {
     
            FileInputStream fin = new FileInputStream(initiatorCert);
            CertificateFactory f = CertificateFactory.getInstance("X.509");
            X509Certificate certificate = (X509Certificate)f.generateCertificate(fin);
            
            fin.close();
            
            PublicKey pk = (PublicKey)certificate.getPublicKey();
            
//            certKey = pk.toString();
            
            cipherText = encrypt(initiatorPwd,pk);

     
        } catch (CertificateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
//            try {
//                if (br != null)
//                    br.close();
//    // 
//            } catch (IOException ex) {
//                ex.printStackTrace();
//            }
        }
                
      
        return cipherText;
    }
    
    public static byte[] encrypt(String text, PublicKey key) {

        byte[] cipherText = null;

        try {

            // get an RSA cipher object and print the provider

            final Cipher cipher = Cipher.getInstance(ALGORITHM);

            // encrypt the plain text using the public key

            cipher.init(Cipher.ENCRYPT_MODE, key);

            cipherText = cipher.doFinal(text.getBytes());

        } catch (Exception e) {

            e.printStackTrace();

        }

        return cipherText;

    }
    
    public static String decrypt(byte[] text, PrivateKey key) {

        byte[] dectyptedText = null;

        try {

            // get an RSA cipher object and print the provider

            final Cipher cipher = Cipher.getInstance(ALGORITHM);

 

            // decrypt the text using the private key

            cipher.init(Cipher.DECRYPT_MODE, key);

            dectyptedText = cipher.doFinal(text);

 

        } catch (Exception ex) {

            ex.printStackTrace();

        }

 

        return new String(dectyptedText);

    }
}

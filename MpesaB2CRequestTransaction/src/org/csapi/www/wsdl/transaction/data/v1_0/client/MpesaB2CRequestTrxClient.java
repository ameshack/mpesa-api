/*
    Author: Alex Mbolonzi

 */
package org.csapi.www.wsdl.transaction.data.v1_0.client;

import java.io.UnsupportedEncodingException;
import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.csapi.www.wsdl.transaction.data.v1_0._interface.QueryTransactionServiceStub;
import org.csapi.www.wsdl.transaction.data.v1_0._interface.QueryTransactionServiceStub.ExtensionInfo;
import org.csapi.www.wsdl.transaction.data.v1_0._interface.QueryTransactionServiceStub.ParameterType;
import org.csapi.www.wsdl.transaction.data.v1_0._interface.QueryTransactionServiceStub.QueueTimeOutList;
import org.csapi.www.wsdl.transaction.data.v1_0._interface.QueryTransactionServiceStub.RequestSOAPHeader;
import org.csapi.www.wsdl.transaction.data.v1_0._interface.QueryTransactionServiceStub.RequestSOAPHeaderE;
import org.csapi.www.wsdl.transaction.data.v1_0._interface.QueryTransactionServiceStub.QueryTranscation;
import org.csapi.www.wsdl.transaction.data.v1_0._interface.QueryTransactionServiceStub.QueryTranscationE;
import org.csapi.www.wsdl.transaction.data.v1_0._interface.QueryTransactionServiceStub.QueryTranscationResponseE;
import org.csapi.www.wsdl.transaction.data.v1_0._interface.QueryTransactionServiceStub.QueryTranscationResponse;
import org.csapi.www.wsdl.transaction.data.v1_0._interface.QueryTransactionServiceStub.Response;
import org.csapi.www.wsdl.transaction.data.v1_0._interface.QueryTransactionServiceStub.SubmitApiRequestList;
import org.csapi.www.wsdl.transaction.data.v1_0._interface.QueryTransactionServiceStub.SubmitApiResponseList;
import org.csapi.www.wsdl.transaction.data.v1_0._interface.QueryTransactionServiceStub.SubmitApiResultList;

import org.csapi.www.wsdl.transaction.data.v1_0.client.SHAHashEncryptor;
import org.csapi.www.wsdl.transaction.data.v1_0.entity.MMMMpesaRequest;
import org.csapi.www.wsdl.transaction.data.v1_0.entity.MMMMpesaResponse;

import org.csapi.www.wsdl.transaction.data.v1_0.entity.ApiInfo;

import org.csapi.www.wsdl.transaction.data.v1_0.entity.Node;


public class MpesaB2CRequestTrxClient {

    /* Get actual class name to be printed on */
    static Logger log = Logger.getLogger(MpesaB2CRequestTrxClient.class.getName());
    
    public MpesaB2CRequestTrxClient(){
        
//        SendRequestTransaction();
      //PropertiesConfigurator is used to configure logger from properties file
        PropertyConfigurator.configure("/opt/log4j/mpesalog4j.properties");
        
    }
    
    public MMMMpesaResponse SendRequestTransaction(ApiInfo apiInfo, MMMMpesaRequest queryRequest, Node node) {
        
        log.info("Initiating Query Transaction.....");
        
        RequestSOAPHeader requestSOAPHeader = new RequestSOAPHeader();
        
        RequestSOAPHeaderE requestSOAPHeaderE = new RequestSOAPHeaderE();
        
        QueryTranscation queryTransaction = new QueryTranscation();
        
        QueryTranscationE queryTransactionE = new QueryTranscationE();
        
        String spID = apiInfo.getSp_id();
        
        String password = apiInfo.getSp_password();
        
        String serviceID = apiInfo.getService_id();
        
        SimpleDateFormat timeSatmp = new SimpleDateFormat("yyyyMMddHHmmss");
        
        SimpleDateFormat timeSatmp2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        Date date = new Date();
        
        String passwordString = spID + password + timeSatmp.format(date);
        
        String hashedPasswordString = null;
        
        SHAHashEncryptor encryptor = new SHAHashEncryptor();
        
        try {
            hashedPasswordString = encryptor.hashPassword(passwordString);
        } catch (Exception e1) {
            // TODO Auto-generated catch block
//            e1.printStackTrace();
            log.error("Exception: ",e1.fillInStackTrace());
        }
        //set SOAP Header details
        requestSOAPHeader.setSpId(spID);
        requestSOAPHeader.setSpPassword(hashedPasswordString);
        requestSOAPHeader.setServiceId(serviceID);
        requestSOAPHeader.setTimeStamp(timeSatmp.format(date));
        
        //set requestSOAPHeaderE details
        requestSOAPHeaderE.setRequestSOAPHeader(requestSOAPHeader);
        
//        int stan = (int)Math.floor( Math.random() * 999998 + 1 );
//        NumberFormat formatter = new DecimalFormat("000000");

        String originatorConversationID = queryRequest.getTransactionID();
        
      //set query transaction details
        queryTransaction.setOriginatorConversationID(originatorConversationID);
        
        ParameterType paramType = new ParameterType();
        
        paramType.setKey("aa");
        
        paramType.setValue("11");
        
        ExtensionInfo extInfo = new ExtensionInfo();
        
        extInfo.addItem(paramType);
        
        queryTransaction.setExtensionInfo(extInfo);
        //set query transactionE details
        queryTransactionE.setQueryTranscation(queryTransaction);
        
        MMMMpesaResponse responseMsg = new MMMMpesaResponse();
        
        try {
           
          //set necessary keystore properties - using a p12 file
            System.setProperty("javax.net.ssl.keyStore",apiInfo.getKey_store());
            System.setProperty("javax.net.ssl.keyStorePassword",apiInfo.getKey_store_password());
//            System.setProperty("javax.net.ssl.keyStoreType", "PKCS12");       
                        
            //set necessary truststore properties - using JKS
            System.setProperty("javax.net.ssl.trustStore",apiInfo.getTrust_store());
            System.setProperty("javax.net.ssl.trustStorePassword",apiInfo.getTrust_store_password());
            
            long timeout = node.getTimeout();

            log.info("Timeout currently set to: " + timeout);
            
           QueryTransactionServiceStub queryTransactionServiceStub = new QueryTransactionServiceStub("https://196.201.214.136:18423/queryTransactionService/services/transaction");
    
           queryTransactionServiceStub._getServiceClient().getOptions().setTimeOutInMilliSeconds(timeout);
           
           QueryTranscationResponseE queryTransactionResponseE = new QueryTranscationResponseE();
            
           queryTransactionResponseE = queryTransactionServiceStub.queryTransaction(queryTransactionE,requestSOAPHeaderE);
        
           QueryTranscationResponse queryTransactionResponse = new QueryTranscationResponse();
           
           queryTransactionResponse = queryTransactionResponseE.getQueryTranscationResponse();
           
           String responseCode = null;
           
           String responseDesc = null;
           
           String conversationID = null;
           //get Transaction response details
           
           Response response = new Response();
           
           response = queryTransactionResponse.getResult();
           
           responseCode = response.getResponseCode();
           
           responseDesc = response.getResponseDesc();
            
           log.info("Response: " + " Response Code: " + responseCode + " Response Desc: " + responseDesc);
           
           log.info("Check for API request list : " + queryTransactionResponse.isSubmitApiRequestListSpecified());
           
           if(queryTransactionResponse.isSubmitApiRequestListSpecified()) {
                   
                   SubmitApiRequestList submitApiRequestList = new SubmitApiRequestList();
               
                   submitApiRequestList = queryTransactionResponse.getSubmitApiRequestList();
                   
                   log.info("Check for API request : " + submitApiRequestList.isSubmitApiRequestSpecified());
                   
                   if(submitApiRequestList.isSubmitApiRequestSpecified()) {
                   
                       OMElement submitApiRequestDocument = null;
                       
                       submitApiRequestDocument = submitApiRequestList.getSubmitApiRequest();

                       byte[] decoded = Base64.decodeBase64(StringUtils.getBytesUtf8(submitApiRequestDocument.getText()));
                       
                       String originReq = null;
                       
                       try {
                           originReq = new String(decoded, "UTF-8");
                       } catch (UnsupportedEncodingException e) {
                           // TODO Auto-generated catch block
                           e.printStackTrace();
                       }
                           log.info("Api request : " + originReq);

                   }
                   
           }
           
           log.info("Check for API response list : " + queryTransactionResponse.isSubmitApiResponseListSpecified());
           
           if(queryTransactionResponse.isSubmitApiResponseListSpecified()) {
                              
               SubmitApiResponseList submitApiResponseList = new SubmitApiResponseList();
               
               submitApiResponseList = queryTransactionResponse.getSubmitApiResponseList();
               
               log.info("Check for API response : " + submitApiResponseList.isSubmitApiResponseSpecified());
               
               if(submitApiResponseList.isSubmitApiResponseSpecified()) {
                   
                   OMElement submitApiResponseDocument = null;
                   
                   submitApiResponseDocument = submitApiResponseList.getSubmitApiResponse();

                   byte[] decoded = Base64.decodeBase64(StringUtils.getBytesUtf8(submitApiResponseDocument.getText()));
                   
                   String originRes = null;
                   
                   try {
                       originRes = new String(decoded, "UTF-8");
                   } catch (UnsupportedEncodingException e) {
                       // TODO Auto-generated catch block
                       e.printStackTrace();
                   }

                   log.info("Api response : " + originRes);
               }
           }
           
           log.info("Check for API result list : " + queryTransactionResponse.isSubmitApiResultListSpecified());
           
           if(queryTransactionResponse.isSubmitApiResultListSpecified()) {
               
               SubmitApiResultList submitApiResultList = new SubmitApiResultList();
               
               submitApiResultList = queryTransactionResponse.getSubmitApiResultList();

               log.info("Check for API result : " + submitApiResultList.isSubmitApiResultSpecified());
               
               if(submitApiResultList.isSubmitApiResultSpecified()) {

                       OMElement submitApiResultDocument = null;
                       
                       submitApiResultDocument = submitApiResultList.getSubmitApiResult();

                       byte[] decoded = Base64.decodeBase64(StringUtils.getBytesUtf8(submitApiResultDocument.getText()));
                       
                       String originResult = null;
                       
                       try {
                           originResult = new String(decoded, "UTF-8");
                       } catch (UnsupportedEncodingException e) {
                           // TODO Auto-generated catch block
                           e.printStackTrace();
                       }

                       log.info("Api result : " + originResult);
               }
               
           }
           
           log.info("Check for API queuetimeout list : " + queryTransactionResponse.isQueueTimeOutListSpecified());
           if(queryTransactionResponse.isQueueTimeOutListSpecified()) {
               
               QueueTimeOutList queueTimeoutList = new QueueTimeOutList();
               
               queueTimeoutList = queryTransactionResponse.getQueueTimeOutList();
               
               log.info("Check for API queuetimeout : " + queueTimeoutList.isQueueTimeOutSpecified());
           }
           
           log.info("Check for API extensionInfo list : " + queryTransactionResponse.isExtensionInfoSpecified());
           if(queryTransactionResponse.isExtensionInfoSpecified()) {
               
               ExtensionInfo extensionInfo = new ExtensionInfo();
               
               extensionInfo = queryTransactionResponse.getExtensionInfo();
           }
           
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
//            e.printStackTrace();
            log.error("Exception: ",e.fillInStackTrace());
        }
        
        return responseMsg;
    }
    /**
     * @param args
     */
//    public static void main(String[] args) {
//        // TODO Auto-generated method stub
//        MpesaB2CRequestTrxClient rqtTrx = new MpesaB2CRequestTrxClient();
//    }

}

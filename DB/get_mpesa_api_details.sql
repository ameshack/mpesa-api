USE `forge`;
DROP procedure IF EXISTS `get_mpesa_api_details`;


DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_mpesa_api_details`(IN server_name varchar(20),
IN reqTimestamp varchar(25),IN trx_type varchar(45),IN dest_acc varchar(20),
IN amount decimal(15,2),IN orig_trx_id varchar(45),IN source_acc varchar(45),
IN origin_id INTEGER,IN origin_name varchar(45),IN mpesa_command_code varchar(5),
IN account_reference varchar(70),IN organization_code varchar(45),OUT mpesa_sp_id varchar(10),
OUT mpesa_sp_password varchar(45),OUT mpesa_service_id varchar(15),
OUT mpesa_command_id varchar(64),OUT mpesa_language_code varchar(15),
OUT mpesa_queue_timeout_url varchar(100),OUT mpesa_third_party_id varchar(15),
OUT mpesa_caller_password varchar(45),OUT mpesa_check_sum varchar(15),
OUT mpesa_result_url varchar(100),OUT mpesa_request_url varchar(100),
OUT mpesa_query_trx_url varchar(100),OUT mpesa_identifier varchar(45),
OUT mpesa_initiator_password varchar(45),OUT mpesa_short_code varchar(10),
OUT mpesa_primary_party_id INTEGER,OUT mpesa_receiver_id INTEGER,
OUT mpesa_access_id INTEGER,OUT mpesa_access_identifier varchar(15),
OUT mpesa_key_owner INTEGER,OUT mpesa_key_store varchar(100),
OUT mpesa_key_store_password varchar(45),OUT mpesa_caller_type INTEGER,
OUT mpesa_trust_store varchar(100),OUT mpesa_trust_store_password varchar(45),
OUT mpesa_identifier_type INTEGER,OUT mpesa_transaction_id INTEGER,
OUT mpesa_timeout INTEGER,OUT mpesa_sql_state INTEGER,
OUT mpesa_institution_code varchar(45),OUT mpesa_account_id varchar(45),
OUT mpesa_currency_code varchar(5),OUT mpesa_host_ip varchar(15),
OUT mpesa_host_port varchar(11),OUT mpesa_float_account varchar(20),
OUT mpesa_comm_account varchar(20),OUT mpesa_reciever_shortcode varchar(12),
OUT mpesa_merchant_type varchar(45), OUT merchant_identifier_type INTEGER,
OUT mpesa_initiator_cert varchar(100),OUT mpesa_validation_url varchar(100),
OUT mpesa_confirmation_url varchar(100))

BEGIN

		-- DECLARE mpesa_node_id INTEGER;

		DECLARE mpesa_api_cur CURSOR FOR SELECT b.sp_id,AES_DECRYPT(b.sp_password, SHA1('@ut0b073')) sp_password,
						b.service_id,d.command_id,b.language_code,b.queue_timeout_url,
						b.third_party_id,AES_DECRYPT(b.caller_password, SHA1('@ut0b073')) caller_password,
						b.check_sum,b.result_url,b.request_url,b.query_trx_url,
						b.identifier,AES_DECRYPT(b.initiator_password, SHA1('@ut0b073')) initiator_password,
						b.short_code,b.primary_party_id,b.receiver_id,b.access_id,
						b.access_identifier,b.key_owner,b.key_store,b.key_store_password,
						b.caller_type,b.trust_store,b.trust_store_password,
						b.identifier_type,b.initiator_cert,b.validation_url,b.confirmation_url FROM node a INNER JOIN mpesa_api b
						ON a.id = b.node_id INNER JOIN api_account c 
						ON c.idapi_account = b.idapi_account INNER JOIN mpesa_api_command d
						ON b.id = d.mpesa_api_id WHERE a.name = server_name 
						AND c.code = origin_id AND c.status = 'ACTIVE' AND d.command_code LIKE mpesa_command_code 
						ORDER BY a.id ASC LIMIT 1;

	    
		DECLARE EXIT HANDLER FOR NOT FOUND 

			BEGIN 

				CALL log_forge_error (CONCAT('procedure name: get_mpesa_api_details() NO RECORD FOUND FOR',serverName));

			END;

		DECLARE EXIT HANDLER FOR SQLEXCEPTION 

			BEGIN 

				CALL log_forge_error (CONCAT('procedure name: get_mpesa_api_details() SQL EXCEPTION'));

			END;

		DECLARE CONTINUE HANDLER FOR SQLWARNING

			BEGIN 

				CALL log_forge_error (CONCAT('procedure name: get_mpesa_api_details() SQL WARNING'));

			END;

		

		CALL `forge`.`log_mpayment_request`(server_name,curdate(),
			trx_type,dest_acc,amount,orig_trx_id,source_acc,origin_id,
			origin_name,account_reference,@trxID,@inst_code,@acc_id,@currency_code,
			@time_out,@host_ip,@host_port,@float_account,@comm_account,
			@sql_state,@node_uname,@node_passwd,@api_url,@callback_url,@callback_method);

		SET mpesa_transaction_id = @trxID;
		SET mpesa_institution_code = @inst_code;
		SET mpesa_account_id = @acc_id;
		SET mpesa_currency_code = @currency_code;
		SET mpesa_timeout = @time_out;
		SET mpesa_host_ip = @host_ip;
		SET mpesa_host_port = @host_port;
		SET mpesa_float_account = @float_account;
		SET mpesa_comm_account = @comm_account;
		SET mpesa_sql_state = @sql_state;

		/*CASE mpesa_sql_state**/

			/*WHEN 0 THEN**/

				/*CASE mpesa_transaction_id**/

					/*WHEN NOT 0 THEN**/

						OPEN mpesa_api_cur;

						FETCH mpesa_api_cur INTO mpesa_sp_id,mpesa_sp_password,mpesa_service_id,mpesa_command_id,
												mpesa_language_code,mpesa_queue_timeout_url,mpesa_third_party_id,
												mpesa_caller_password,mpesa_check_sum,mpesa_result_url,mpesa_request_url,
												mpesa_query_trx_url,mpesa_identifier,mpesa_initiator_password,
												mpesa_short_code,mpesa_primary_party_id,mpesa_receiver_id,
												mpesa_access_id,mpesa_access_identifier,mpesa_key_owner,mpesa_key_store,
												mpesa_key_store_password,mpesa_caller_type,mpesa_trust_store,
												mpesa_trust_store_password,mpesa_identifier_type,mpesa_initiator_cert,
												mpesa_validation_url,mpesa_confirmation_url;

						CLOSE mpesa_api_cur;

					

		SET mpesa_reciever_shortcode = dest_acc;
		SET mpesa_merchant_type = 'TILL';

		CASE mpesa_command_code

			WHEN 'M6' THEN
			
				SET merchant_identifier_type = 2;

			ELSE 

				SET merchant_identifier_type = 4;

		END CASE;
					/*END CASE;**/

			/*END CASE;**/
END$$
DELIMITER ;

GRANT EXECUTE ON PROCEDURE forge.get_mpesa_api_details TO 'restb2c'@'localhost' IDENTIFIED BY 'restb2c';

FLUSH PRIVILEGES;

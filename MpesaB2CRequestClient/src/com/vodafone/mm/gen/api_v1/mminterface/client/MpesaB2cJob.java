/*
    Author: Alex Mbolonzi

 */
package com.vodafone.mm.gen.api_v1.mminterface.client;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import ke.co.ars.entity.ApiInfo;
import ke.co.ars.entity.MpesaRequest;
import ke.co.ars.entity.Transfer;
import ke.co.ars.entity.TrxResponse;

import com.vodafone.mm.gen.api_v1.mminterface.request.RequestMgrServiceStub.ResponseMsg;
import com.vodafone.mm.gen.api_v1.mminterface.response.Response;



import com.vodafone.mm.gen.api_v1.mminterface.client.MpesaB2CGenericAPIRequest;


//import ke.co.mmm.ISOIntegrator.CardToAM;
import ke.co.ars.dao.TransferB2CDAO;
import ke.co.ars.dao.ApiDAO;

public class MpesaB2cJob {
    
    /* Get actual class name to be printed on */
    static Logger log = Logger.getLogger(MpesaB2cJob.class.getName());

    public MpesaB2cJob() {
        
      //PropertiesConfigurator is used to configure logger from properties file
        PropertyConfigurator.configure("/opt/log4j/mpesalog4j.properties");
    }
    
    
    public TrxResponse card2Mobile(String hostName,MpesaRequest mpesaMobileRequest) {

        log.info("Initiating Card2Mobile request.....");

        String trxType = null;
        
        Transfer mpesaTransaction = new Transfer();
        
        mpesaTransaction.setServerName(hostName);
        
        switch(mpesaMobileRequest.getCommandCode()){
        
        case "M1": 
        	//SalaryPayment
        	mpesaTransaction.setDestAccount(mpesaMobileRequest.getDestMSISDN());
        	trxType = "B2C";
        	break;
        case "M2": 
        	//BusinessPayment
        	mpesaTransaction.setDestAccount(mpesaMobileRequest.getDestMSISDN());
        	trxType = "B2C";
        	break;
        	
        case "M3": 
        	//BusinessPaymentWithWithdrawalChargePaid
        	mpesaTransaction.setDestAccount(mpesaMobileRequest.getDestMSISDN());
        	trxType = "B2C";
        	break;
        	
        case "M4": 
        	//'SalaryPaymentWithWithdrawalChargePaid
        	mpesaTransaction.setDestAccount(mpesaMobileRequest.getDestMSISDN());
        	trxType = "B2C";
        	break;
        	
        case "M5": 
        	//PromotionPayment
        	mpesaTransaction.setDestAccount(mpesaMobileRequest.getDestMSISDN());
        	trxType = "B2C";
        	break;
        	
        case "M6": 
        	//BusinessBuyGoods
        	mpesaTransaction.setDestAccount(mpesaMobileRequest.getTillNumber());
        	trxType = "B2B";
        	break;
        	
        case "M7": 
        	//BusinessPayBill
        	mpesaTransaction.setDestAccount(mpesaMobileRequest.getPaybill());
        	trxType = "B2B";
        	break;
        	
        case "M8": 
        	//DisburseFundsToBusiness
        	mpesaTransaction.setDestAccount(mpesaMobileRequest.getPaybill());
        	trxType = "B2B";
        	break;
        	
        case "M9": 
        	//BusinessToBusinessTransfer
        	mpesaTransaction.setDestAccount(mpesaMobileRequest.getPaybill());
        	trxType = "B2B";
        	break;
        case "M10": 
        	//BusinessTransferFromMMFToUtility
        	mpesaTransaction.setDestAccount(mpesaMobileRequest.getPaybill());
        	trxType = "B2B";
        	break;
        	
        case "M11": 
        	//BusinessTransferFromUtilityToMMF
        	mpesaTransaction.setDestAccount(mpesaMobileRequest.getPaybill());
        	trxType = "B2B";
        	break;
        	
        case "M12": 
        	//MerchantToMerchantTransfer
        	mpesaTransaction.setDestAccount(mpesaMobileRequest.getPaybill());
        	trxType = "B2B";
        	break;
        	
        case "M13": 
        	//MerchantTransferFromMerchantToWorking
        	mpesaTransaction.setDestAccount(mpesaMobileRequest.getPaybill());
        	trxType = "B2B";
        	break;
        	
        case "M14": 
        	//MerchantTransferFromWorkingToMerchant
        	mpesaTransaction.setDestAccount(mpesaMobileRequest.getPaybill());
        	trxType = "B2B";
        	break;
        	
        case "M15": 
        	//MerchantServicesMMFAccountTransfer
        	mpesaTransaction.setDestAccount(mpesaMobileRequest.getPaybill());
        	trxType = "B2B";
        	break;
        	
        case "M16": 
        	//'AgencyFloatAdvance
        	mpesaTransaction.setDestAccount(mpesaMobileRequest.getPaybill());
        	trxType = "B2B";
        	break;
        default:
        	
        	mpesaTransaction.setDestAccount(mpesaMobileRequest.getDestMSISDN());
        	trxType = "B2C";
        	break;
        }
        
        
        mpesaTransaction.setTransactionType(trxType);
        mpesaTransaction.setAmount(mpesaMobileRequest.getAmount());
        mpesaTransaction.setTrxCode(mpesaMobileRequest.getOrigTrxCode());
        mpesaTransaction.setSourceMSISDN(mpesaMobileRequest.getSenderMSISDN());
        mpesaTransaction.setOrigID(mpesaMobileRequest.getOrigID());
        mpesaTransaction.setOrig(mpesaMobileRequest.getOrig());
        mpesaTransaction.setCommandCode(mpesaMobileRequest.getCommandCode());
        mpesaTransaction.setPaybill(mpesaMobileRequest.getPaybill());
        mpesaTransaction.setTillNumber(mpesaMobileRequest.getTillNumber());
        mpesaTransaction.setAccountReference(mpesaMobileRequest.getAccountReference());
        mpesaTransaction.setBusinessNumber(mpesaMobileRequest.getBusinessNumber());

        ApiDAO mpesaApiDAO = new ApiDAO();
        
        ApiInfo mpesaApiInfo = new ApiInfo();
        
        log.info("Getting API details for " + hostName);
        
        mpesaApiInfo = mpesaApiDAO.getApiDetails(mpesaTransaction);
            
        Response mpesaResponse;
        
//        TrxRequest mpesaTransferRequest;
        
        ResponseMsg responseMsg;
        
        TrxResponse trxResponseStatus = new TrxResponse();
        
        switch(mpesaApiInfo.getStatusCode()){
        
        case 0:
        	
//        	mpesaTransferRequest = new TrxRequest();
//        	
//        	mpesaTransferRequest.setTransactionID(mpesaApiInfo.getTransactionID());
//            mpesaTransferRequest.setMsisdn(mpesaMobileRequest.getDestMSISDN());
//            mpesaTransferRequest.setAmount(mpesaMobileRequest.getAmount());
//            mpesaRequest.setTimeout(mpesaApiInfo.getTimeout());
//            mpesaRequest.setISOServerIP(mpesaApiInfo.getISOServerIP());
//            mpesaRequest.setISOServerPort(mpesaApiInfo.getISOServerPort());
            
            try {
                
                MpesaB2CGenericAPIRequest mobileDeposit = new MpesaB2CGenericAPIRequest();
                
                responseMsg = new ResponseMsg();
                
                switch(trxType){
                
                case "B2C":
                	
                	responseMsg = mobileDeposit.SendB2Crequest(mpesaApiInfo,mpesaMobileRequest);
                	
                	break;
                	
                case "B2B":
                	
                	responseMsg = mobileDeposit.SendB2Brequest(mpesaApiInfo,mpesaMobileRequest);
                	
                	break;
                }
                
                
                String responseString = responseMsg.getResponseMsg();
                
                JAXBContext jc;
                                
                jc = JAXBContext.newInstance("com.vodafone.mm.gen.api_v1.mminterface.response");
                
                Unmarshaller unmarshaller = jc.createUnmarshaller();
                
                StringBuffer xmlStr = new StringBuffer(responseString);
                
                mpesaResponse = new Response();
                
                mpesaResponse = (Response)((javax.xml.bind.JAXBElement) unmarshaller.unmarshal(new StringReader(xmlStr.toString()))).getValue();
                

                if(mpesaResponse.getResponseCode().equals("0")) {
                    
                    mpesaResponse.setResponseDesc("Success");
                }

                TrxResponse mpesaApiResponse = new TrxResponse();
                
                mpesaApiResponse.setTransactionID(mpesaApiInfo.getTransactionID());
                mpesaApiResponse.setStatusDescription(mpesaResponse.getResponseDesc());
                mpesaApiResponse.setTraceNumber(mpesaResponse.getConversationID());
                mpesaApiResponse.setStatusCode(Integer.valueOf(mpesaResponse.getResponseCode()));
                
                TransferB2CDAO mpesaApiResponseDAO = new TransferB2CDAO();
                
                mpesaApiResponseDAO.updateTransactionResponse(mpesaApiResponse); 
                           
                trxResponseStatus.setStatusCode(Integer.valueOf(mpesaResponse.getResponseCode()));
                trxResponseStatus.setStatusDescription(mpesaResponse.getResponseDesc());

            } catch (JAXBException e) {
                // TODO Auto-generated catch block
//                e.printStackTrace();
                log.error("Exception: ",e.fillInStackTrace());
            }
        	break;
      
        	default:
        		
        		trxResponseStatus.setStatusCode(96);
                trxResponseStatus.setStatusDescription("Server unavailable");
                
        	break;
        }
                
        
//        int waitTime = 7000;
//        
//        
//        try {
//            
//            log.debug("Thread sleep time : " + waitTime);
//            
//            Thread.sleep(waitTime);
//            
//        } catch (InterruptedException e) {
//            // TODO Auto-generated catch block
////            e.printStackTrace();
//            log.error("Exception: ",e.fillInStackTrace());
//        }
        
        return trxResponseStatus;
    }

    public TrxResponse card2MobileRetry() {

        log.info("Initiating Card2MobileRetry request.....");

        ApiDAO mpesaApiDAORetry = new ApiDAO();
        
        ApiInfo mpesaApiInfoRetry = new ApiInfo();
        
        log.info("Getting API details for Unprocessed Trx ");
        
        mpesaApiInfoRetry = mpesaApiDAORetry.getApiDetailsOfUnprocessedTransaction();
            
        Response mpesaResponseRetry;
        
        MpesaRequest mpesaRequestRetry;
        
        ResponseMsg responseMsgRetry;
        
        TrxResponse trxResponseStatusRetry = new TrxResponse();
        
        switch(mpesaApiInfoRetry.getStatusCode()){
        
        case 0:
        	
        	mpesaRequestRetry = new MpesaRequest();
        	
        	mpesaRequestRetry.setOrigTrxCode(mpesaApiInfoRetry.getTransactionID());
            mpesaRequestRetry.setDestMSISDN(mpesaApiInfoRetry.getCreditAcc());
            mpesaRequestRetry.setAmount(mpesaApiInfoRetry.getAmount());
//            mpesaRequest.setTimeout(mpesaApiInfo.getTimeout());
//            mpesaRequest.setISOServerIP(mpesaApiInfo.getISOServerIP());
//            mpesaRequest.setISOServerPort(mpesaApiInfo.getISOServerPort());
            
            try {
                
                MpesaB2CGenericAPIRequest mobileDeposit = new MpesaB2CGenericAPIRequest();
                
                responseMsgRetry = new ResponseMsg();
                
                responseMsgRetry = mobileDeposit.SendB2Crequest(mpesaApiInfoRetry,mpesaRequestRetry);
                
                String responseString = responseMsgRetry.getResponseMsg();
                
                JAXBContext jc;
                                
                jc = JAXBContext.newInstance("com.vodafone.mm.gen.api_v1.mminterface.response");
                
                Unmarshaller unmarshaller = jc.createUnmarshaller();
                
                StringBuffer xmlStr = new StringBuffer(responseString);
                
                mpesaResponseRetry = new Response();
                
                mpesaResponseRetry = (Response)((javax.xml.bind.JAXBElement) unmarshaller.unmarshal(new StringReader(xmlStr.toString()))).getValue();
                

                if(mpesaResponseRetry.getResponseCode().equals("0")) {
                    
                    mpesaResponseRetry.setResponseDesc("Success");
                }

                TrxResponse mpesaApiResponse = new TrxResponse();
                
                mpesaApiResponse.setTransactionID(mpesaApiInfoRetry.getTransactionID());
                mpesaApiResponse.setStatusDescription(mpesaResponseRetry.getResponseDesc());
                mpesaApiResponse.setTraceNumber(mpesaResponseRetry.getConversationID());
                mpesaApiResponse.setStatusCode(Integer.valueOf(mpesaResponseRetry.getResponseCode()));
                
                TransferB2CDAO mpesaApiResponseDAO = new TransferB2CDAO();
                
                mpesaApiResponseDAO.updateTransactionResponse(mpesaApiResponse); 
                           
                trxResponseStatusRetry.setStatusCode(Integer.valueOf(mpesaResponseRetry.getResponseCode()));
                trxResponseStatusRetry.setStatusDescription(mpesaResponseRetry.getResponseDesc());

            } catch (JAXBException e) {
                // TODO Auto-generated catch block
//                e.printStackTrace();
                log.error("Exception: ",e.fillInStackTrace());
            }
        	break;
      
        	default:
        		
        		trxResponseStatusRetry.setStatusCode(96);
                trxResponseStatusRetry.setStatusDescription("Server unavailable");
                
        	break;
        }
                
        
//        int waitTime = 7000;
//        
//        
//        try {
//            
//            log.debug("Thread sleep time : " + waitTime);
//            
//            Thread.sleep(waitTime);
//            
//        } catch (InterruptedException e) {
//            // TODO Auto-generated catch block
////            e.printStackTrace();
//            log.error("Exception: ",e.fillInStackTrace());
//        }
        
        return trxResponseStatusRetry;
    }
}



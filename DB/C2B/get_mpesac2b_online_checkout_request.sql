USE `forge`;
DROP procedure IF EXISTS `get_mpesac2b_confirm_online_checkout_request`;


DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_mpesac2b_confirm_online_checkout_request`(IN serverName varchar(20),
IN origTrxID varchar(45),IN origin_id bigint(20),IN origin_name varchar(45),
OUT trxID INTEGER,OUT acc_id varchar(45),OUT time_out INTEGER,
OUT sql_state INTEGER,OUT node_uname varchar(45),OUT node_passwd VARCHAR(255),
OUT api_url varchar(100),OUT c2b_msisdn varchar(45),OUT c2b_trace_data VARCHAR(255))
BEGIN

	DECLARE payLoad varchar(200);

	DECLARE trxType varchar(45);

	DECLARE c2b_checkout_cur CURSOR FOR SELECT a.id,a.senderMSISDN,a.responseData FROM transfers a 
										WHERE TRIM(a.origTrxCode) = TRIM(origTrxID) AND 
										a.origID = origin_id AND TRIM(a.orig) = TRIM(origin_name) 
										ORDER BY a.id DESC LIMIT 1;

	DECLARE EXIT HANDLER FOR SQLSTATE '23000' 

	BEGIN

		CALL log_forge_error (CONCAT('procedure name: get_mpesac2b_confirm_online_checkout_request() Constraint Violation.'));

		SET sql_state = 23000;
	END;

        DECLARE EXIT HANDLER FOR NOT FOUND 

	BEGIN 

		CALL log_volt_error (CONCAT('procedure name: get_mpesac2b_confirm_online_checkout_request() NO RECORD FOUND FOR ',origTrxID));

		SET  sql_state = 1329; 

	END;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION 

	BEGIN 

		CALL log_forge_error (CONCAT('procedure name: get_mpesac2b_confirm_online_checkout_request() SQL EXCEPTION'));

	END;

	DECLARE CONTINUE HANDLER FOR SQLWARNING 

	BEGIN 

		CALL log_forge_error (CONCAT('procedure name: get_mpesac2b_confirm_online_checkout_request() SQL WARNING'));

	END;
	/*get the node details and check its status **/

	CALL get_node_details (serverName,@nodeID,@extAccID,@nodeName,@hostIP,@hostPort,@timeOut,
							@uName,@pwd,@nodeURL,@nodeStatus,@instCode,@accID,@currencyCode,
							@float_bank_acc,@comm_bank_acc,@nodeCallbackURL,@nodeCallbackMethod);

	-- SET inst_code = @instCode;
	SET acc_id = @accID;
	-- SET currency_code = @currencyCode;
	SET time_out = @timeOut;
	-- SET host_ip = @hostIP;
	-- SET host_port = @hostPort;
	-- SET float_account = @float_bank_acc;
	-- SET comm_account = @comm_bank_acc; 
	SET node_uname = @uName;
	SET node_passwd = @pwd;
	SET api_url = @nodeURL;
	
	/*Check the node status if status is zero node is up, log transaction details**/
	CASE @nodeStatus

		WHEN 0 THEN

			SET trxType = 'C2B confirm transaction';

			SET payLoad = CONCAT(IFNULL(origTrxID,'0'),'|',IFNULL(origin_id,'0'),'|',IFNULL(origin_name,'DUMMY'),'|',IFNULL(serverName,'srv'));

			INSERT INTO hits (req_timestamp,trx_type,payload) VALUES (NOW(),trxType,payLoad);
 
			COMMIT;

			OPEN c2b_checkout_cur;

			FETCH c2b_checkout_cur INTO trxID,c2b_msisdn,c2b_trace_data;

			CLOSE c2b_checkout_cur;

			SET sql_state = 0;

		ELSE
		
			SET trxID = 0;

			SET sql_state = 95;

	END CASE;

END$$
DELIMITER ;

GRANT EXECUTE ON PROCEDURE forge.get_mpesac2b_confirm_online_checkout_request TO 'restb2c'@'localhost' IDENTIFIED BY 'restb2c';

FLUSH PRIVILEGES;

/*
    Author: Alex Mbolonzi

 */
package com.sendy.entity;

public class SendyResponse {

    private int responseCode;
    
    private String responseDetails;

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseDetails() {
		return responseDetails;
	}

	public void setResponseDetails(String responseDetails) {
		this.responseDetails = responseDetails;
	}
}

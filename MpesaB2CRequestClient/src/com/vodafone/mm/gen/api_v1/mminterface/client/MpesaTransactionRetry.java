/*
    Author: Alex Mbolonzi

 */
package com.vodafone.mm.gen.api_v1.mminterface.client;

import java.io.IOException;
import java.io.StringWriter;
import java.math.BigInteger;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import ke.co.ars.entity.ApiInfo;

//import com.vodafone.mm.gen.api_v1.mminterface.entity.Node;
import com.vodafone.mm.gen.api_v1.mminterface.request.ObjectFactory;
import com.vodafone.mm.gen.api_v1.mminterface.request.ParameterType;
import com.vodafone.mm.gen.api_v1.mminterface.request.Request;
import com.vodafone.mm.gen.api_v1.mminterface.request.RequestMgrServiceStub;
import com.vodafone.mm.gen.api_v1.mminterface.request.Request.Identity;
import com.vodafone.mm.gen.api_v1.mminterface.request.Request.Transaction;
import com.vodafone.mm.gen.api_v1.mminterface.request.Request.Identity.AccessDevice;
import com.vodafone.mm.gen.api_v1.mminterface.request.Request.Identity.Caller;
import com.vodafone.mm.gen.api_v1.mminterface.request.Request.Identity.Initiator;
import com.vodafone.mm.gen.api_v1.mminterface.request.Request.Identity.PrimartyParty;
import com.vodafone.mm.gen.api_v1.mminterface.request.Request.Identity.ReceiverParty;
import com.vodafone.mm.gen.api_v1.mminterface.request.Request.Transaction.Parameters;
import com.vodafone.mm.gen.api_v1.mminterface.request.Request.Transaction.ReferenceData;
import com.vodafone.mm.gen.api_v1.mminterface.request.Request.Transaction.Parameters.Parameter;
import com.vodafone.mm.gen.api_v1.mminterface.request.RequestMgrServiceStub.RequestMsg;
import com.vodafone.mm.gen.api_v1.mminterface.request.RequestMgrServiceStub.RequestSOAPHeader;
import com.vodafone.mm.gen.api_v1.mminterface.request.RequestMgrServiceStub.RequestSOAPHeaderE;
import com.vodafone.mm.gen.api_v1.mminterface.request.RequestMgrServiceStub.ResponseMsg;
import com.vodafone.mm.gen.api_v1.mminterface.client.SHAHashEncryptor;
import com.vodafone.mm.gen.api_v1.mminterface.client.SSLKey;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

public class MpesaTransactionRetry {

    /* Get actual class name to be printed on */
    static Logger log = Logger.getLogger(MpesaTransactionRetry.class.getName());
    
    public MpesaTransactionRetry(){
        
      //PropertiesConfigurator is used to configure logger from properties file
        PropertyConfigurator.configure("/opt/log4j/mpesalog4j.properties");   
    }
    
    public ResponseMsg ResendB2Crequest(ApiInfo apiInfo) throws JAXBException {
        
        log.info("Initiating ResendB2Crequest.....");

        SimpleDateFormat timeStamp = new SimpleDateFormat("yyyyMMddHHmmss");
        
        Date date = new Date();
        
        String passwordString = apiInfo.getSp_id() + apiInfo.getSp_password() + timeStamp.format(date);
        String hashedPasswordString = "";
        
        SHAHashEncryptor encryptor = new SHAHashEncryptor();
        
        try {
            hashedPasswordString = encryptor.hashPassword(passwordString);
        } catch (Exception e1) {
            // TODO Auto-generated catch block
//            e1.printStackTrace();
            log.error("Exception: ",e1.fillInStackTrace());
        }
        
        RequestSOAPHeader requestSOAPHeader = new RequestSOAPHeader();
        
        //set the SOAP Header details
        requestSOAPHeader.setSpId(apiInfo.getSp_id());
        requestSOAPHeader.setSpPassword(hashedPasswordString);
        requestSOAPHeader.setServiceId(apiInfo.getService_id());
        requestSOAPHeader.setTimeStamp(timeStamp.format(date));
        
        Transaction transaction = new Transaction();
        //set the transaction details
        transaction.setCommandID(apiInfo.getCommand_id());
        transaction.setLanguageCode(apiInfo.getLanguage_code());
        transaction.setOriginatorConversationID(apiInfo.getTransactionID()+ "R" + timeStamp.format(date));
        transaction.setConversationID("");
        transaction.setRemark("0");
//        transaction.setEncryptedParameters("");
        
        Parameter trxParam = new Parameter();
        
        Parameter trxParam1 = new Parameter();
        
        Parameters trxParams = new Parameters();
        
        trxParam1.setKey("Amount");
        trxParam1.setValue(apiInfo.getAmount());
        
        trxParams.getParameter().add(trxParam1);
        
        transaction.setParameters(trxParams);
        
        
        ParameterType trxParamType = new ParameterType();
        
        ParameterType trxParamType1 = new ParameterType();
        
        ReferenceData refData = new ReferenceData();
        //set reference data
        trxParamType.setKey("QueueTimeoutURL");
        trxParamType.setValue(apiInfo.getQueue_timeout_url());
        refData.getReferenceItem().add(trxParamType);
        
        transaction.setReferenceData(refData);
        
        SimpleDateFormat timeSatmp2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSS'Z'");
        
        transaction.setTimestamp(timeSatmp2.format(date.getTime()));
        
        Caller caller = new Caller();
        //set caller details
        caller.setCallerType(BigInteger.valueOf(apiInfo.getCallerType()));
        caller.setThirdPartyID(apiInfo.getThird_party_id());
        caller.setPassword(apiInfo.getCaller_password());
        caller.setCheckSum(apiInfo.getCheck_sum());
        caller.setResultURL(apiInfo.getResult_url());
        
        SSLKey fParser = new SSLKey();
        
        byte[] key = null;
        
        try {
            key = fParser.certKey(apiInfo.getInitiator_password(),apiInfo.getInitiator_cert());
        } catch (IOException e1) {
            // TODO Auto-generated catch block
//            e1.printStackTrace();
            log.error("Exception: ",e1.fillInStackTrace());
        }
        
        String securityCredential = null;

        securityCredential = Base64.encode(key);

        Initiator initiator = new Initiator();
        //set initiator details
        initiator.setIdentifierType(BigInteger.valueOf(apiInfo.getIdentifierType()));
        initiator.setIdentifier(apiInfo.getIdentifier());
        initiator.setSecurityCredential(securityCredential);
        initiator.setShortCode(apiInfo.getShort_code());
        
        int primaryPartyID = apiInfo.getPrimary_party_id();

        PrimartyParty primaryParty = new PrimartyParty();
        //set primary party details
        primaryParty.setIdentifierType(BigInteger.valueOf(primaryPartyID));
        primaryParty.setIdentifier(apiInfo.getShort_code());
        primaryParty.setShortCode(apiInfo.getShort_code());
        
        ReceiverParty receiverParty = new ReceiverParty();
        //set Reciever party detail
        receiverParty.setIdentifierType(BigInteger.valueOf(apiInfo.getReceiver_id()));
        receiverParty.setIdentifier(apiInfo.getCreditAcc());
        receiverParty.setShortCode(apiInfo.getShort_code());
        
        AccessDevice accessDevice = new AccessDevice();
        //set Access Device details
        accessDevice.setIdentifierType(BigInteger.valueOf(apiInfo.getAccess_id()));
        accessDevice.setIdentifier(apiInfo.getAccess_identifier());
        
        Identity identity = new Identity();
        
        identity.setCaller(caller);
        identity.setInitiator(initiator);
        identity.setPrimartyParty(primaryParty);
        identity.setReceiverParty(receiverParty);
        identity.setAccessDevice(accessDevice);
        
        Request request = new Request();
        
        request.setTransaction(transaction);
        
        request.setIdentity(identity);
        
        int keyOwner = apiInfo.getKey_owner();
        //set request details
        request.setKeyOwner(BigInteger.valueOf(keyOwner));
        
        
        ObjectFactory objFactory = new ObjectFactory();
        
        JAXBElement<Request> rq = objFactory.createRequest(request);
        
        Request formatedRequest = rq.getValue();
        
        JAXBContext jc = JAXBContext.newInstance("com.vodafone.mm.gen.api_v1.mminterface.request");
        
        Marshaller marshaller = jc.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
//        marshaller.marshal(formatedRequest, System.out);
        
        StringWriter stringWriter = new StringWriter();

        marshaller.marshal(formatedRequest,stringWriter);
        
        // Convert StringWriter to String
        String xml = stringWriter.toString();
        

//        System.out.println(xml);
        log.info("Request xml: " + xml);
        
        RequestMsg requestMsg = new RequestMsg();
        
        requestMsg.setRequestMsg(xml);
        
        ResponseMsg responseMsg = new ResponseMsg();
       
        try {
            
            //set necessary keystore properties - using a p12 file
            System.setProperty("javax.net.ssl.keyStore",apiInfo.getKey_store());
            System.setProperty("javax.net.ssl.keyStorePassword",apiInfo.getKey_store_password());
//            System.setProperty("javax.net.ssl.keyStoreType", "PKCS12");       
                        
            //set necessary truststore properties - using JKS
            System.setProperty("javax.net.ssl.trustStore",apiInfo.getTrust_store());
            System.setProperty("javax.net.ssl.trustStorePassword",apiInfo.getTrust_store_password());
            
            long timeout = apiInfo.getTimeout();

            log.info("Timeout currently set to: " + timeout);    

            RequestMgrServiceStub requestMgrServiceStub = new RequestMgrServiceStub(apiInfo.getRequestUrl());
            
            requestMgrServiceStub._getServiceClient().getOptions().setTimeOutInMilliSeconds(timeout);
            
            
           try {
               
        	   RequestSOAPHeaderE requestSOAPHeaderE = new RequestSOAPHeaderE();
               
               requestSOAPHeaderE.setRequestSOAPHeader(requestSOAPHeader);
               
               responseMsg = requestMgrServiceStub.genericAPIRequest(requestMsg,requestSOAPHeaderE);
            
        } catch (Exception e) {
            // TODO Auto-generated catch block
           // e.printStackTrace();       
            log.error("Exception: ",e.fillInStackTrace());
        }
      
            
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
//            e.printStackTrace();
            log.error("Exception: ",e.fillInStackTrace());
        }
        
            return responseMsg;
        }


}

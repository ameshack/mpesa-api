USE `forge`;
DROP procedure IF EXISTS `update_c2b_checkout_transaction`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_c2b_checkout_transaction`(IN trxID INTEGER, 
IN c2b_return_code varchar(10),IN c2b_response_desc varchar(255),
IN mpesa_trx_number varchar(70))
BEGIN

DECLARE EXIT HANDLER FOR NOT FOUND 

BEGIN 

	CALL log_forge_error (CONCAT('procedure name: update_c2b_checkout_transaction() NO RECORD FOUND FOR ',trxID));

END;

DECLARE EXIT HANDLER FOR SQLEXCEPTION 

BEGIN 

	CALL log_forge_error (CONCAT('procedure name: update_c2b_checkout_transaction() SQL EXCEPTION'));

END;

DECLARE CONTINUE HANDLER FOR SQLWARNING 

BEGIN 

	CALL log_forge_error (CONCAT('procedure name: update_update_c2b_checkout_transaction() SQL WARNING'));

END;

	UPDATE transfers SET confirm_returncode = c2b_return_code,confirm_description = c2b_response_desc,confirm_trx_id = mpesa_trx_number WHERE id = trxID LIMIT 1;

	COMMIT;

END$$

DELIMITER ;

GRANT EXECUTE ON PROCEDURE forge.update_c2b_checkout_transaction TO 'restb2c'@'localhost' IDENTIFIED BY 'restb2c';

FLUSH PRIVILEGES;

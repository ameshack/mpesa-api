/**
 * ResultMgrServiceMessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
package com.vodafone.mm.gen.api_v1.mminterface.resultws.result;

import javax.xml.stream.XMLStreamConstants;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.impl.llom.OMTextImpl;
import org.apache.axiom.soap.SOAPEnvelope;
import org.apache.axiom.soap.SOAPFactory;

/**
 * ResultMgrServiceMessageReceiverInOut message receiver
 */

public class ResultMgrServiceMessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver {

    public void invokeBusinessLogic(org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext)
            throws org.apache.axis2.AxisFault {

        try {

            // get the implementation class for the Web Service
            Object obj = getTheImplementationObject(msgContext);

            MpesaB2CGenericAPIResult skel = (MpesaB2CGenericAPIResult) obj;
            // Out Envelop
            org.apache.axiom.soap.SOAPEnvelope envelope = null;
            // Find the axisOperation that has been set by the Dispatch phase.
            org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext().getAxisOperation();
            if (op == null) {
                throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
            }

            java.lang.String methodName;
            if ((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJavaIdentifier(op.getName().getLocalPart())) != null)) {

                if ("genericAPIResult".equals(methodName)) {

                    com.vodafone.mm.gen.api_v1.mminterface.resultws.result.ResponseMsg responseMsg1 = null;
                    com.vodafone.mm.gen.api_v1.mminterface.resultws.result.ResultMsg wrappedParam =
                            (com.vodafone.mm.gen.api_v1.mminterface.resultws.result.ResultMsg) fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.vodafone.mm.gen.api_v1.mminterface.resultws.result.ResultMsg.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));

                    responseMsg1 =

                            skel.genericAPIResult(wrappedParam);

                    envelope = toEnvelope(getSOAPFactory(msgContext), responseMsg1, false, new javax.xml.namespace.QName("http://api-v1.gen.mm.vodafone.com/mminterface/result",
                            "genericAPIResult"));

                } else {
                    throw new java.lang.RuntimeException("method not found");
                }

                newMsgContext.setEnvelope(envelope);
            }
        } catch (java.lang.Exception e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }
    }

    //
    private org.apache.axiom.om.OMElement toOM(com.vodafone.mm.gen.api_v1.mminterface.resultws.result.ResultMsg param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

        try {
            return param.getOMElement(com.vodafone.mm.gen.api_v1.mminterface.resultws.result.ResultMsg.MY_QNAME,
                    org.apache.axiom.om.OMAbstractFactory.getOMFactory());
        } catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }

    }

    private org.apache.axiom.om.OMElement toOM(com.vodafone.mm.gen.api_v1.mminterface.resultws.result.ResponseMsg param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

        try {
            return param.getOMElement(com.vodafone.mm.gen.api_v1.mminterface.resultws.result.ResponseMsg.MY_QNAME,
                    org.apache.axiom.om.OMAbstractFactory.getOMFactory());
        } catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }

    }

    private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.vodafone.mm.gen.api_v1.mminterface.resultws.result.ResponseMsg param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
            throws org.apache.axis2.AxisFault {
        try {
            org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

            emptyEnvelope.getBody().addChild(param.getOMElement(com.vodafone.mm.gen.api_v1.mminterface.resultws.result.ResponseMsg.MY_QNAME, factory));

//            SOAPFactory factory1 = OMAbstractFactory.getSOAP11Factory();
//            SOAPEnvelope emptyEnvelope = factory1.getDefaultEnvelope();
//            
            OMNamespace methodNs = factory.createOMNamespace("http://api-v1.gen.mm.vodafone.com/mminterface/request","req");
            emptyEnvelope.declareNamespace(methodNs);
//            
//            String xmlForCdata = param.getResponseMsg();
//            
//            OMElement xmlElement= factory.createOMElement("req:ResponseMsg", "","");
//            emptyEnvelope.getBody().addChild(xmlElement);
//            
//            OMTextImpl omText = (OMTextImpl) xmlElement.getOMFactory().createOMText(xmlElement, xmlForCdata, XMLStreamConstants.CDATA);
//            xmlElement.addChild(omText);
            
            return emptyEnvelope;
        } catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }
    }

    private com.vodafone.mm.gen.api_v1.mminterface.resultws.result.ResponseMsg wrapGenericAPIResult() {
        com.vodafone.mm.gen.api_v1.mminterface.resultws.result.ResponseMsg wrappedElement = new com.vodafone.mm.gen.api_v1.mminterface.resultws.result.ResponseMsg();
        return wrappedElement;
    }

    /**
     * get the default envelope
     */
    private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory) {
        return factory.getDefaultEnvelope();
    }

    private java.lang.Object fromOM(
            org.apache.axiom.om.OMElement param,
            java.lang.Class type,
            java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault {

        try {

            if (com.vodafone.mm.gen.api_v1.mminterface.resultws.result.ResultMsg.class.equals(type)) {

                return com.vodafone.mm.gen.api_v1.mminterface.resultws.result.ResultMsg.Factory.parse(param.getXMLStreamReaderWithoutCaching());

            }

            if (com.vodafone.mm.gen.api_v1.mminterface.resultws.result.ResponseMsg.class.equals(type)) {

                return com.vodafone.mm.gen.api_v1.mminterface.resultws.result.ResponseMsg.Factory.parse(param.getXMLStreamReaderWithoutCaching());

            }

        } catch (java.lang.Exception e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }
        return null;
    }

    /**
     * A utility method that copies the namepaces from the SOAPEnvelope
     */
    private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env) {
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
            org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
            returnMap.put(ns.getPrefix(), ns.getNamespaceURI());
        }
        return returnMap;
    }

    private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
        org.apache.axis2.AxisFault f;
        Throwable cause = e.getCause();
        if (cause != null) {
            f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
        } else {
            f = new org.apache.axis2.AxisFault(e.getMessage());
        }

        return f;
    }

}// end of class

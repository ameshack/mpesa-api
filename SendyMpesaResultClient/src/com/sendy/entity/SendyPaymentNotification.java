package com.sendy.entity;

public class SendyPaymentNotification {

	private String orig_trx_id;
	private String mpesa_trx_date;
	private String mpesa_trx_id;
	private String amount;
	private String trx_status;
	private String returnCode;
	private String description;
	private String trx_id;
	private String msisdn;
	
	public String getOrig_trx_id() {
		return orig_trx_id;
	}
	public void setOrig_trx_id(String orig_trx_id) {
		this.orig_trx_id = orig_trx_id;
	}
	public String getMpesa_trx_date() {
		return mpesa_trx_date;
	}
	public void setMpesa_trx_date(String mpesa_trx_date) {
		this.mpesa_trx_date = mpesa_trx_date;
	}
	public String getMpesa_trx_id() {
		return mpesa_trx_id;
	}
	public void setMpesa_trx_id(String mpesa_trx_id) {
		this.mpesa_trx_id = mpesa_trx_id;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getTrx_status() {
		return trx_status;
	}
	public void setTrx_status(String trx_status) {
		this.trx_status = trx_status;
	}
	public String getReturnCode() {
		return returnCode;
	}
	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTrx_id() {
		return trx_id;
	}
	public void setTrx_id(String trx_id) {
		this.trx_id = trx_id;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
}

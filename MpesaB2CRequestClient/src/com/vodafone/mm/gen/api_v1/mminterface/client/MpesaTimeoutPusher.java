package com.vodafone.mm.gen.api_v1.mminterface.client;

import java.io.UnsupportedEncodingException;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Date;

import ke.co.ars.dao.ApiDAO;
import ke.co.ars.entity.ApiInfo;
import ke.co.ars.entity.Transfer;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.vodafone.mm.gen.api_v1.mminterface.request.RequestMgrServiceStub;
import com.vodafone.mm.gen.api_v1.mminterface.request.RequestMgrServiceStub.RequestMsg;
import com.vodafone.mm.gen.api_v1.mminterface.request.RequestMgrServiceStub.RequestSOAPHeader;
import com.vodafone.mm.gen.api_v1.mminterface.request.RequestMgrServiceStub.RequestSOAPHeaderE;
import com.vodafone.mm.gen.api_v1.mminterface.request.RequestMgrServiceStub.ResponseMsg;
import com.vodafone.mm.gen.api_v1.mminterface.client.SHAHashEncryptor;

public class MpesaTimeoutPusher {
	
	/* Get actual class name to be printed on */
    static Logger log = Logger.getLogger(MpesaTimeoutPusher.class.getName());

	public MpesaTimeoutPusher(){
		
		//PropertiesConfigurator is used to configure logger from properties file
        PropertyConfigurator.configure("/opt/log4j/mpesalog4j.properties");        
	}
	
	public void timeoutRequest(String transactionID, String originalRequest){
		
		byte[] decodedString = Base64.decodeBase64(StringUtils.getBytesUtf8(originalRequest));
		
		String originReq = null;
		
		try {
            originReq = new String(decodedString, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
		
		Transfer timeoutTransfer = new Transfer();
		
		timeoutTransfer.setTransferID(Integer.valueOf(transactionID));
		
		ApiInfo trxTimeoutApiInfo = new ApiInfo();
		
		ApiDAO timeoutApiDAO = new ApiDAO();
		
		trxTimeoutApiInfo = timeoutApiDAO.getApiDetailsOfTransaction(timeoutTransfer);
		
		log.info("Initiating Timeouted GenericAPIRequest.....");

        SimpleDateFormat timeStamp = new SimpleDateFormat("yyyyMMddHHmmss");
        
        Date date = new Date();
        
        String passwordString = trxTimeoutApiInfo.getSp_id() + trxTimeoutApiInfo.getSp_password() + timeStamp.format(date);
        
        String hashedPasswordString = "";
        
        SHAHashEncryptor encryptor = new SHAHashEncryptor();
        
        try {
            hashedPasswordString = encryptor.hashPassword(passwordString);
        } catch (Exception e1) {
            // TODO Auto-generated catch block
//            e1.printStackTrace();
            log.error("Exception: ",e1.fillInStackTrace());
        }
        
        RequestSOAPHeader requestSOAPHeader = new RequestSOAPHeader();
        
        //set the SOAP Header details
        requestSOAPHeader.setSpId(trxTimeoutApiInfo.getSp_id());
        requestSOAPHeader.setSpPassword(hashedPasswordString);
        requestSOAPHeader.setServiceId(trxTimeoutApiInfo.getService_id());
        requestSOAPHeader.setTimeStamp(timeStamp.format(date));
        
        log.info("Timedout Request : " + originReq);
        
        RequestMsg requestMsg = new RequestMsg();
        
        requestMsg.setRequestMsg(originReq);
        
        ResponseMsg responseMsg = new ResponseMsg();
       
        try {
            
            //set necessary keystore properties - using a p12 file
            System.setProperty("javax.net.ssl.keyStore",trxTimeoutApiInfo.getKey_store());
            System.setProperty("javax.net.ssl.keyStorePassword",trxTimeoutApiInfo.getKey_store_password());
//            System.setProperty("javax.net.ssl.keyStoreType", "PKCS12");       
                        
            //set necessary truststore properties - using JKS
            System.setProperty("javax.net.ssl.trustStore",trxTimeoutApiInfo.getTrust_store());
            System.setProperty("javax.net.ssl.trustStorePassword",trxTimeoutApiInfo.getTrust_store_password());
            
            long timeout = trxTimeoutApiInfo.getTimeout();

            log.info("Timeout currently set to: " + timeout);    

            RequestMgrServiceStub requestMgrServiceStub = new RequestMgrServiceStub(trxTimeoutApiInfo.getRequestUrl());
            
            requestMgrServiceStub._getServiceClient().getOptions().setTimeOutInMilliSeconds(timeout);
            
            
           try {
               
        	   RequestSOAPHeaderE requestSOAPHeaderE = new RequestSOAPHeaderE();
               
               requestSOAPHeaderE.setRequestSOAPHeader(requestSOAPHeader);
               
               responseMsg = requestMgrServiceStub.genericAPIRequest(requestMsg,requestSOAPHeaderE);
            
        } catch (Exception e) {
            // TODO Auto-generated catch block
           // e.printStackTrace();       
            log.error("Exception: ",e.fillInStackTrace());
        }
      
            
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
//            e.printStackTrace();
            log.error("Exception: ",e.fillInStackTrace());
        }
        
//            return responseMsg;
//        }
//		System.out.print(originReq);
	}
	
//	public static void main(String[] args) {
//		
//		MpesaTimeoutPusher pusher = new MpesaTimeoutPusher();
//	}

}

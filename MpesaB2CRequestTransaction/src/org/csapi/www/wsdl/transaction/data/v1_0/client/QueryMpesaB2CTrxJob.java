/*
    Author: Alex Mbolonzi

 */
package org.csapi.www.wsdl.transaction.data.v1_0.client;

import java.io.IOException;
import java.io.StringReader;
import java.util.Random;
import java.util.StringTokenizer;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.csapi.www.wsdl.transaction.data.v1_0.dao.AMB2CDAO;
import org.csapi.www.wsdl.transaction.data.v1_0.dao.ApiDAO;
import org.csapi.www.wsdl.transaction.data.v1_0.dao.NodeDAO;
import org.csapi.www.wsdl.transaction.data.v1_0.entity.ApiInfo;
import org.csapi.www.wsdl.transaction.data.v1_0.entity.Hits;
import org.csapi.www.wsdl.transaction.data.v1_0.entity.MMMMpesaRequest;
import org.csapi.www.wsdl.transaction.data.v1_0.entity.MMMMpesaResponse;
import org.csapi.www.wsdl.transaction.data.v1_0.entity.Node;
import org.csapi.www.wsdl.transaction.data.v1_0.entity.Transfer;

//import ke.co.mmm.ISOEntity.MMMISORequest;
//import ke.co.mmm.ISOEntity.MMMISOResponse;
//import ke.co.mmm.ISOEntity.MobileSubscriber;
import org.csapi.www.wsdl.transaction.data.v1_0.response.Response;
//import ke.co.mmm.ISOIntegrator.CardToAM;

public class QueryMpesaB2CTrxJob {
    
    /* Get actual class name to be printed on */
    static Logger log = Logger.getLogger(QueryMpesaB2CTrxJob.class.getName());

    public QueryMpesaB2CTrxJob() {
        
      //PropertiesConfigurator is used to configure logger from properties file
        PropertyConfigurator.configure("/opt/log4j/mpesalog4j.properties");
    }
    
    
    public String queryMpesaB2CTransaction(String hostName, String destmobile, 
            String sourcemobile, String trxID, int sourceID, 
            String source) {

        log.info("Initiating Query  Mpesa B2C transaction request.....");
        
        String destmsisdn = destmobile;
        
        String sourcemsisdn = sourcemobile;

        String transactionID = trxID;
        
        int origID = sourceID;
        
        String orig = source;

        String payload = destmsisdn + "|" + transactionID + "|" + 
                         sourcemsisdn + "|" + destmsisdn + "|" + origID + "|" + orig; 
                
        log.info("Insert Transaction query request into hits table, payload... " + payload);
        
        Hits hit = new Hits();
        
        hit.setTrxType("MPESAB2CQueryTrx");
        
        hit.setPayload(payload);
        
        AMB2CDAO b2cDAO = new AMB2CDAO();
        
        int hitID = b2cDAO.logHits(hit);
        
        String serverName = hostName;

        Node nodeDetails = new Node();

        NodeDAO nodeDAO = new NodeDAO();

        log.info("Getting server details for " + hostName);
        
        nodeDetails = nodeDAO.getServerDetails(serverName);
        
        int nodeStatus = nodeDetails.getStatus();
        
        Response mpesaResponse = new Response();
        
        String returnResponseString = null;
        
        if (nodeStatus == 0) {
            
            ApiInfo apiInfo = new ApiInfo();
            
            ApiDAO apiDao = new ApiDAO();
            
            apiInfo = apiDao.getApiDetails(nodeDetails.getNodeID(),origID);
            
//            String institutionCode = nodeDetails.getInstitutionCode();
//
//            String merchantCode = nodeDetails.getAccountcode();
//
//            String currencyCode = nodeDetails.getCurrencyCode();

            String serverIP = nodeDetails.getNodeIP();

            int serverPort = nodeDetails.getNodePort();

            int timeout = nodeDetails.getTimeout();

            Transfer transfer = new Transfer();
            
            transfer = b2cDAO.findByTracetNumber(transactionID,sourceID);
            
            log.info("Originator ID " + transfer.getTransferID());
            
            MMMMpesaRequest mpesaRequest = new MMMMpesaRequest();
            
            mpesaRequest.setTransactionID(String.valueOf(transfer.getTransferID()));
            mpesaRequest.setMsisdn(destmsisdn);
//            mpesaRequest.setInstitutionCode(institutionCode);
//            mpesaRequest.setMerchantCode(merchantCode);
//            mpesaRequest.setCurrencyCode(currencyCode);
            mpesaRequest.setTimeout(timeout);
            mpesaRequest.setISOServerIP(serverIP);
            mpesaRequest.setISOServerPort(serverPort);
            
//            try {
                 
                MpesaB2CRequestTrxClient transactionQuery = new MpesaB2CRequestTrxClient();
                
                MMMMpesaResponse responseMsg = new MMMMpesaResponse();
                
                responseMsg = transactionQuery.SendRequestTransaction(apiInfo,mpesaRequest,nodeDetails);
                
//                String responseString = responseMsg.getResponseMsg();
//                
//                JAXBContext jc;
//                
////                JAXBElement<Response> response = null;
//                
//                jc = JAXBContext.newInstance("org.csapi.www.wsdl.transaction.data.v1_0.response");
//                
//                Unmarshaller unmarshaller = jc.createUnmarshaller();
//                
//                StringBuffer xmlStr = new StringBuffer(responseString);
//                
////                response = (JAXBElement<Response>)((JAXBElement) unmarshaller.unmarshal(new StreamSource(new StringReader(xmlStr.toString())))).getValue();
//                
//                mpesaResponse = (Response)((javax.xml.bind.JAXBElement) unmarshaller.unmarshal(new StringReader(xmlStr.toString()))).getValue();
//                
////                mpesaResponse = response.getValue();
//                
//                if(mpesaResponse.getResponseCode().equals("0")) {
//                    
//                    mpesaResponse.setResponseDesc("Success");
//                }
//                
//                
//                Transfer b2cTrx = new Transfer();
//                
//                b2cTrx.setHitsID(hitID);
//                b2cTrx.setNodeID(nodeDetails.getNodeID());
//                b2cTrx.setOrigID(origID);
//                b2cTrx.setOrig(orig);
//                b2cTrx.setBusinessNumber(nodeDetails.getAccountcode());
//                b2cTrx.setTrxCode(transactionID);
//                b2cTrx.setDestAccount(destmsisdn);
//                b2cTrx.setSourceMSISDN(sourcemsisdn);
//                b2cTrx.setAmount(AMOUNT);
//                b2cTrx.setCurrencyCode(nodeDetails.getCurrencyCode());
////                b2cTrx.setResultData(responseMsg.getTransactionData());
//                b2cTrx.setStatus(0);
//                b2cTrx.setResultStatus(mpesaResponse.getResponseCode());
//                b2cTrx.setTraceNumber(mpesaResponse.getConversationID());
//                b2cTrx.setDescription(mpesaResponse.getResponseDesc());
//                
//                
//                returnResponseString = mpesaResponse.getResponseCode() + "$" + mpesaResponse.getResponseDesc(); 
//                        
//                log.info("Insert Card2Mobile transaction details.... ");
//                
//                b2cDAO.addC2BTrx(b2cTrx);

//            } catch (JAXBException e) {
//                // TODO Auto-generated catch block
////                e.printStackTrace();
//                log.error("Exception: ",e.fillInStackTrace());
//            }

        } else {

            mpesaResponse.setResponseCode("95");

            mpesaResponse.setResponseDesc("Server currently unavailable.");
        }
        
        log.info("Response: ");
        
        return returnResponseString;
    }
    
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        QueryMpesaB2CTrxJob requestB2C = new QueryMpesaB2CTrxJob();
        
//        Random generator = new Random();  
//        
//        int num = generator.nextInt(899999) + 100000;
        
        String trxID = "415";
        
        log.info("TransactionID: " + trxID);
        
        requestB2C.queryMpesaB2CTransaction("SRVSAFKE02","254722259541","254722259541",trxID,2,"POAPAYKE01");
    }
}



package com.ars.c2b.register.url.controller;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.ars.c2b.register.url.client.RegisterURLClient;

import ke.co.ars.dao.ApiDAO;
import ke.co.ars.entity.ApiInfo;
import ke.co.ars.entity.Transfer;

public class RegisterURLController {
	
	/* Get actual class name to be printed on */
    static Logger log = Logger.getLogger(RegisterURLController.class.getName());
    
    public RegisterURLController() {
    	
    	//PropertiesConfigurator is used to configure logger from properties file
        PropertyConfigurator.configure("E:\\log4j\\mpesalog4j.properties");
    }

	public void processRegisterMpesaC2BUrl(String serverName0,int origId0,String commandCode0) {
		
		log.info("Initiating processRegisterMpesaC2BUrl request.....");
		
		Transfer registerUrlTransfer = new Transfer();
		
		registerUrlTransfer.setServerName(serverName0);
		registerUrlTransfer.setOrigID(origId0);
		registerUrlTransfer.setCommandCode(commandCode0);
		
		//get mpesa c2b url registration details
		ApiInfo processRegisterMpesaUrl0 = new ApiInfo();
		
		ApiDAO processRegisterApiDAO0 = new ApiDAO();
		
		processRegisterMpesaUrl0 = processRegisterApiDAO0.getMpesaC2BRegisterUrlInfo(registerUrlTransfer);
		
		//send register url request to SAG
		RegisterURLClient registerURLClient0 = new RegisterURLClient();
		
		registerURLClient0.registerC2BUrl(processRegisterMpesaUrl0);
		
	}
}

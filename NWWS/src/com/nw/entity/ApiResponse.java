package com.nw.entity;

public class ApiResponse {

	private String originatorTransactionID;
	private String refrenceCode;
	private String responseCode;
	private String responseDetails;
	
	public ApiResponse(){
		
	}
	
	public ApiResponse(String orignTrxID,String refCode,String respCode, String respDetails){
		this.originatorTransactionID = orignTrxID;
		this.refrenceCode = refCode;
		this.responseCode = respCode;
		this.responseDetails = respDetails;
	}
	
	public String getOriginatorTransactionID() {
		return originatorTransactionID;
	}
	public void setOriginatorTransactionID(String originatorTransactionID) {
		this.originatorTransactionID = originatorTransactionID;
	}
	public String getRefrenceCode() {
		return refrenceCode;
	}
	public void setRefrenceCode(String refrenceCode) {
		this.refrenceCode = refrenceCode;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseDetails() {
		return responseDetails;
	}
	public void setResponseDetails(String responseDetails) {
		this.responseDetails = responseDetails;
	}
	
	@Override
	public String toString() {

	        return new StringBuffer(" origin_transactionId : ").append(this.originatorTransactionID)
	        		.append(" reference_code : ").append(this.refrenceCode)
	        		.append(" status : ").append(this.responseCode)
	        		.append(" details : ").append(this.responseDetails).toString();

	}
}

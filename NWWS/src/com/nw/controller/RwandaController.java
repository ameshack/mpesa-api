/*
    Author: Alex Mbolonzi

 */
package com.nw.controller;

//import java.util.ArrayList;
//import java.util.List;
//import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import ke.co.ars.entity.InstacashSendMoney;
import com.instacashworlwide.client.InstacashJob;
import com.nw.entity.Response;
import ke.co.ars.entity.TrxResponse;

//import ke.co.abc.ISOIntegrator.AbcIsoJob;

public class RwandaController {
    
    /* Get actual class name to be printed on */
    static Logger log = Logger.getLogger(RwandaController.class.getName());
    
    public RwandaController() {
        
      //PropertiesConfigurator is used to configure logger from properties file
        
        PropertyConfigurator.configure("/opt/log4j/B2CWebserviceLog4j.properties");
    }

    public Response card2MobileRwanda(String MNO,String countryCode, 
            String transactionID, String sourceMobile, String destMobile,
            String sourceAmount,String destAmount, int sourceID, String source, 
            String fxRate,String currencyTo,String currencyFrom) {

        log.info("card2MobileRwanda() : " + MNO);
        
        Response response = new Response();

        String host = null;
        
        String status = null;
        
        String responseDesc = null;
        
        TrxResponse c2mTransactionResponse = null;
        
//            switch (MNO) {
//                    
//                    case "SAFKE":

                                        
                    switch(source.toUpperCase()){
                    
                    case "POAPAYRW01":
                    	
                    	host = "SRVINSTACASHRW" + "01";
                    	
                    	InstacashSendMoney instacashSendMoney = new InstacashSendMoney();
                    	
                    	instacashSendMoney.setWallet(MNO);
                    	instacashSendMoney.setDestcountry("Rwanda");
                    	instacashSendMoney.setSession(transactionID);
                    	instacashSendMoney.setSenderphone(sourceMobile);
                    	instacashSendMoney.setRecipientphone(destMobile);
                    	instacashSendMoney.setSendamount(Double.valueOf(sourceAmount));
                    	instacashSendMoney.setDestinationamount(Double.valueOf(destAmount));
                    	instacashSendMoney.setFxrate(fxRate);
                    	instacashSendMoney.setCurrencyto(currencyTo);
                    	instacashSendMoney.setCurrencyfrom(currencyFrom);
                    	
                    	InstacashJob instacashJob = new InstacashJob();
                        
                    	c2mTransactionResponse = new TrxResponse();
                    	
                    	c2mTransactionResponse = instacashJob.instaCashRequest(instacashSendMoney,host,
                    			sourceID,source);
                    	
                    	break;

                    }
                      
                                    status = "00";

//                        break;
                        
//                        default:                            
//                            status = "M0007";                            
//                        break;
//                    }
                    
                    response.setResponseStatus(status);
                    response.setResponseDescription(c2mTransactionResponse.getStatusDescription());
                    response.setTransactionID(transactionID);

        return response;
    }
    
}

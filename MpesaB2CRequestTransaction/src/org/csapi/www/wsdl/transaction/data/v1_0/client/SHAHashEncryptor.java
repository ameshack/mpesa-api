/*
    Author: Alex Mbolonzi

 */
package org.csapi.www.wsdl.transaction.data.v1_0.client;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;

//import org.apache.commons.codec.Base64;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;

public class SHAHashEncryptor 
{
    public String hashPassword(String passwordString)throws Exception
    {
    
         
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        
        md.update(passwordString.getBytes());
 
        byte byteData[] = md.digest();
 
        //convert the byte to hex format method 1
//        StringBuffer sb = new StringBuffer();
//        for (int i = 0; i < byteData.length; i++) {
//         sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
//        }
 
//        System.out.println("Hex format : " + sb.toString());
 
        //convert the byte to hex format method 2
        StringBuffer hexString = new StringBuffer();
        
        for (int i=0;i<byteData.length;i++) {
            String hex=Integer.toHexString(0xff & byteData[i]);
            if(hex.length()==1) hexString.append('0');
            hexString.append(hex);
        }
        
        
//        String encodedBytes = Base64.encode(hexString.toString().getBytes());
        String encodedBytes = Base64.encodeBase64String(StringUtils.getBytesUtf8(hexString.toString()));
//        System.out.println("encodedBytes " + new String(encodedBytes));
        
//        System.out.println("Hex format : " + hexString.toString());
        return encodedBytes;
    }
}
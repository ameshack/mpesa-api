USE `forge`;
DROP procedure IF EXISTS `get_mpesa_api_details_by_transaction_id`;


DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_mpesa_api_details_by_transaction_id`(IN orig_trx_id varchar(45),
OUT mpesa_sp_id varchar(10),
OUT mpesa_sp_password varchar(45),OUT mpesa_service_id varchar(15),
OUT mpesa_command_id varchar(15),OUT mpesa_language_code varchar(15),
OUT mpesa_queue_timeout_url varchar(100),OUT mpesa_third_party_id varchar(15),
OUT mpesa_caller_password varchar(45),OUT mpesa_check_sum varchar(15),
OUT mpesa_result_url varchar(100),OUT mpesa_request_url varchar(100),
OUT mpesa_query_trx_url varchar(100),OUT mpesa_identifier INTEGER,
OUT mpesa_initiator_password varchar(45),OUT mpesa_short_code varchar(10),
OUT mpesa_primary_party_id INTEGER,OUT mpesa_receiver_id INTEGER,
OUT mpesa_access_id INTEGER,OUT mpesa_access_identifier varchar(15),
OUT mpesa_key_owner INTEGER,OUT mpesa_key_store varchar(100),
OUT mpesa_key_store_password varchar(45),OUT mpesa_caller_type INTEGER,
OUT mpesa_trust_store varchar(100),OUT mpesa_trust_store_password varchar(45),
OUT mpesa_identifier_type INTEGER,OUT mpesa_timeout INTEGER,
OUT mpesa_sql_state INTEGER,OUT mpesa_host_ip varchar(15),
OUT mpesa_host_port varchar(11))

BEGIN

		DECLARE mpesa_api_cur CURSOR FOR SELECT b.sp_id,AES_DECRYPT(b.sp_password, SHA1('@ut0b073')) sp_password,
						b.service_id,b.command_id,b.language_code,b.queue_timeout_url,
						b.third_party_id,AES_DECRYPT(b.caller_password, SHA1('@ut0b073')) caller_password,
						b.check_sum,b.result_url,b.request_url,b.query_trx_url,
						b.identifier,AES_DECRYPT(b.initiator_password, SHA1('@ut0b073')) initiator_password,
						b.short_code,b.primary_party_id,b.receiver_id,b.access_id,
						b.access_identifier,b.key_owner,b.key_store,b.key_store_password,
						b.caller_type,b.trust_store,b.trust_store_password,
						b.identifier_type,c.host_ip,c.host_port,c.timeout FROM transfers a INNER JOIN mpesa_api b
						ON a.destNodeID = b.node_id INNER JOIN node c ON c.id = b.node_id
						WHERE a.id = TRIM(orig_trx_id) ORDER BY a.id ASC LIMIT 1;

		DECLARE EXIT HANDLER FOR NOT FOUND 

			BEGIN 

				CALL log_forge_error (CONCAT('procedure name: get_mpesa_api_details_by_transaction_id() NO RECORD FOUND FOR',serverName));

				SET mpesa_sql_state = 23000;
			END;

		DECLARE EXIT HANDLER FOR SQLEXCEPTION 

			BEGIN 

				CALL log_forge_error (CONCAT('procedure name: get_mpesa_api_details_by_transaction_id() SQL EXCEPTION'));
				
				SET mpesa_sql_state = 1105;
			END;

		DECLARE CONTINUE HANDLER FOR SQLWARNING

			BEGIN 

				CALL log_forge_error (CONCAT('procedure name: get_mpesa_api_details_by_transaction_id() SQL WARNING'));

			END;


						OPEN mpesa_api_cur;

						FETCH mpesa_api_cur INTO mpesa_sp_id,mpesa_sp_password,mpesa_service_id,mpesa_command_id,
												mpesa_language_code,mpesa_queue_timeout_url,mpesa_third_party_id,
												mpesa_caller_password,mpesa_check_sum,mpesa_result_url,mpesa_request_url,
												mpesa_query_trx_url,mpesa_identifier,mpesa_initiator_password,
												mpesa_short_code,mpesa_primary_party_id,mpesa_receiver_id,
												mpesa_access_id,mpesa_access_identifier,mpesa_key_owner,mpesa_key_store,
												mpesa_key_store_password,mpesa_caller_type,mpesa_trust_store,
												mpesa_trust_store_password,mpesa_identifier_type,mpesa_host_ip,
												mpesa_host_port,mpesa_timeout;

						CLOSE mpesa_api_cur;

						SET mpesa_sql_state = 0;
END$$
DELIMITER ;

GRANT EXECUTE ON PROCEDURE forge.get_mpesa_api_details_by_transaction_id TO 'restb2c'@'localhost' IDENTIFIED BY 'restb2c';

FLUSH PRIVILEGES;
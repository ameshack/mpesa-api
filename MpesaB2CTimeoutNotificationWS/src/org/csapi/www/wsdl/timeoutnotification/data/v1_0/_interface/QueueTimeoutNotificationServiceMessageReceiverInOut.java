/**
 * QueueTimeoutNotificationServiceMessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
package org.csapi.www.wsdl.timeoutnotification.data.v1_0._interface;

/**
 * QueueTimeoutNotificationServiceMessageReceiverInOut message receiver
 */

public class QueueTimeoutNotificationServiceMessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver {

    public void invokeBusinessLogic(org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext)
            throws org.apache.axis2.AxisFault {

        // System.out.println(msgContext.getEnvelope().toString());

        try {

            // get the implementation class for the Web Service
            Object obj = getTheImplementationObject(msgContext);

            QueueTimeoutNotificationServiceSkeletonInterface skel = (QueueTimeoutNotificationServiceSkeletonInterface) obj;
            // Out Envelop
            org.apache.axiom.soap.SOAPEnvelope envelope = null;
            // Find the axisOperation that has been set by the Dispatch phase.
            org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext().getAxisOperation();

            if (op == null) {
                throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
            }

            java.lang.String methodName;
            if ((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJavaIdentifier(op.getName().getLocalPart())) != null)) {

                if ("notifyQueueTimeout".equals(methodName)) {

                    org.csapi.www.schema.timeoutnotification.data.v1_0.local.NotifyQueueTimeoutResponseE notifyQueueTimeoutResponse3 = null;
                    org.csapi.www.schema.timeoutnotification.data.v1_0.local.NotifyQueueTimeoutE wrappedParam =
                            (org.csapi.www.schema.timeoutnotification.data.v1_0.local.NotifyQueueTimeoutE) fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    org.csapi.www.schema.timeoutnotification.data.v1_0.local.NotifyQueueTimeoutE.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));

                    notifyQueueTimeoutResponse3 =

                            skel.notifyQueueTimeout(wrappedParam);

                    envelope = toEnvelope(getSOAPFactory(msgContext), notifyQueueTimeoutResponse3, false, new javax.xml.namespace.QName("http://www.csapi.org/wsdl/timeoutnotification/data/v1_0/interface",
                            "notifyQueueTimeout"));

                    // System.out.println(envelope.toString());
                } else {
                    throw new java.lang.RuntimeException("method not found");
                }

                newMsgContext.setEnvelope(envelope);
            }
        } catch (java.lang.Exception e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }
    }

    //
    private org.apache.axiom.om.OMElement toOM(org.csapi.www.schema.timeoutnotification.data.v1_0.local.NotifyQueueTimeoutE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

        try {
            return param.getOMElement(org.csapi.www.schema.timeoutnotification.data.v1_0.local.NotifyQueueTimeoutE.MY_QNAME,
                    org.apache.axiom.om.OMAbstractFactory.getOMFactory());
        } catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }

    }

    private org.apache.axiom.om.OMElement toOM(org.csapi.www.schema.timeoutnotification.data.v1_0.local.NotifyQueueTimeoutResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

        try {
            return param.getOMElement(org.csapi.www.schema.timeoutnotification.data.v1_0.local.NotifyQueueTimeoutResponseE.MY_QNAME,
                    org.apache.axiom.om.OMAbstractFactory.getOMFactory());
        } catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }

    }

    private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, org.csapi.www.schema.timeoutnotification.data.v1_0.local.NotifyQueueTimeoutResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
            throws org.apache.axis2.AxisFault {
        try {
            org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

            emptyEnvelope.getBody().addChild(param.getOMElement(org.csapi.www.schema.timeoutnotification.data.v1_0.local.NotifyQueueTimeoutResponseE.MY_QNAME, factory));

            return emptyEnvelope;
        } catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }
    }

    private org.csapi.www.schema.timeoutnotification.data.v1_0.local.NotifyQueueTimeoutResponseE wrapnotifyQueueTimeout() {
        org.csapi.www.schema.timeoutnotification.data.v1_0.local.NotifyQueueTimeoutResponseE wrappedElement = new org.csapi.www.schema.timeoutnotification.data.v1_0.local.NotifyQueueTimeoutResponseE();
        return wrappedElement;
    }

    /**
     * get the default envelope
     */
    private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory) {
        return factory.getDefaultEnvelope();
    }

    private java.lang.Object fromOM(
            org.apache.axiom.om.OMElement param,
            java.lang.Class type,
            java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault {

        try {

            if (org.csapi.www.schema.timeoutnotification.data.v1_0.local.NotifyQueueTimeoutE.class.equals(type)) {

                return org.csapi.www.schema.timeoutnotification.data.v1_0.local.NotifyQueueTimeoutE.Factory.parse(param.getXMLStreamReaderWithoutCaching());

            }

            if (org.csapi.www.schema.timeoutnotification.data.v1_0.local.NotifyQueueTimeoutResponseE.class.equals(type)) {

                return org.csapi.www.schema.timeoutnotification.data.v1_0.local.NotifyQueueTimeoutResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());

            }

        } catch (java.lang.Exception e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }
        return null;
    }

    /**
     * A utility method that copies the namepaces from the SOAPEnvelope
     */
    private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env) {
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
            org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
            returnMap.put(ns.getPrefix(), ns.getNamespaceURI());
        }
        return returnMap;
    }

    private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
        org.apache.axis2.AxisFault f;
        Throwable cause = e.getCause();
        if (cause != null) {
            f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
        } else {
            f = new org.apache.axis2.AxisFault(e.getMessage());
        }

        return f;
    }

}// end of class

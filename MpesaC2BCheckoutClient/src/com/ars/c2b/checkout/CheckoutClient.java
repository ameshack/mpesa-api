package com.ars.c2b.checkout;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import ke.co.ars.entity.C2BCheckoutRequest;

import com.ars.encryption.SHAHashEncryptor;

import ns.CheckOutHeader;
import ns.Lnmo_checkout_ServiceStub;
import ns.ProcessCheckOutRequest;
import ns.ProcessCheckOutResponse;
import ns.TransactionConfirmRequest;
import ns.TransactionConfirmResponse;
import ns.TransactionStatusRequest;
import ns.TransactionStatusResponse;

public class CheckoutClient {
	
	/* Get actual class name to be printed on */
    static Logger log = Logger.getLogger(CheckoutClient.class.getName());
    
    public CheckoutClient() {
    	
    	//PropertiesConfigurator is used to configure logger from properties file
        PropertyConfigurator.configure("E:\\log4j\\mpesalog4j.properties"); 
    }

	public ProcessCheckOutResponse sendMpesaC2BCheckout(C2BCheckoutRequest checkoutRequest0) {
		
		log.info("Initiating sendMpesaC2BCheckout Request.....");
		
		SimpleDateFormat timeStamp = new SimpleDateFormat("yyyyMMddHHmmss");
        
        Date date = new Date();
        
        String passwordString = checkoutRequest0.getMerchantId() + 
        		checkoutRequest0.getMerchantPassword() + timeStamp.format(date);
        
        String hashedPasswordString = null;
        
        SHAHashEncryptor checkOutEncryptor = new SHAHashEncryptor();
        
        ProcessCheckOutResponse processCheckOutResponse0 = null;
        
        try {
//        	encrypt and has password
			hashedPasswordString = checkOutEncryptor.hashPassword(passwordString);
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("Exception: ",e.fillInStackTrace());
		}
        
        //Set SOAP header parameters
		CheckOutHeader processCheckOutHeader = new CheckOutHeader();
		
		processCheckOutHeader.setMERCHANT_ID(checkoutRequest0.getMerchantId());
		processCheckOutHeader.setPASSWORD(hashedPasswordString);
		processCheckOutHeader.setTIMESTAMP(timeStamp.format(date));
		
		//set checkout request SOAP body parameters
		ProcessCheckOutRequest processCheckOutRequest = new ProcessCheckOutRequest();
		
		processCheckOutRequest.setMERCHANT_TRANSACTION_ID(checkoutRequest0.getMerchantTransactionId());
		processCheckOutRequest.setREFERENCE_ID(checkoutRequest0.getReferenceId());
		processCheckOutRequest.setAMOUNT(checkoutRequest0.getAmount());
		processCheckOutRequest.setMSISDN(checkoutRequest0.getMsisdn());
		//optional parameters
		processCheckOutRequest.setENC_PARAMS(checkoutRequest0.getEncryptedData());
		processCheckOutRequest.setCALL_BACK_URL(checkoutRequest0.getCallbackURL());
		processCheckOutRequest.setCALL_BACK_METHOD(checkoutRequest0.getCallbackMethod());
		
		
		try {
			
//			//set necessary truststore properties - using JKS
//            System.setProperty("javax.net.ssl.trustStore","C:\\Program Files\\Java\\jdk1.8.0_60\\jre\\lib\\security\\cacerts");
//            System.setProperty("javax.net.ssl.trustStorePassword","changeit");
//            
			//set timeout in milliseconds
			long timeout = checkoutRequest0.getRequestTimeout();
			
			log.debug("timeout ..... " + timeout + " SAG URL: " + checkoutRequest0.getSagULR());
			
			//set SAG endpoint
			Lnmo_checkout_ServiceStub lnmoCheckoutServiceStub;
			
			lnmoCheckoutServiceStub = new Lnmo_checkout_ServiceStub(checkoutRequest0.getSagULR());
			
			lnmoCheckoutServiceStub._getServiceClient().getOptions().setTimeOutInMilliSeconds(timeout);
			
			processCheckOutResponse0 = new ProcessCheckOutResponse();
			
//			log.debug("Header ..... " + processCheckOutHeader.toString() + " Request : " + processCheckOutRequest.toString());
			
			//send checkout request
			processCheckOutResponse0 = lnmoCheckoutServiceStub.processCheckOut(processCheckOutRequest, processCheckOutHeader);
			
			log.debug("checkout response..... " + processCheckOutResponse0.toString());
			
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("Exception: ",e.fillInStackTrace());
		}
		
		return processCheckOutResponse0;
	}
	
	public TransactionConfirmResponse sendConfirmC2BPayment(C2BCheckoutRequest checkoutRequest1) {
		
		log.info("Initiating sendConfirmC2BPayment Request.....");
		
		SimpleDateFormat timeStamp = new SimpleDateFormat("yyyyMMddHHmmss");
        
        Date date = new Date();
        
        String passwordString = checkoutRequest1.getMerchantId() + 
        		checkoutRequest1.getMerchantPassword() + timeStamp.format(date);
        
        String hashedPasswordString = null;
        
        SHAHashEncryptor confirmPaymentEncryptor = new SHAHashEncryptor();
        
        
        try {
//        	encrypt and has password
			hashedPasswordString = confirmPaymentEncryptor.hashPassword(passwordString);
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("Exception: ",e.fillInStackTrace());
		}
        
        //Set SOAP header parameters
		CheckOutHeader confirmC2BPaymentHeader = new CheckOutHeader();
		
		confirmC2BPaymentHeader.setMERCHANT_ID(checkoutRequest1.getMerchantId());
		confirmC2BPaymentHeader.setPASSWORD(hashedPasswordString);
		confirmC2BPaymentHeader.setTIMESTAMP(timeStamp.format(date));
		
		//set transaction confrim request body parameters
		TransactionConfirmRequest c2btransactionConfirmRequest = new TransactionConfirmRequest();
		
		//cutomer mobile number
		c2btransactionConfirmRequest.setTRX_ID(checkoutRequest1.getMerchantTransactionId());
		
		//unique transaction id
		c2btransactionConfirmRequest.setMERCHANT_TRANSACTION_ID(null);
		
		TransactionConfirmResponse c2bTransactionConfirmResponse = null;
		
		try {
			
//			//set necessary truststore properties - using JKS
//            System.setProperty("javax.net.ssl.trustStore","C:\\Program Files\\Java\\jdk1.8.0_60\\jre\\lib\\security\\cacerts");
//            System.setProperty("javax.net.ssl.trustStorePassword","changeit");
            
			
			//set timeout in milliseconds
			long timeout = checkoutRequest1.getRequestTimeout();
			
			//set SAG endpoint
			Lnmo_checkout_ServiceStub lnmoConfirmTransactionServiceStub;
			
			//set SAG endpoint
			lnmoConfirmTransactionServiceStub = new Lnmo_checkout_ServiceStub(checkoutRequest1.getSagULR());
			
			lnmoConfirmTransactionServiceStub._getServiceClient().getOptions().setTimeOutInMilliSeconds(timeout);
			
			c2bTransactionConfirmResponse = new TransactionConfirmResponse();
			
			//send request and get response parameters
			c2bTransactionConfirmResponse = lnmoConfirmTransactionServiceStub.confirmTransaction(c2btransactionConfirmRequest, confirmC2BPaymentHeader);
			
			
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("Exception: ",e.fillInStackTrace());
		}
			
		return c2bTransactionConfirmResponse;
	}
	
	public TransactionStatusResponse sendQueryMpesaC2BTransactionStatus(C2BCheckoutRequest checkoutRequest2) {
		
		log.info("Initiating sendQueryMpesaC2BTransactionStatus Request.....");
		
		SimpleDateFormat timeStamp = new SimpleDateFormat("yyyyMMddHHmmss");
        
        Date date = new Date();
        
        String passwordString = checkoutRequest2.getMerchantId() + 
        		checkoutRequest2.getMerchantPassword() + timeStamp.format(date);
        
        String hashedPasswordString = null;
        
        SHAHashEncryptor queryTransactionStatusEncryptor = new SHAHashEncryptor();
        
        
        try {
//        	encrypt and has password
			hashedPasswordString = queryTransactionStatusEncryptor.hashPassword(passwordString);
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("Exception: ",e.fillInStackTrace());
		}
        
        //Set SOAP header parameters
		CheckOutHeader queryTransactionStatusHeader = new CheckOutHeader();
		
		queryTransactionStatusHeader.setMERCHANT_ID(checkoutRequest2.getMerchantId());
		queryTransactionStatusHeader.setPASSWORD(hashedPasswordString);
		queryTransactionStatusHeader.setTIMESTAMP(timeStamp.format(date));
		
		//set transaction request parameters
		TransactionStatusRequest c2bTransactionStatusRequest = new TransactionStatusRequest();
		
		//optional
		c2bTransactionStatusRequest.setTRX_ID(checkoutRequest2.getSagTransactionId());
		
//		c2bTransactionStatusRequest.setMERCHANT_TRANSACTION_ID(checkoutRequest2.getMerchantTransactionId());;
		
		TransactionStatusResponse c2bTransactionStatusResponse = null;
		
		try {
			
			//set timeout in milliseconds
			long timeout = checkoutRequest2.getRequestTimeout();
			
			//set SAG endpoint
			Lnmo_checkout_ServiceStub lnmoTransactionStatusServiceStub;
			
			//set SAG endpoint
			lnmoTransactionStatusServiceStub = new Lnmo_checkout_ServiceStub(checkoutRequest2.getSagULR());
			
			lnmoTransactionStatusServiceStub._getServiceClient().getOptions().setTimeOutInMilliSeconds(timeout);
			
			//send SOAP request and process response from SAG
			c2bTransactionStatusResponse = new TransactionStatusResponse();
			
			c2bTransactionStatusResponse = lnmoTransactionStatusServiceStub.transactionStatusQuery(c2bTransactionStatusRequest, queryTransactionStatusHeader);
			
			
			//TODO: log details to DB, implement logging
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("Exception: ",e.fillInStackTrace());
		}
		
		return c2bTransactionStatusResponse;
	}
}
